package com.tud.kom.parkinglot.activities.reservation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tud.kom.parkinglot.model.ParkingLot;

import java.util.List;

public class ParkingLotAdapter extends ArrayAdapter<ParkingLot> {

    public ParkingLotAdapter(@NonNull final Context context, final List<ParkingLot> objects) {
        super(context, android.R.layout.simple_spinner_item, objects);
    }

    @Override
    public View getDropDownView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
        ParkingLot parkingLot = super.getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(super.getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);

        }

        TextView textView = convertView.findViewById(android.R.id.text1);
        textView.setText(parkingLot.getName());

        return convertView;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
        ParkingLot parkingLot = super.getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(super.getContext()).inflate(android.R.layout.simple_spinner_item, parent, false);

        }

        TextView textView = convertView.findViewById(android.R.id.text1);
        textView.setText(parkingLot.getName());

        return convertView;
    }

    public int getIndexOfItemWithId(int id) {
        for (int i = 0; i < getCount(); i++) {
            if (getItem(i).getId() == id) {
                return i;
            }
        }

        return 0;
    }
}
