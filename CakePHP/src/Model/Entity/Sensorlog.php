<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Sensorlog Entity
 *
 * @property int $id
 * @property int $parkinglot_id
 * @property int $x
 * @property int $y
 * @property int $z
 * @property int $zThreshold
 * @property string $state
 * @property float $supplyVoltage
 * @property int $temperature
 * @property float $humidity
 * @property int $RSSI_gwRX
 * @property int $RSSI_nodeRX_avg
 * @property int $numRetries_NodeToGW
 * @property int $numRxTimeouts
 * @property int $errorCode
 * @property string $filename
 * @property \Cake\I18n\Time $logtime
 * @property string $verbatim
 *
 * @property \App\Model\Entity\Parkinglot $parkinglot
 */
class Sensorlog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
