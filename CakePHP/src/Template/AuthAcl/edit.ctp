<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
        <li class="btn btn-default"><?= $this->Html->link(__('List Auth Acl'), ['action' => 'index']) ?></li>
        <li class="btn btn-default"><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $authAcl->id],
                ['confirm' => __('Are you sure you want to delete Action->{0}, on Controller->{1} for Role->{2}', $authAcl->actions, $authAcl->controller, $authRoles[0])]
            )
        ?></li>
        <li class="btn btn-default"><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
        <li class="btn btn-default"><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
    </ul>
</div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Authorization Acl
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <?= $this->Form->create($authAcl) ?>
                        <fieldset>
                            <legend><?= __('Edit Auth Acl') ?></legend>
                            <?php
                                echo "<div class='form-group'>".$this->Form->input('controller', ['label' => 'Controller:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('actions', ['label' => 'Actions:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('role_id', array('type' => 'select',
                                                                'options' => $authAcl->role,'class' => 'form-control'
                                                                ))."</div>";
                            ?>
                        </fieldset>
                        <?= $this->Form->button('Submit',  array('class' => 'btn btn-default')); ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php $this->Html->addCrumb('AuthAcl', '/authAcl'); ?>
<?php $this->Html->addCrumb('Edit', ['controller' => 'AuthAcl', 'action' => 'edit']); ?>