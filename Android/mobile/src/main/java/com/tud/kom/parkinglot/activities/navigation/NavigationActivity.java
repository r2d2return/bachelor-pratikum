package com.tud.kom.parkinglot.activities.navigation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.google.firebase.iid.FirebaseInstanceId;
import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.activities.history.HistoryFragment;
import com.tud.kom.parkinglot.activities.main.MainActivity;
import com.tud.kom.parkinglot.activities.overview.OverviewFragment;
import com.tud.kom.parkinglot.network.NetworkCallback;
import com.tud.kom.parkinglot.network.RequestAbstraction;
import com.tud.kom.parkinglot.network.Response;

public class NavigationActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, NetworkCallback {
    private static final String TAG = NavigationActivity.class.getSimpleName();
    private ProgressDialog mProgressDialog;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String token = FirebaseInstanceId.getInstance().getToken();

        String username = SavedPreferences.getPreference(this, SavedPreferences.Filename.USERNAME);
        String password = SavedPreferences.getPreference(this, SavedPreferences.Filename.PASSWORD);
        if (token != null)
            RequestAbstraction.sendFirebaseInstanceId(null, token, username, password);

        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        mNavigationView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(mNavigationView.getMenu().getItem(0));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mNavigationView.getMenu().getItem(1).isChecked()) {
            showOverview();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_overview) {
            showOverview();
        }
        else if (id == R.id.nav_history) {
            showHistory();
        }
        else {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.waitingForLogout));
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            RequestAbstraction.clearFirebaseInstanceId(this, SavedPreferences.getPreference(this, SavedPreferences.Filename.USERNAME), SavedPreferences.getPreference(this, SavedPreferences.Filename.PASSWORD));
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.nav_content, fragment);
        transaction.commit();
    }

    private void showHistory() {
        this.setTitle(R.string.history);
        mNavigationView.getMenu().getItem(1).setChecked(true);
        showFragment(new HistoryFragment());
    }

    private void showOverview() {
        this.setTitle(R.string.overview);
        mNavigationView.getMenu().getItem(0).setChecked(true);
        showFragment(new OverviewFragment());
    }

    /**
     * logout response
     */
    @Override
    public void handleWebsiteResponse(Response response) {
        SavedPreferences.deleteAllPreferences(this);
        Intent intent = new Intent(this, MainActivity.class);
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        startActivity(intent);
        finish();
    }
}
