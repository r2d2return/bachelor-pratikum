<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ReservationsFixture
 *
 */
class ReservationsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    /*public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'token' => ['type' => 'string', 'fixed' => true, 'length' => 32, 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null],
        'start' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'end' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'parkinglot_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'users_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'visitedPersonId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'licencePlate' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'reservationState' => ['type' => 'string', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'messageInfo' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'isRecurring' => ['type' => 'string', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'recurringID' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'parkingLot_id' => ['type' => 'index', 'columns' => ['parkinglot_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];*/
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public function init(){
        $this->records = [
            [   // ist aktiv
                'id' => 1,
                'token' => 'activeToken1',
                'start' => '2017-08-23 12:04:00',
                'end' => '2021-08-23 11:04:00',
                'parkinglot_id' => 1,
                'users_id' => 1,
                'visitedPersonId' => 1,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // ist abgelaufen
                'id' => 2,
                'token' => 'inValidToken1',
                'start' => '2017-08-23 11:04:00',
                'end' => '2017-08-24 11:04:00',
                'parkinglot_id' => 1,
                'users_id' => 1,
                'visitedPersonId' => 1,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // hat noch nicht begonnen
                'id' => 3,
                'token' => 'inActiveToken1',
                'start' => '2018-08-23 11:04:00',
                'end' => '2018-08-24 11:04:00',
                'parkinglot_id' => 1,
                'users_id' => 1,
                'visitedPersonId' => 1,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // ist aktiv
                'id' => 4,
                'token' => 'activeToken2',
                'start' => '2017-08-23 12:04:00',
                'end' => '2021-08-23 11:04:00',
                'parkinglot_id' => 2,
                'users_id' => 2,
                'visitedPersonId' => 2,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // ist abgelaufen
                'id' => 5,
                'token' => 'inValidToken2',
                'start' => '2017-08-23 11:04:00',
                'end' => '2017-08-24 11:04:00',
                'parkinglot_id' => 2,
                'users_id' => 2,
                'visitedPersonId' => 2,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // hat noch nicht begonnen
                'id' => 6,
                'token' => 'inActiveToken2',
                'start' => '2018-08-23 11:04:00',
                'end' => '2018-08-24 11:04:00',
                'parkinglot_id' => 2,
                'users_id' => 2,
                'visitedPersonId' => 2,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // ist aktiv
                'id' => 7,
                'token' => 'activeToken3',
                'start' => '2017-08-23 12:04:00',
                'end' => '2021-08-23 11:04:00',
                'parkinglot_id' => 3,
                'users_id' => 3,
                'visitedPersonId' => 3,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // ist abgelaufen
                'id' => 8,
                'token' => 'inValidToken3',
                'start' => '2017-08-23 11:04:00',
                'end' => '2017-08-24 11:04:00',
                'parkinglot_id' => 3,
                'users_id' => 3,
                'visitedPersonId' => 3,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // hat noch nicht begonnen
                'id' => 9,
                'token' => 'inActiveToken3',
                'start' => '2022-08-23 11:04:00',
                'end' => '2022-08-24 11:04:00',
                'parkinglot_id' => 3,
                'users_id' => 3,
                'visitedPersonId' => 3,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // ist aktiv
                'id' => 10,
                'token' => 'activeToken4',
                'start' => '2017-08-23 12:04:00',
                'end' => '2021-08-23 11:04:00',
                'parkinglot_id' => 4,
                'users_id' => 4,
                'visitedPersonId' => 4,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // ist abgelaufen
                'id' => 11,
                'token' => 'inValidToken4',
                'start' => '2017-08-23 11:04:00',
                'end' => '2017-08-24 11:04:00',
                'parkinglot_id' => 4,
                'users_id' => 4,
                'visitedPersonId' => 4,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // hat noch nicht begonnen
                'id' => 12,
                'token' => 'inActiveToken4',
                'start' => '2022-08-23 11:04:00',
                'end' => '2022-08-24 11:04:00',
                'parkinglot_id' => 4,
                'users_id' => 4,
                'visitedPersonId' => 4,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],


            [   // ist aktiv
                'id' => 13,
                'token' => 'activeToken5',
                'start' => '2017-08-23 12:04:00',
                'end' => '2021-08-23 11:04:00',
                'parkinglot_id' => 5,
                'users_id' => 5,
                'visitedPersonId' => 5,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // ist abgelaufen
                'id' => 14,
                'token' => 'inValidToken5',
                'start' => '2017-08-23 11:04:00',
                'end' => '2017-08-24 11:04:00',
                'parkinglot_id' => 5,
                'users_id' => 5,
                'visitedPersonId' => 5,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ],

            [   // hat noch nicht begonnen
                'id' => 15,
                'token' => 'inActiveToken5',
                'start' => '2022-08-23 11:04:00',
                'end' => '2022-08-24 11:04:00',
                'parkinglot_id' => 5,
                'users_id' => 5,
                'visitedPersonId' => 5,
                'licencePlate' => 'Lorem ipsum dolor sit amet',
                'reservationState' => 'RESERVED',
                'messageInfo' => 'Lorem ipsum dolor sit amet',
                'isRecurring' => 'NO',
                'recurringID' => 1,
                'created' => '2017-08-23 11:04:00'
            ]

        ];

        parent::init();
    }
}
