package com.tud.kom.parkinglot.utils;

import android.graphics.drawable.ColorDrawable;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.espresso.remote.annotation.RemoteMsgConstructor;
import android.support.v7.widget.CardView;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

import static android.support.test.espresso.intent.Checks.checkNotNull;

public class WithBackgroundColorMatcher extends BoundedMatcher<View, CardView> {

    private int mColor;

    private Matcher<Integer> mColorMatcher;

    @RemoteMsgConstructor
    private WithBackgroundColorMatcher(Matcher<Integer> colorMatcher) {
        super(CardView.class);
        this.mColorMatcher = colorMatcher;
    }

    public static Matcher<View> withBackgroundColor(final Matcher<Integer> integerMatcher) {
        return new WithBackgroundColorMatcher(checkNotNull(integerMatcher));
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("expected background color " + String.format("#%X", Integer.parseInt(mColorMatcher.toString().replace("is <", "").replace(">", ""))) + " but card has background color " + String.format("#%X", mColor));
    }

    @Override
    protected boolean matchesSafely(final CardView card) {
        mColor = ((ColorDrawable) card.getBackground()).getColor();
        return mColorMatcher.matches(mColor);
    }
}
