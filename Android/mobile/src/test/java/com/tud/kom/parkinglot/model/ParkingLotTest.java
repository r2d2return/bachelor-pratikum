package com.tud.kom.parkinglot.model;

import org.junit.Assert;
import org.junit.Test;

public class ParkingLotTest {
    @Test
    public void testDefaultIsNull() {
        ParkingLot s = new ParkingLot();
        Assert.assertEquals(0, s.getId());
    }

    @Test
    public void testToString() {
        Assert.assertNotNull(new ParkingLot().toString());
    }
}
