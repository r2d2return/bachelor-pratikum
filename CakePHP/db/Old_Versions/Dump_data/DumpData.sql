
--
-- Dumping data for table `gateways`
--

INSERT INTO `gateways` (`id`, `label`, `fqdn`, `site_id`) VALUES
(1, 'RTST', 'rtst.gateway.smartparking.hornjak.de', 1),
(2, 'MST', 'mst.gateway.smartparking.hornjak.de', 4);

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `description`, `type`, `upperLeftLongitude`, `upperLeftLatitude`, `upperRightLongitude`, `upperRightLatitude`, `lowerLeftLongitude`, `lowerLeftLatitude`, `lowerRightLongitude`, `lowerRightLatitude`, `width`, `length`, `imagePath`) VALUES
(1, 'parking/13', 'SQARE', 49.874404, 8.66052, 49.874395, 8.66046, 49.874376, 8.660467, 49.874386, 8.660526, NULL, NULL, NULL),
(2, 'parking/99', 'CIRCLE', 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL);

--
-- Dumping data for table `parkinglots`
--

INSERT INTO `parkinglots` (`id`, `pid`, `description`, `mqttchannel`, `gateway_id`, `status`, `ip`, `hwid`, `lastStateChange`, `type`, `locationId`) VALUES
(1, 13, 'S3|19 P7', 'parking/13', 1, 'blocked', '192.168.10.51', '00-aa-bb-cc-de-02', '2017-09-11 15:48:14', NULL, 1),
(7, 99, 'devTest', 'parking/99', 2, 'blocked', '192.168.18.68', '00-aa-bb-cc-de-04', '2017-09-11 15:48:21', NULL, 2);

--
-- Dumping data for table `parkinglots_sites`
--

INSERT INTO `parkinglots_sites` (`id`, `sitesId`, `parkingLotsId`) VALUES
(2, 1, 1),
(4, 1, 7),
(6, 2, 7);

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `token`, `start`, `end`, `parkinglot_id`, `users_id`, `visitedPersonId`, `licencePlate`, `reservationState`, `messageInfo`, `isRecurring`, `recurringID`, `created`) VALUES
(18, '322072516324a9e6b7026a76146ed326', '2017-09-06 17:00:00', '2017-09-06 20:00:00', 1, 3, 40, 'ikjs', '', '', '', NULL, '2017-09-06 17:01:09'),
(19, '5aa17cb032f71d3959f053447d5d9380', '2017-09-07 13:01:00', '2017-09-07 16:01:00', 1, 3, 39, 'fsadd', '', '', '', NULL, '2017-09-07 13:01:42'),
(21, '1fd3edf4bccef4845c6e7fe0ac02df29', '2017-09-11 11:22:00', '2017-09-14 14:45:00', 1, 3, 40, 'licenceplate', 'RESERVED', '', '', NULL, '2017-09-11 11:24:57');


--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `name`, `description`, `prefix`, `locationID`, `isPublic`, `publicStart`, `publicEnd`, `isPublicRecurring`, `recurringId`) VALUES
(1, 'KOM / Dez. V', NULL, 'kom_', 1, NULL, NULL, NULL, NULL, NULL),
(2, 'Lichtwiese', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(3, 'Lab', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(4, 'MST', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL);

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`id`, `name`, `label`) VALUES
(1, 'TU Darmstadt', 'TUD'),
(2, 'Some Consultant', 'SCON');

--
-- Dumping data for table `tenants_users`
--

INSERT INTO `tenants_users` (`id`, `tenant_id`, `user_id`) VALUES
(1, 1, 3),
(2, 2, 50);




--
-- Dumping data for table `users_zones`
--

INSERT INTO `users_zones` (`id`, `user_id`, `zone_id`, `keyName`, `start`, `end`) VALUES
(1, 3, 1, NULL, NULL, NULL),
(2, 50, 2, NULL, NULL, NULL);


--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`id`, `name`, `description`) VALUES
(1, 'kom', 'KOM Parking Zone P5-P7'),
(2, 'Bibliothek', 'p20 -p 30');

--
-- Dumping data for table `zones_sites`
--

INSERT INTO `zones_sites` (`id`, `zonesId`, `sitesId`) VALUES
(1, 1, 1),
(2, 2, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
