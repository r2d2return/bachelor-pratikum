<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ParkinglotsZonesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ParkinglotsZonesTable Test Case
 */
class ParkinglotsZonesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ParkinglotsZonesTable
     */
    public $ParkinglotsZones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.parkinglots_zones',
        'app.parkinglots',
        'app.gateways',
        'app.sites',
        'app.issued_comands',
        'app.remote_keys',
        'app.reservations',
        'app.sensorlogs',
        'app.parkinglots_sites',
        'app.zones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ParkinglotsZones') ? [] : ['className' => ParkinglotsZonesTable::class];
        $this->ParkinglotsZones = TableRegistry::get('ParkinglotsZones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ParkinglotsZones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
