<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Sensorlogs'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Parkinglots'), ['controller' => 'Parkinglots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Parkinglot'), ['controller' => 'Parkinglots', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="sensorlogs form large-10 medium-9 columns">
    <?= $this->Form->create($sensorlog) ?>
    <fieldset>
        <legend><?= __('Add Sensorlog') ?></legend>
        <?php
            echo $this->Form->input('parkinglot_id', ['options' => $parkinglots, 'empty' => true]);
            echo $this->Form->input('x');
            echo $this->Form->input('y');
            echo $this->Form->input('z');
            echo $this->Form->input('logtime');
            echo $this->Form->input('verbatim');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<?php $this->Html->addCrumb('Screenlogs', '/screenlogs'); ?>
<?php $this->Html->addCrumb('Add', ['controller' => 'Screenlogs', 'action' => 'add']); ?>