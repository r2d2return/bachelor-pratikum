    <div id="md-sub-menu">
        <ul>
            <li><a class="active">Status</a></li>
            <li><a class="modal-reservation" data-reservation-parking-id="<?php echo $parkinglot["id"]; ?>">Reservation</a></li>
            <li><a class="modal-reservation-request">Reservation Request</a></li>
        </ul>
    </div>
    <p style="border-bottom: 1px solid #0000cc"><b>P<?php echo $parkinglot["id"]; ?> : <?php echo $parkinglot["description"];?></b></p>
    <p>Current Status: <?php echo $parkinglot["status"];?></p>

    <?php if($parkinglot["status"] != "free"): ?>
        <p class="btn btn-default free-click"  data-free-parking-id="<?php echo $parkinglot["id"]; ?>">Free</p>
    <?php else: ?>
        <p class="btn btn-default block-click"  data-block="block" data-block-parking-id="<?php echo $parkinglot["id"]; ?>">Block</p>
    <?php endif;?>

