package com.tud.kom.parkinglot.base;

import android.support.v7.util.DiffUtil;

import java.util.List;

public abstract class BaseDiffUtilsCallback<T> extends DiffUtil.Callback {
    protected List<T> mListNew;
    protected List<T> mListOld;

    @Override
    public int getNewListSize() {
        return mListNew.size();
    }

    @Override
    public int getOldListSize() {
        return mListOld.size();
    }

    public void setListNew(final List<T> listNew) {
        mListNew = listNew;
    }

    public void setListOld(final List<T> listOld) {
        mListOld = listOld;
    }
}
