package com.tud.kom.parkinglot.model;

import com.tud.kom.parkinglot.model.parser.ReservationParser;
import com.tud.kom.parkinglot.utils.ExternalFileUtils;

import junit.framework.Assert;

import org.json.JSONException;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;

public class ParserTest {
    @Test
    public void checkParseMaximal() {
        String jsonString = ExternalFileUtils.getFileFromPath(this, "parserTest/reservationsMaximal.json");
        try {
            ArrayList<Reservation> parsed = ReservationParser.parseReservationsArray(jsonString);
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
            Assert.fail("exception in parsing");
        }
    }

    @Test
    public void checkParseMinimal() {
        String jsonString = ExternalFileUtils.getFileFromPath(this, "parserTest/reservationsMinimal.json");
        try {
            ArrayList<Reservation> parsed = ReservationParser.parseReservationsArray(jsonString);
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
            Assert.fail("exception in parsing");
        }
    }
}
