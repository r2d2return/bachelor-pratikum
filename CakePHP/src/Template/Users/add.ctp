
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Users') ?></h1>
        <ul class="side-nav">
            <li class="btn btn-default"><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add User
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <?= $this->Form->create($user) ?>
                        <fieldset>
                            <legend><?= __('Add User') ?></legend>
                            <?php
                                if($admin == 1) {
                                    echo $this->Form->label('Role:');
                                    echo "<div class='form-group' label='Role :'>".$this->Form->select('role', $roles, array('class' => 'form-control', 'required' => true))."</div>";
                                    echo $this->Form->label('Zone:');
                                    echo "<div class='form-group'>".$this->Form->select('zone', $zones, ['class' => 'form-control', 'required' => true])."</div>";
                                    echo $this->Form->label('Tenant:');
                                    echo "<div class='form-group'>".$this->Form->select('tenant', $tenants, ['class' => 'form-control', 'required' => true])."</div>";
                                } elseif ($reservationManager == 1) {
                                echo $this->Form->label('Role:');
                                    echo "<div class='form-group'>".$this->Form->select('role', $reservationQuery, ['label' => 'Role:', 'class' => 'form-control', 'required' => true])."</div>";
                                } elseif ($facilityManager == 1) {
                                    echo $this->Form->label('Role:');
                                    echo "<div class='form-group'>".$this->Form->select('role', $facilityQuery, ['label' => 'Role:', 'class' => 'form-control', 'required' => true])."</div>";
                                } else {
                                    echo "<div class='alert alert-warning'>
                                        <strong>You are not allowed to do this action!</strong>
                                    </div>";
                                }
                                echo "<div class='form-group'>".$this->Form->input('username', ['label' => 'Username:', 'class' => 'form-control', 'required' => true])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('password', ['label' => 'Password:', 'class' => 'form-control', 'required' => true])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('email', ['label' => 'Email:', 'class' => 'form-control', 'required' => true])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('firstname', ['label' => 'First Name:', 'class' => 'form-control', 'required' => true])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('lastname', ['label' => 'Last Name:', 'class' => 'form-control', 'required' => true])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('mobileNumber', ['label' => 'Mobile Number:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('landlineNumber', ['label' => 'Landline Number:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('title', ['label' => 'Title:', 'class' => 'form-control'])."</div>";
                                echo $this->Form->label('Gender:');
                                echo "<div class='form-group'>".$this->Form->select('gender', $gender, ['class' => 'form-control'])."</div>";
                                echo $this->Form->label('State:');
                                echo "<div class='form-group'>".$this->Form->select('userState', $state, ['class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('company', ['label' => 'Company:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('department', ['label' => 'Department:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('street', ['label' => 'Street:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('ZIP', ['label' => 'Zip:', 'class' => 'form-control'])."</div>";
                            ?>
                        </fieldset>
                        <?= $this->Form->button('Submit',  array('class' => 'btn btn-default')); ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php $this->Html->addCrumb('Users', '/users'); ?>
<?php $this->Html->addCrumb('Add', ['controller' => 'Users', 'action' => 'add']); ?>

