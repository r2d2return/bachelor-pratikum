package com.tud.kom.parkinglot.model;

import android.content.Context;
import android.text.format.DateFormat;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class Reservation implements Serializable {
    private Date endTime;
    private int id;
    private ParkingLot parkingLot;
    private Date startTime;
    private String token;
    // muss nicht zum neu createn gesetzt werden
    private boolean notified;

    // licensePlate
    // reservation state
    // messageInfo
    // created

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Reservation that = (Reservation) o;

        if (id != that.id) {
            return false;
        }
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) {
            return false;
        }
        if (parkingLot != null ? !parkingLot.equals(that.parkingLot) : that.parkingLot != null) {
            return false;
        }
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) {
            return false;
        }
        return token != null ? token.equals(that.token) : that.token == null;
    }

    public String getReservationTimePeriod(Context context) {
        String startDate = DateFormat.getMediumDateFormat(context).format(startTime);
        String endDate = DateFormat.getMediumDateFormat(context).format(endTime);
        CharSequence startTimeString = DateFormat.getTimeFormat(context).format(startTime);
        CharSequence endTimeString = DateFormat.getTimeFormat(context).format(endTime);
        if (startDate.equals(endDate)) {
            endDate = "";
        }
        return String.format("%s %s - %s %s", startDate, startTimeString, endDate, endTimeString);
    }

    public boolean isActive() {
        long nowTime = System.currentTimeMillis();
        return nowTime >= startTime.getTime() && nowTime <= endTime.getTime();
    }

    @Override
    public String toString() {
        return "Reservation{" + "endTime=" + endTime + ", id=" + id + ", parkingLot=" + parkingLot + ", startTime=" + startTime + ", token='" + token + '\'' + ", notified=" + notified + '}';
    }

    @Override
    public int hashCode() {
        int result = endTime != null ? endTime.hashCode() : 0;
        result = 31 * result + id;
        result = 31 * result + (parkingLot != null ? parkingLot.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (token != null ? token.hashCode() : 0);
        return result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ParkingLot getParkingLot() {
        return parkingLot;
    }

    public void setParkingLot(ParkingLot parkinglotPropsal) {
        this.parkingLot = parkinglotPropsal;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Timestamp getEndTime() {
        return new Timestamp(endTime.getTime());
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Timestamp getStartTime() {
        return new Timestamp(startTime.getTime());
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public boolean isNotified() {
        return notified;
    }

    public void setNotified(boolean notified) {
        this.notified = notified;
    }
}
