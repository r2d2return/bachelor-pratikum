<div>
	<table style="margin: auto; text-align:left;">
		<tr>
			<td style="font-weight:bold;">User:</td>
			<td><?= h($reservation->user->username) ?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;">Assigned parking lot:</td>
			<td><?= h($reservation->parkinglot->description) ?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;">booking start:</td>
			<td><?= h($reservation->start) ?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;">booking end:</td>
			<td><?= h($reservation->end) ?></td>
		</tr>
		<?php
			if ($isActive) {

		?>

                <tr>
                    <td style="font-weight: bold;">Current state:</td><td><span id="state<?php echo $reservation->parkinglot->pid ?>">unknown</span></td>
                </tr>

		<tr>
		  <td colspan="2">
		      <div>
		          <button id="openButton<?php echo $reservation->parkinglot->pid ?>" class="button" style="background-color: rgb(0,230,0);display: none;" onClick="sendcmdOpen(<?php echo $reservation->parkinglot->pid . "," . $reservation->parkinglot->gateway_id ?>)" >free</button> <br />
                  <button id="closeButton<?php echo $reservation->parkinglot->pid ?>" class="button" style="background-color: rgb(230,0,0);display: none;" onClick="sendcmdClose(<?php echo $reservation->parkinglot->pid . "," . $reservation->parkinglot->gateway_id ?>)">block</button><br />
              </div>
		  </td>
		</tr>
		
	    <?php } else {
	    ?>
	    <tr>
			<td colspan="2" style="text-align:center;background-color:rgb(230,230,230);font-weight:bold;font-size:1.5em;">Sorry, booking not active yet.</td>
	    </tr>
	    <?php } 
	    ?>
	</table>
	<div align="center" style="width:100%;"><span id="status" align="center" style="text-align:center;align:center;"></span></div>
	<div align="center" style="width:100%;">
	z: <span id="zValue<?php echo $reservation->parkinglot->pid ?>" align="center" style="text-align:center;align:center;"></span><br />
	zThresh: <span id="zThresh<?php echo $reservation->parkinglot->pid ?>" align="center" style="text-align:center;align:center;"></div>
	
</div>
<script type="text/javascript">
          $(document).ready(function() {
            MQTTconnect();
            setTimeout(function() { 
                sendcmdRefresh(<?= h($reservation->parkinglot->pid) ?>);
            },1900);
          });
</script>
<?php $this->Html->addCrumb('Parking', ['controller' => 'Parking', 'action' => 'index']); ?>