<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $email
 * @property string $mobileNumber
 * @property string $landlineNumber
 * @property string $firstname
 * @property string $lastname
 * @property string $title
 * @property string $gender
 * @property string $userState
 * @property string $company
 * @property string $department
 * @property string $street
 * @property string $ZIP
 *
 * @property \App\Model\Entity\DatabaseLog[] $database_logs
 * @property \App\Model\Entity\Tenant[] $tenants
 * @property \App\Model\Entity\Role[] $roles
 * @property \App\Model\Entity\Zone[] $zones
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
    protected function _setPassword($password) {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }
}
