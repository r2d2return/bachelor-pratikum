<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Sensorlogs'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Parkinglots'), ['controller' => 'Parkinglots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Reservations'), ['controller' => 'Reservations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
    </ul>
</div>
<div class="sensorlogs index large-10 medium-9 columns">
	<h4 style="margin-top:1em";> statistics for parkinglot with pid: 
		<?= $this->Html->link($parkinglot_id, 
			['controller' => 'Parkinglots', 'action' => 'view', 
			$parkinglot_id]) 
		?>
	</h4>
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('x') ?></th>
            <th><?= $this->Paginator->sort('y') ?></th>
            <th><?= $this->Paginator->sort('z') ?></th>
            <th><?= $this->Paginator->sort('logtime') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($allSensorlogsForLot as $sensorlog): ?>
        <tr>
            <td><?= $this->Number->format($sensorlog->id) ?></td>
            <td><?= $this->Number->format($sensorlog->x) ?></td>
            <td><?= $this->Number->format($sensorlog->y) ?></td>
            <td><?= $this->Number->format($sensorlog->z) ?></td>
            <td><?= h($sensorlog->logtime->i18nFormat('dd.MM.YYYY HH:mm:ss', 'Europe/Paris', 'de-DE')) ?></td>
            <td class="actions">
            	<?= $this->Html->image('edit.png', [ 'alt' => __('Edit'), 
                								     'url' => ['controller' => 'Sensorlogs', 
                								     		   'action' => 'edit', $sensorlog->id]
                								   ]) ?>
			     <?= $this->Html->link(
                					  $this->Html->image('delete.png', [ 'alt' => __('Delete')]),
                					  ['controller' => 'Sensorlogs', 'action' => 'delete', $sensorlog->id],
                					  ['confirm' => __('Are you sure you want to delete # {0}?', $sensorlog->id), 
                					   'escapeTitle' => false]
                				     ) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<?php $this->Html->addCrumb('Screenlogs', '/screenlogs'); ?>
<?php $this->Html->addCrumb('Details', ['controller' => 'Screenlogs', 'action' => 'view']); ?>