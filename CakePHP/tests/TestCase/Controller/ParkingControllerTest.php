<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ParkingController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

use Cake\Auth\DefaultPasswordHasher;

use Cake\Datasource\Exception\RecordNotFoundException;
/**
 * App\Controller\SitesController Test Case
 */
class ParkingControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
     public $fixtures = [
        'app.reservations',
        'app.parkinglots',
        'app.gateways',
        'app.sites',
        'app.parkinglots_sites',
        'app.zones',
        'app.users',
        'app.database_logs',
        'app.tenants',
        'app.tenants_users',
        'app.roles',
        'app.auth_acl',
        'app.users_roles',
        'app.users_zones',
        'app.locations',
        'app.sensorlogs',
        'app.failed_login_attempts',
        ];


    /** 
        CONFIGURATION
        ===============================================================================
    */
        //Define which keys should be checked to be included/excluded from a response -> See "Schnittstellendefinition"
        public $json_return_keys = [
                            ];

        //Define which ROLES are allowed and not allowed to access the location /sites.json
         public $authorized_roles = ['Admin'];                  
         public $unauthorized_roles = [ 'Guest', 'Visitor', 'Facility Manager', 'Reservation Manager'];                   


        public $global_users;

        /*
        ======================================================================================
        */
        
        // HELPERS
        function doesContain($array){
            foreach ($array as $key) {
                $this->assertResponseContains($key);
            }
        }

        function doesNOTContain($array){
            foreach ($array as $key) {
                $this->assertResponseNOTContains($key);
            }
        }

    /**
       ===============================================================================
    */

    /**
     * Test open method 
     * Try to open the parking hoop with a non existing token
     * For further information check the documentation 
     * @return void
     */
    public function testOpenNonExistingToken()
    {
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not A User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        $this->get('/parking/open/NONEXISTING.json');
        

        //
        
        //debug($this->_response->body());
        //debug($this->_response);

        $this->assertResponseError();
        $this->assertResponseNotEmpty();
        $this->assertResponseCode(404);
        $this->assertResponseContains("Not Found");
    }
    

    /**
     * Test open method 
     * Try to open the parking hoop with a existing token that belongs to a reservation in the past
     * For further information check the documentation 
     * @return void
     */
    public function testOpenInvalidToken()
    {
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not A User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        
        $this->get('/parking/open/inValidToken1.json');
        

        //
        
        //debug($this->_response->body());
        $this->assertResponseError();
        $this->assertResponseNotEmpty();
        $this->assertResponseCode(404);
        $this->assertResponseContains('Please be aware that your reservation is only valid between');

    }

    /**
     * Test open method 
     * Try to open the parking hoop with a existing token but whoms coresponding reservation has not yet startet
     * For further information check the documentation 
     * @return void
     */
    public function testOpenInActiveToken()
    {
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not A User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        
        $this->get('/parking/open/inActiveToken1.json');
        

        //
        
        //echo($this->_response->body());
        //debug($this->_response);

        $this->assertResponseError();
        $this->assertResponseNotEmpty();
        $this->assertResponseCode(404);
        $this->assertResponseContains('Please be aware that your reservation is only valid between');
    }

    

    /**
     * Test open method 
     * Try to open the parking hoop with a existing token whoms coresponding reservation has startet
     * For further information check the documentation 
     * @return void
     */
    public function testOpenActiveToken()
    {
        
        //debug($user);
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not a User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        $this->get('/parking/open/activeToken1.json');
        //echo("\n\n". $this->_response->body()."\n\n");
        //debug($this->_response);
        $this->assertResponseOk();
        $this->assertResponseNotEmpty();
        $this->assertResponseCode(200);
        $this->assertResponseContains('"message":"Success"');

        //Check if all fields according to the "Schnittstellendefinition" are retrieved
        //$this->doesContain($this->json_return_keys);
        //------------------//
        
       
        
        $this->assertContentType('application/json');
        
        
    }

    /**
     * Test open method 
     * Try to open the parking hoop with a existing token whoms coresponding reservation has startet
     * For further information check the documentation 
     * @return void
     */
    public function testOpenActiveTokenHTML()
    {
        
        //debug($user);
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not a User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        $this->get('/parking/open/activeToken1');
        //echo("\n\n". $this->_response->body()."\n\n");
        debug($this->_response->body());
        $this->assertResponseOk();
        $this->assertResponseNotEmpty();
        $this->assertResponseCode(200);
        //$this->assertResponseContains('"message":"Success"');

        //Check if all fields according to the "Schnittstellendefinition" are retrieved
        //$this->doesContain($this->json_return_keys);
        //------------------//
        
    
        
        
    }

    /**
     * Test open method 
     * Try to open the parking hoop with no provided token
     * For further information check the documentation 
     * @return void
     */
    public function testOpenTokenNULL()
    {
        
        //debug($user);
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not a User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        $this->get('/parking/open/.json');

        //echo("\n\n".$this->_response->body()."\n\n");
        //debug($this->_response);
        
        $this->assertResponseError();
        $this->assertResponseCode(404);
        $this->assertResponseContains('Could not find valid token');
        
        $this->assertContentType('application/json');
        
        
    }

    /**
    ==============================================================Close=======================================================================
    */

    /**
     * Test close method 
     * Try to open the parking hoop with a non existing token
     * For further information check the documentation 
     * @return void
     */
    public function testCloseNonExistingToken()
    {
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not A User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        $this->get('/parking/close/NONEXISTING.json');
        

        //
        
        //debug($this->_response->body());
        //debug($this->_response);

        $this->assertResponseError();
        $this->assertResponseNotEmpty();
        $this->assertResponseCode(404);
        $this->assertResponseContains("Not Found");
    }
    

    /**
     * Test close method 
     * Try to open the parking hoop with a existing token that belongs to a reservation in the past
     * For further information check the documentation 
     * @return void
     */
    public function testCloseInvalidToken()
    {
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not A User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        
        $this->get('/parking/close/inValidToken1.json');
        

        //
        
        //debug($this->_response->body());
        $this->assertResponseError();
        $this->assertResponseNotEmpty();
        $this->assertResponseCode(404);
        $this->assertResponseContains('Please be aware that your reservation is only valid between');

    }

    /**
     * Test close method 
     * Try to open the parking hoop with a existing token but whoms coresponding reservation has not yet startet
     * For further information check the documentation 
     * @return void
     */
    public function testCloseInActiveToken()
    {
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not A User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        
        $this->get('/parking/close/inActiveToken1.json');
        

        //
        
        //echo($this->_response->body());
        //debug($this->_response);

        $this->assertResponseError();
        $this->assertResponseNotEmpty();
        $this->assertResponseCode(404);
        $this->assertResponseContains('Please be aware that your reservation is only valid between');
    }

    

    /**
     * Test close method 
     * Try to open the parking hoop with a existing token whoms coresponding reservation has startet
     * For further information check the documentation 
     * @return void
     */
    public function testCloseActiveToken()
    {
        
        //debug($user);
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not a User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        $this->get('/parking/close/activeToken1.json');
        //echo("\n\n". $this->_response->body()."\n\n");
        //debug($this->_response);
        $this->assertResponseOk();
        $this->assertResponseNotEmpty();
        $this->assertResponseCode(200);
        $this->assertResponseContains('"message":"Success"');

        //Check if all fields according to the "Schnittstellendefinition" are retrieved
        //$this->doesContain($this->json_return_keys);
        //------------------//
        
       
        
        $this->assertContentType('application/json');
        
        
    }


    /**
     * Test close method 
     * Try to open the parking hoop with no provided token
     * For further information check the documentation 
     * @return void
     */
    public function testCloseTokenNULL()
    {
        
        //debug($user);
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not a User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        $this->get('/parking/close/.json');

        //echo("\n\n".$this->_response->body()."\n\n");
        //debug($this->_response);
        
        $this->assertResponseError();
        $this->assertResponseCode(404);
        $this->assertResponseContains('Could not find valid token');
        
        $this->assertContentType('application/json');
        
        
    }

    /**
     * Test view method
     *
     * @return void
     
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    */
}
