<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Gateways'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sites'), ['controller' => 'Sites', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Site'), ['controller' => 'Sites', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Parkinglots'), ['controller' => 'Parkinglots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Parkinglot'), ['controller' => 'Parkinglots', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="gateways form large-10 medium-9 columns">
    <?= $this->Form->create($gateway) ?>
    <fieldset>
        <legend><?= __('Add Gateway') ?></legend>
        <?php
            echo $this->Form->input('label');
            echo $this->Form->input('fqdn');
            echo $this->Form->input('site_id', ['options' => $sites, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<?php $this->Html->addCrumb('Gateways', '/gateways'); ?>
<?php $this->Html->addCrumb('Add', ['controller' => 'Gateways', 'action' => 'add']); ?>