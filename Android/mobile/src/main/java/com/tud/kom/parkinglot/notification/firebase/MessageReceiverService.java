package com.tud.kom.parkinglot.notification.firebase;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.network.RequestAbstraction;
import com.tud.kom.parkinglot.notification.LocationServiceHelper;

public class MessageReceiverService extends FirebaseMessagingService {
    private static final String TAG = MessageReceiverService.class.getSimpleName();

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "Firebase Message Received!");
        try {
            prepareScheduleAlarm(remoteMessage.getData().get("user_id"), false);
        } catch (NullPointerException npe) {
            Log.w(TAG, "No UserID -> Firebase Message Ignored");
        }
    }

    /**
     * I HATE TESTS
     *
     * @param userIdFromMessage
     * @param testMode
     */
    public int prepareScheduleAlarm(String userIdFromMessage, boolean testMode) {
        String username = SavedPreferences.getPreference(this, SavedPreferences.Filename.USERNAME);
        String password = SavedPreferences.getPreference(this, SavedPreferences.Filename.PASSWORD);
        String uid = SavedPreferences.getPreference(this, SavedPreferences.Filename.UID);
        if (username == null || password == null || uid == null || !userIdFromMessage.equals(uid)) {
            Log.d(TAG, "User Is Not Logged In Anymore -> Delete InstanceId From Server");
            if (!testMode)
                RequestAbstraction.clearFirebaseInstanceId(null, FirebaseInstanceId.getInstance().getToken(), username);
            return 1;
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "No Location Permission -> Firebase Message Ignored");
            return 2;
        }
        LocationServiceHelper.scheduleNextAlarm(this, null, username, password);
        return 0;
    }
}