<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersZonesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersZonesTable Test Case
 */
class UsersZonesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersZonesTable
     */
    public $UsersZones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users_zones',
        'app.users',
        'app.database_logs',
        'app.tenants',
        'app.tenants_users',
        'app.roles',
        'app.auth_acl',
        'app.users_roles',
        'app.zones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UsersZones') ? [] : ['className' => UsersZonesTable::class];
        $this->UsersZones = TableRegistry::get('UsersZones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersZones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
