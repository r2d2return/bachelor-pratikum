<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

/**
 * Reservations 
 * manages the users of the smart parking system
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class ReservationManagerController extends AppController
{
	var $uses = false;

	public function index()
	{

		$userRoles = $this->Users->getRoles($this->Auth->user('id'));


		$users = $this->paginate('Users', [ 'contain' => ['Roles']]);

		$this->set('users', $this->Users->find('all', [ 'contain' => [ 'Roles' ]]));
		$this->set('users', $this->paginate($this->Users));

		$this->set(compact('users'));
	}
	
	/**
	 * View method
	 *
	 * @param string|null $id User id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		if (!$id) {
			throw new NotFoundException(__('Invalid user'));
		}
		
		$user = $this->Users->get($id, [
				'contain' => ['Roles']
		]);

		$this->set('user', $user);
		$this->set('_serialize', ['user']);
	}
	
	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{


		$user = $this->Users->newEntity(['associated' => ['Roles']]);

		if ($this->request->is('post')) {
			$user = $this->Users->patchEntity($user, $this->request->data);

			$roles2 = $this->Users->Roles->find()->where(function ($exp, $q) {
        	return $exp->in('Roles.id', $this->request->data['role']);
    		});

			$user->roles = $roles2->toArray();
			if ($this->Users->save($user)) {

				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		}

		$roles = $this->Users->Roles->find('list');

		$this->set(compact('user', 'roles'));
		$this->set('_serialize', ['user'], ['roles']);
	}
	
	/**
	 * Edit method
	 *
	 * @param string|null $id User id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$user = $this->Users->get($id, [
				'contain' => ['Roles']
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$user = $this->Users->patchEntity($user, $this->request->data);

			$roles2 = $this->Users->Roles->find()->where(function ($exp, $q) {
        	return $exp->in('Roles.id', $this->request->data['role']);
    		});
			$user->roles = $roles2->toArray();

			if ($this->Users->save($user)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		}
		$roles = $this->Users->Roles->find('list');

		$this->set(compact('user', 'roles'));
		$this->set('_serialize', ['user']);
	}
	
	/**
	 * Delete method
	 *
	 * @param string|null $id User id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$user = $this->Users->get($id);
		if ($this->Users->delete($user)) {
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
