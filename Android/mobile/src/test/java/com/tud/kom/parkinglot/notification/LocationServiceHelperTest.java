package com.tud.kom.parkinglot.notification;

import com.tud.kom.parkinglot.model.Reservation;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;

public class LocationServiceHelperTest {
    @Test
    public void nextResForAlarm1() {
        Assert.assertNull(LocationServiceHelper.nextReservationForAlarm(null));
    }

    @Test
    public void nextResForAlarm2() {
        ArrayList<Reservation> reservations = new ArrayList<>();
        Assert.assertNull(LocationServiceHelper.nextReservationForAlarm(reservations));
    }

    @Test
    public void nextResForAlarm3() {
        ArrayList<Reservation> reservations = new ArrayList<>();
        Reservation resWithNotified = new Reservation();
        resWithNotified.setNotified(true);
        reservations.add(resWithNotified);
        Reservation resWithoutNotified = new Reservation();
        reservations.add(resWithoutNotified);
        Assert.assertEquals(resWithoutNotified, LocationServiceHelper.nextReservationForAlarm(reservations));
    }
}
