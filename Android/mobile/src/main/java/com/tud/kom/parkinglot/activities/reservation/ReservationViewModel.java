package com.tud.kom.parkinglot.activities.reservation;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.tud.kom.parkinglot.model.ParkingLot;
import com.tud.kom.parkinglot.model.Reservation;
import com.tud.kom.parkinglot.model.Site;
import com.tud.kom.parkinglot.model.Zone;
import com.tud.kom.parkinglot.network.NetworkCallback;
import com.tud.kom.parkinglot.network.RequestAbstraction;
import com.tud.kom.parkinglot.network.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReservationViewModel extends ViewModel implements NetworkCallback {

    public static final String RESERVATION_SAVED = "Reservation saved";
    private static final String TAG = ReservationViewModel.class.getName();
    private MutableLiveData<Calendar> mEndCalendar;
    private MutableLiveData<String> mError;
    private MutableLiveData<List<ParkingLot>> mParkingLots;
    private MutableLiveData<List<Site>> mSites;
    private MutableLiveData<Calendar> mStartCalendar;
    private MutableLiveData<List<Zone>> mZones;
    private String pw;
    private MutableLiveData<Reservation> reservation;
    private String username;

    public ReservationViewModel(String username, String pw) {
        this.username = username;
        this.pw = pw;
    }

    public LiveData<Calendar> getEndCalendar() {
        if (this.mEndCalendar == null) {
            this.mEndCalendar = new MutableLiveData<>();
            final Calendar endCalendar = Calendar.getInstance();
            endCalendar.add(Calendar.HOUR_OF_DAY, 1);
            this.mEndCalendar.setValue(endCalendar);
        }
        return this.mEndCalendar;
    }

    public LiveData<String> getError() {
        if (this.mError == null)
            this.mError = new MutableLiveData<>();
        return this.mError;
    }

    public LiveData<List<ParkingLot>> getParkingLots() {
        if (this.mParkingLots == null)
            this.mParkingLots = new MutableLiveData<>();
        return this.mParkingLots;
    }

    public LiveData<Reservation> getReservation() {
        if (this.reservation == null)
            this.reservation = new MutableLiveData<>();
        return this.reservation;
    }

    public LiveData<List<Site>> getSites() {
        if (this.mSites == null)
            this.mSites = new MutableLiveData<>();
        return this.mSites;
    }

    public LiveData<Calendar> getStartCalendar() {
        if (this.mStartCalendar == null) {
            this.mStartCalendar = new MutableLiveData<>();
            this.mStartCalendar.setValue(Calendar.getInstance());
        }
        return this.mStartCalendar;
    }

    public LiveData<List<Zone>> getZones() {
        if (this.mZones == null)
            this.mZones = new MutableLiveData<>();
        return this.mZones;
    }

    @Override
    public void handleWebsiteResponse(Response response) {
        if (response.getRespCode() != 200)
            this.mError.setValue("ErrorRespCodeNotAsExpected: " + response.getRespCode());
        else
            this.mError.setValue(RESERVATION_SAVED);
    }

    public void loadParkingLots(int siteId) {
        RequestAbstraction.getParkingLotsOfSite(this::onParkingLotResponse, this.username, this.pw, siteId);
    }

    public void loadSites(int zoneId) {
        RequestAbstraction.getSitesOfZone(this::onSiteResponse, this.username, this.pw, zoneId);
    }

    public void loadZones() {
        RequestAbstraction.getZones(this::onZoneResponse, this.username, this.pw);
    }

    public void sendReservation(final Reservation reservation) {
        RequestAbstraction.postReservation(reservation, this, this.username, this.pw);
    }

    public void setEndCalendar(final int year, final int month, final int dayOfMonth) {
        Calendar calendar = this.mEndCalendar.getValue();
        calendar.set(year, month, dayOfMonth);
        this.mEndCalendar.setValue(calendar);
    }

    public void setEndCalendar(final int hourOfDay, final int minute) {
        Calendar calendar = this.mEndCalendar.getValue();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        this.mEndCalendar.setValue(calendar);
    }

    public void setStartCalendar(final int year, final int month, final int day) {
        Calendar calendar = this.mStartCalendar.getValue();
        long diff = this.calculateDiff(calendar);
        calendar.set(year, month, day);
        this.mStartCalendar.setValue(calendar);
        Calendar newEndCalendar = Calendar.getInstance();
        newEndCalendar.setTimeInMillis(calendar.getTimeInMillis() + diff);
        this.mEndCalendar.setValue(newEndCalendar);
    }

    public void setStartCalendar(final int hourOfDay, final int minute) {
        Calendar calendar = this.mStartCalendar.getValue();
        long diff = this.calculateDiff(calendar);
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        this.mStartCalendar.setValue(calendar);

        Calendar newEndCalendar = Calendar.getInstance();
        newEndCalendar.setTimeInMillis(calendar.getTimeInMillis() + diff);
        this.mEndCalendar.setValue(newEndCalendar);
    }

    private long calculateDiff(Calendar startTime) {
        final long endTime = this.mEndCalendar.getValue().getTimeInMillis();
        return endTime - startTime.getTimeInMillis();
    }

    public void onParkingLotResponse(final Response response) {
        if (response.getRespCode() != 200)
            this.mError.setValue("ErrorRespCodeNotAsExpected: " + response.getRespCode());
        else {
            try {
                JSONArray jsonArray = new JSONArray(response.getResp());
                List<ParkingLot> parkingLots = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonParkingLot = jsonArray.getJSONObject(i);
                    ParkingLot parkinglotPropsal = new ParkingLot();
                    parkinglotPropsal.setId(jsonParkingLot.getInt("id"));
                    parkinglotPropsal.setName(jsonParkingLot.getString("description"));
                    parkingLots.add(parkinglotPropsal);
                }
                this.mParkingLots.setValue(parkingLots);
            } catch (JSONException e) {
                this.mError.setValue("ErrorJsonCouldntBeParsed: " + e.getMessage());
            }
        }
    }

    public void onSiteResponse(Response response) {
        if (response.getRespCode() != 200)
            this.mError.setValue("ErrorRespCodeNotAsExpected: " + response.getRespCode());
        else {
            try {
                JSONArray jsonArray = new JSONArray(response.getResp());
                List<Site> sites = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonSite = jsonArray.getJSONObject(i);
                    Site site = new Site();
                    site.setId(jsonSite.getInt("id"));
                    site.setName(jsonSite.getString("name"));
                    sites.add(site);
                }
                this.mSites.setValue(sites);
            } catch (JSONException e) {
                this.mError.setValue("ErrorJsonCouldntBeParsed: " + e.getMessage());
            }
        }
    }

    public void onZoneResponse(Response response) {
        if (response.getRespCode() != 200)
            this.mError.setValue("ErrorRespCodeNotAsExpected: " + response.getRespCode());
        else {
            try {
                JSONArray jsonArray = new JSONArray(response.getResp());
                List<Zone> zones = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonSite = jsonArray.getJSONObject(i);
                    Zone zone = new Zone();
                    zone.setId(jsonSite.getInt("id"));
                    zone.setName(jsonSite.getString("name"));
                    zones.add(zone);
                }
                this.mZones.setValue(zones);
            } catch (JSONException e) {
                this.mError.setValue("ErrorJsonCouldntBeParsed: " + e.getMessage());
            }
        }
    }
}
