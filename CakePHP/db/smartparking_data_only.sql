-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 22. Feb 2018 um 17:14
-- Server-Version: 10.1.26-MariaDB-0+deb9u1
-- PHP-Version: 7.0.27-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Daten für Tabelle `auth_acl`
--

INSERT INTO `auth_acl` (`id`, `controller`, `actions`, `role_id`) VALUES
(1, 'Reservations', '*', 1),
(2, 'Users', 'index', 2),
(3, 'Users', '*', 1),
(4, 'Dashboard', '*', 1),
(5, 'RemoteKeys', '*', 1),
(6, 'AuthAcl', '*', 1),
(7, 'Maps', '*', 1),
(8, 'Roles', '*', 1),
(9, 'Users', '*', 4),
(10, 'DatabaseLogs', '*', 1),
(13, 'Reservations', '*', 4),
(14, 'Dashboard', '*', 4),
(15, 'Reservations', '*', 3),
(18, 'Users', 'index, getUser', 3),
(19, 'Zones', 'index, view', 3),
(20, 'Sites', 'index, view', 3),
(21, 'Parkinglots', 'index, view', 3),
(22, 'Parkinglots', '*', 1);

--
-- Daten für Tabelle `locations`
--

INSERT INTO `locations` (`id`, `description`, `type`, `upperLeftLongitude`, `upperLeftLatitude`, `upperRightLongitude`, `upperRightLatitude`, `lowerLeftLongitude`, `lowerLeftLatitude`, `lowerRightLongitude`, `lowerRightLatitude`, `width`, `length`, `imagePath`) VALUES
(1, 'parking/13', 'SQARE', 49.874404, 8.66052, 49.874395, 8.66046, 49.874376, 8.660467, 49.874386, 8.660526, NULL, NULL, 'NULL'),
(2, 'parking/14', 'SQARE', 49.874428, 8.660471, 49.874428, 8.660471, 49.874428, 8.660471, 49.874428, 8.660471, NULL, NULL, 'NULL'),
(3, 'parking/15', 'SQARE', 49.874416, 8.66047, 49.874416, 8.66047, 49.874416, 8.66047, 49.874416, 8.66047, NULL, NULL, 'NULL'),
(4, 'parking/16', 'SQARE', 49.874335, 8.660485, 49.874335, 8.660485, 49.874335, 8.660485, 49.874335, 8.660485, NULL, NULL, 'NULL'),
(5, 'parking/17', 'SQARE', 49.874241, 8.660506, 49.874241, 8.660506, 49.874241, 8.660506, 49.874241, 8.660506, NULL, NULL, 'NULL'),
(6, 'parking/18', 'SQARE', 49.874404, 8.66052, 49.874395, 8.66046, 49.874376, 8.660467, 49.874386, 8.660526, NULL, NULL, 'NULL'),
(7, 'parking/19', 'SQARE', 49.874428, 8.660471, 49.874428, 8.660471, 49.874428, 8.660471, 49.874428, 8.660471, NULL, NULL, 'NULL'),
(8, 'parking/20', 'SQARE', 49.874416, 8.66047, 49.874416, 8.66047, 49.874416, 8.66047, 49.874416, 8.66047, NULL, NULL, 'NULL'),
(9, 'parking/21', 'SQARE', 49.874335, 8.660485, 49.874335, 8.660485, 49.874335, 8.660485, 49.874335, 8.660485, NULL, NULL, 'NULL'),
(10, 'parking/22', 'SQARE', 49.874241, 8.660506, 49.874241, 8.660506, 49.874241, 8.660506, 49.874241, 8.660506, NULL, NULL, 'NULL');

-- --------------------------------------------------------

--
-- Daten für Tabelle `parkinglots`
--

INSERT INTO `parkinglots` (`id`, `pid`, `description`, `mqttchannel`, `gateway_id`, `status`, `ip`, `hwid`, `lastStateChange`, `type`, `locationId`) VALUES
(1, 13, 'S3|19 P7', 'parking/13', 1, 'blocked', '192.168.10.51', '00-aa-bb-cc-de-02', '2017-09-11 17:48:14', '', 1),
(2, 14, 'S3|19 P8', 'parking/14', 1, 'blocked', '192.168.10.52', '00-aa-bb-cc-de-03', '2018-02-22 15:44:19', '', 2),
(3, 15, 'S3|19 P9', 'parking/15', 1, 'blocked', '192.168.10.53', '00-aa-bb-cc-de-04', '2018-02-22 15:44:19', '', 3),
(4, 16, 'S3|19 P10', 'parking/16', 1, 'blocked', '192.168.10.54', '00-aa-bb-cc-de-05', '2018-02-22 15:44:19', '', 4),
(5, 17, 'S3|19 P11', 'parking/17', 1, 'blocked', '192.168.10.55', '00-aa-bb-cc-de-06', '2018-02-22 15:44:19', '', 5),
(6, 18, 'S3|19 P12', 'parking/18', 1, 'blocked', '192.168.10.56', '00-aa-bb-cc-de-06', '2018-02-22 16:04:17', '', 6),
(7, 19, 'S3|19 P13', 'parking/19', 1, 'blocked', '192.168.10.57', '00-aa-bb-cc-de-07', '2018-02-22 16:04:40', '', 7),
(8, 20, 'S3|19 P14', 'parking/20', 1, 'blocked', '192.168.10.58', '00-aa-bb-cc-de-08', '2018-02-22 16:04:40', '', 8),
(9, 21, 'S3|19 P15', 'parking/21', 1, 'blocked', '192.168.10.59', '00-aa-bb-cc-de-09', '2018-02-22 16:04:40', '', 9),
(10, 22, 'S3|19 P16', 'parking/22', 1, 'blocked', '192.168.10.60', '00-aa-bb-cc-de-10', '2018-02-22 16:04:40', '', 10);

-- --------------------------------------------------------

--
-- Daten für Tabelle `parkinglots_sites`
--

INSERT INTO `parkinglots_sites` (`id`, `sitesId`, `parkingLotsId`) VALUES
(1, 5, 1),
(2, 5, 2),
(3, 5, 3),
(4, 5, 4),
(5, 5, 5),
(6, 6, 6),
(7, 6, 7),
(8, 6, 8),
(9, 6, 9),
(10, 6, 10);

-- --------------------------------------------------------


--
-- Daten für Tabelle `reservations`
--

INSERT INTO `reservations` (`id`, `token`, `start`, `end`, `parkinglot_id`, `users_id`, `visitedPersonId`, `licencePlate`, `reservationState`, `messageInfo`, `isRecurring`, `recurringID`, `created`) VALUES
(39, 'e39b96b7be3d5c89fc9011432e85cc49', '2018-04-02 08:00:00', '2018-04-06 18:00:00', 1, 57, 57, 'TE-ST 7', 'RESERVED', 'GENERIC_MESSAGE', NULL, NULL, '2018-02-22 16:41:31'),
(40, 'db22d8464d728a7e826361d6c75fbea8', '2018-04-02 08:00:00', '2018-04-06 18:00:00', 2, 57, 57, 'TE-ST 8', 'RESERVED', 'GENERIC_MESSAGE', NULL, NULL, '2018-02-22 16:45:46'),
(41, '42ac3b28ea1f1b5c283e7e40ac5e337e', '2018-04-02 08:00:00', '2018-04-06 18:00:00', 3, 57, 57, 'TE-ST 9', 'RESERVED', 'GENERIC_MESSAGE', NULL, NULL, '2018-02-22 16:46:43'),
(42, '45fc49ca58ba3977075e5e7dded5df9c', '2018-04-02 08:00:00', '2018-04-06 18:00:00', 4, 57, 57, 'TE-ST 10', 'RESERVED', 'GENERIC_MESSAGE', NULL, NULL, '2018-02-22 16:47:20'),
(43, '0ed32a3841dfe4f086684425508dc01f', '2018-04-02 08:00:00', '2018-04-06 18:00:00', 5, 57, 57, 'TE-ST 11', 'RESERVED', 'GENERIC_MESSAGE', NULL, NULL, '2018-02-22 16:47:48'),
(44, '5cdec4c73e6dabb2e530fca59a0da610', '2018-02-23 08:00:00', '2018-02-23 18:00:00', 4, 58, 58, 'TE-ST 10', 'RESERVED', 'GENERIC_MESSAGE', NULL, NULL, '2018-02-22 16:52:24');

-- --------------------------------------------------------

--
-- Daten für Tabelle `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Guest'),
(3, 'Visitor'),
(4, 'Reservation Manager'),
(5, 'Facility Manager');

-- --------------------------------------------------------

--
-- Daten für Tabelle `sites`
--

INSERT INTO `sites` (`id`, `name`, `description`, `prefix`, `locationID`, `isPublic`, `publicStart`, `publicEnd`, `isPublicRecurring`, `recurringId`) VALUES
(1, 'KOM / Dez. V', 'NULL', 'kom_', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(2, 'Lichtwiese', 'NULL', 'NULL', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(3, 'Lab', 'NULL', 'NULL', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(4, 'MST', 'NULL', 'NULL', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(5, 'KOM_Smart_App1', NULL, NULL, 1, 'NO', NULL, NULL, NULL, NULL),
(6, 'KOM_Smart_App2', NULL, NULL, 5, 'NO', NULL, NULL, NULL, NULL),
(7, 'KOM_Smart_Facility_Manager1', NULL, NULL, 1, 'NO', NULL, NULL, NULL, NULL),
(8, 'KOM_Smart_Facility_Manager2', NULL, NULL, 1, 'NO', NULL, NULL, NULL, NULL),
(9, 'KOM_Smart_Broker1', NULL, NULL, 1, 'NO', NULL, NULL, NULL, NULL),
(10, 'KOM_Smart_Broker2', NULL, NULL, 1, 'NO', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Daten für Tabelle `tenants`
--

INSERT INTO `tenants` (`id`, `name`, `label`) VALUES
(1, 'TU Darmstadt', 'TUD'),
(2, 'Some Consultant', 'SCON'),
(3, 'KOM_Student_Group', 'KOMSG');

-- --------------------------------------------------------

--
-- Daten für Tabelle `tenants_users`
--

INSERT INTO `tenants_users` (`id`, `tenant_id`, `user_id`) VALUES
(1, 1, 3),
(8, 1, 56),
(9, 1, 57),
(10, 1, 58);

-- --------------------------------------------------------

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created`, `modified`, `email`, `mobileNumber`, `landlineNumber`, `firstname`, `lastname`, `title`, `gender`, `userState`, `company`, `department`, `street`, `ZIP`) VALUES
(3, 'smart', '$2y$10$b2lie4vTGXfZDxm/jsD27uQOXdIZqe2eBLStIk4KV8CXMt9.4iLUG', '2015-07-02 22:23:43', '2017-08-22 09:25:22', 'borhansafa@yahoo.com', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'ACTIVE', 'NULL', 'NULL', 'NULL', 'NULL'),
(56, 'admin', '$2y$10$IiURaYPtpfJc6GLPPiXa8.wSStnfVDFFniFP2gS1MLgRDymJm7VVK', '2018-02-22 16:07:54', '2018-02-22 16:07:54', 'admin@admin.de', '', '', 'Admin', 'Administrator', '', 'Male', 'ACTIVE', '', '', '', ''),
(57, 'app', '$2y$10$CXYxuyV9W7kBmLXHcNwjeOjmsSbHbYykf9sRQfvu6wycziKkBeG6O', '2018-02-22 16:09:30', '2018-02-22 16:09:30', 'app@app.de', '', '', 'Android', 'App', '', 'other', 'ACTIVE', '', '', '', ''),
(58, 'dennis', '$2y$10$9BzCizUMjkdqMShSw.7g6e8EOXx3jcMxL8GB7XVgH3khlrhNVdIey', '2018-02-22 16:49:06', '2018-02-22 16:53:04', 'dennis.droessler@stud.tu-darmstadt.de', '', '', 'Dennis', 'Droessler', '', 'Male', 'ACTIVE', '', '', '', '');

-- --------------------------------------------------------

--
-- Daten für Tabelle `users_roles`
--

INSERT INTO `users_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 3, 1),
(88, 56, 1),
(89, 57, 3),
(91, 58, 1);

-- --------------------------------------------------------

--
-- Daten für Tabelle `users_zones`
--

INSERT INTO `users_zones` (`id`, `user_id`, `zone_id`, `keyName`, `start`, `end`) VALUES
(1, 3, 1, 'NULL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 56, 1, NULL, NULL, NULL),
(9, 57, 1, NULL, NULL, NULL),
(10, 58, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Daten für Tabelle `zones`
--

INSERT INTO `zones` (`id`, `name`, `description`) VALUES
(1, 'kom', 'KOM Parking Zone P5-P7'),
(2, 'Bibliothek', 'p20 -p 30'),
(3, 'KOM_Smart_App', 'Smartparking App Group Zone'),
(4, 'KOM_Smart_Facility_Manager', 'Facility Manager Group'),
(5, 'KOM_Smart_Broker', 'Smartparking Broker Group');

-- --------------------------------------------------------

--
-- Daten für Tabelle `zones_sites`
--

INSERT INTO `zones_sites` (`id`, `zonesId`, `sitesId`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 1, 5),
(4, 3, 5),
(5, 3, 6);
