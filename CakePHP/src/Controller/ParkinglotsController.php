<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Parkinglots Controller
 *
 * @property \App\Model\Table\ParkinglotsTable $Parkinglots
 */
class ParkinglotsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
    	$site = $this->request->getParam('site_id');
    	if ($site != null) {
    		$parkinglots = $this->Parkinglots->find()->matching('Sites', function ($q) {
    			return $q->where(['Sites.id' => $this->request->getParam('site_id')]);
    		});
    			
    		$this->set('parkinglots', $parkinglots);
    		$this->set('_serialize', ['parkinglots']);
    	}
    	else {
        // retrieve all parkinglots from database
        // and make available in view
    	$allParkinglots = $this->Parkinglots->find('all')->contain(['Sites', 'Gateways']);
    	$this->set('parkinglots', $this->paginate($allParkinglots));
        $this->set('_serialize', ['parkinglots']);
    	}
    }

    /**
     * View method
     *
     * @param string|null $id Parkinglot id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        // fetch record associated with id and make available in view
    	$parkinglot = $this->Parkinglots->get($id, [
            'contain' => ['Sites', 'Gateways']
        ]);
        $this->set('parkinglot', $parkinglot);
        $this->set('_serialize', 'parkinglot');
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
    	$parkinglot = $this->Parkinglots->newEntity();
        if ($this->request->is('post')) {
            $parkinglot = $this->Parkinglots->patchEntity($parkinglot, $this->request->data);
            if ($this->Parkinglots->save($parkinglot)) {
                $this->Flash->success(__('The parkinglot has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The parkinglot could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('parkinglot'));
        $this->set('_serialize', 'parkinglot');
    }

    /**
     * Edit method
     *
     * @param string|null $id Parkinglot id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $parkinglot = $this->Parkinglots->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $parkinglot = $this->Parkinglots->patchEntity($parkinglot, $this->request->data);
            if ($this->Parkinglots->save($parkinglot)) {
                $this->Flash->success(__('The parkinglot has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The parkinglot could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('parkinglot'));
        $this->set('_serialize', 'parkinglot');
    }

    /**
     * Delete method
     *
     * @param string|null $id Parkinglot id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $parkinglot = $this->Parkinglots->get($id);
        if ($this->Parkinglots->delete($parkinglot)) {
            $this->Flash->success(__('The parkinglot has been deleted.'));
        } else {
            $this->Flash->error(__('The parkinglot could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
