package com.tud.kom.parkinglot.network;

import com.tud.kom.parkinglot.model.Reservation;
import com.tud.kom.parkinglot.network.Request.RequestType;

/**
 * provides methods for network requests
 */
public abstract class RequestAbstraction {
    /**
     * post reservation request: POST request to reservations/add.json with authorization header
     *
     * @param reservation     the reservation that should be added
     * @param networkCallback callback for network response
     * @param username        name of user
     * @param password        password of user
     */
    public static void postReservation(Reservation reservation, NetworkCallback networkCallback, String username, String password) {
        String basicAuth = Request.getBase64EncodedAuth(username, password);
        Request req = new Request("reservations/add.json", RequestType.POST).addHeaderParam("Authorization", basicAuth);

        /*
        Params that HAVE TO be set to make a successful reservation
         */
        req.addBodyParam("start", reservation.getStartTime().toString());
        req.addBodyParam("end", reservation.getEndTime().toString());
        req.addBodyParam("parkinglot_id", String.valueOf(reservation.getParkingLot().getId()));
        req.addBodyParam("reservationState", "RESERVED");
        req.addBodyParam("licencePlate", "TE-ST-18"); //Typo in license plate is on purpose
        /*
        One of many OPTIONAL parameters that can be set but dont have to.
        For further information please refer to the Smartparking-Documentation
         */
        req.addBodyParam("messageInfo", "");

        new NetworkTask(networkCallback).execute(req);
    }

    /**
     * get reservation request: GET request to reservations.json with authorization header
     *
     * @param networkCallback callback for network response
     * @param username        name of user
     * @param password        password of user
     */
    public static void getReservations(NetworkCallback networkCallback, String username, String password) {
        String basicAuth = Request.getBase64EncodedAuth(username, password);
        Request req = new Request("reservations.json", Request.RequestType.GET).addHeaderParam("Authorization", basicAuth);
        new NetworkTask(networkCallback).execute(req);
    }

    /**
     * delete reservation request: DELETE request to reservations.json with authorization header and res_id in urlParams
     *
     * @param networkCallback callback for network response
     * @param username        name of user
     * @param password        password of user
     * @param id              the id of the reservation, that should be deleted
     */
    public static void deleteReservations(NetworkCallback networkCallback, String username, String password, int id) {
        String basicAuth = Request.getBase64EncodedAuth(username, password);
        Request req = new Request("reservations/delete.json", RequestType.DELETE).addUrlParam("res_id", String.valueOf(id)).addHeaderParam("Authorization", basicAuth);
        new NetworkTask(networkCallback).execute(req);
    }

    /**
     * get old reservations: GET request to reservations/history.json with authorization header
     *
     * @param callback callback for network response
     * @param username name of user
     * @param password password of user
     */
    public static void getHistory(final NetworkCallback callback, final String username, final String password) {
        String basicAuth = Request.getBase64EncodedAuth(username, password);
        Request req = new Request("reservations/history.json", Request.RequestType.GET).addHeaderParam("Authorization", basicAuth);
        new NetworkTask(callback).execute(req);
    }

    /**
     * get parkinglot request: GET request to parkinglots/getParkinglotsForSite.json with authorization header and site_id in urlParams
     *
     * @param networkCallback callback for network response
     * @param username        name of user
     * @param password        password of user
     * @param siteId          id of the site
     */
    public static void getParkingLotsOfSite(NetworkCallback networkCallback, String username, String password, int siteId) {
        String basicAuth = Request.getBase64EncodedAuth(username, password);
        Request req = new Request("parkinglots/getParkinglotsForSite.json", Request.RequestType.GET).addUrlParam("site_id", String.valueOf(siteId)).addHeaderParam("Authorization", basicAuth);
        new NetworkTask(networkCallback).execute(req);
    }

    /**
     * get sites request: GET request to sites/getSitesForZone.json with authorization header with authorization header and zone_id in urlParams
     *
     * @param networkCallback callback for network response
     * @param username        name of user
     * @param password        password of user
     * @param zoneId          id of zone
     */
    public static void getSitesOfZone(NetworkCallback networkCallback, String username, String password, int zoneId) {
        String basicAuth = Request.getBase64EncodedAuth(username, password);
        Request req = new Request("sites/getSitesForZone.json", Request.RequestType.GET).addUrlParam("zone_id", String.valueOf(zoneId)).addHeaderParam("Authorization", basicAuth);
        new NetworkTask(networkCallback).execute(req);
    }

    /**
     * get zones request: GET request to zones.json with authorization header
     *
     * @param networkCallback callback for network response
     * @param username        name of user
     * @param password        password of user
     */
    public static void getZones(NetworkCallback networkCallback, String username, String password) {
        String basicAuth = Request.getBase64EncodedAuth(username, password);
        Request req = new Request("zones.json", Request.RequestType.GET).addHeaderParam("Authorization", basicAuth);
        new NetworkTask(networkCallback).execute(req);
    }

    /**
     * login request: GET request to users/getUser.json with authorization header
     *
     * @param networkCallback callback for network response
     * @param username        name of user
     * @param password        password of user
     */
    public static void loginUser(NetworkCallback networkCallback, String username, String password) {
        String basicAuth = Request.getBase64EncodedAuth(username, password);
        Request req = new Request("users/getUser.json", Request.RequestType.GET).addHeaderParam("Authorization", basicAuth);
        new NetworkTask(networkCallback).execute(req);
    }

    /**
     * request to mark a reservation as notified: POST request to reservations/markAsNotified.json with authorization header and id in urlParams
     *
     * @param networkCallback callback for network response
     * @param reservation     the reservation, which notified-flag should be turned to true
     * @param username        name of user
     * @param password        password of user
     */
    public static void markAsNotified(NetworkCallback networkCallback, Reservation reservation, String username, String password) {
        String basicAuth = Request.getBase64EncodedAuth(username, password);
        Request request = new Request("reservations/markAsNotified.json", RequestType.POST).addBodyParam("id", String.valueOf(reservation.getId())).addHeaderParam("Authorization", basicAuth);
        new NetworkTask(networkCallback).execute(request);
    }

    /**
     * open parkinglot request: GET request to parking/open/%token%.json
     *
     * @param reservation     reservation, which contains the token for opening the parkingLot
     * @param networkCallback callback for network response
     */
    public static void openParkingLot(final Reservation reservation, NetworkCallback networkCallback) {
        Request request = new Request("parking/open/" + reservation.getToken() + ".json", RequestType.GET);
        new NetworkTask(networkCallback).execute(request);
    }

    /**
     * close parkinglot request: GET request to parking/close/%token%.json
     *
     * @param reservation     reservation, which contains the token for closing the parkingLot
     * @param networkCallback callback for network response
     */
    public static void closeParkingLot(final Reservation reservation, NetworkCallback networkCallback) {
        Request request = new Request("parking/close/" + reservation.getToken() + ".json", RequestType.GET);
        new NetworkTask(networkCallback).execute(request);
    }

    /**
     * request to add a firebase instanceId to the database: POST request to firebase/add.json with authorization header and token in urlParams
     *
     * @param networkCallback callback for network response
     * @param instanceId      the instanceId of the device
     * @param username        name of user
     * @param password        password of user
     */
    public static void sendFirebaseInstanceId(NetworkCallback networkCallback, String instanceId, String username, String password) {
        String basicAuth = Request.getBase64EncodedAuth(username, password);
        Request request = new Request("firebase/add.json", RequestType.POST).addBodyParam("token", instanceId).addHeaderParam("Authorization", basicAuth);
        // Request request = new Request("firebase/add.json", RequestType.PUT).addHeaderParam("token", instanceId).addHeaderParam("Authorization", basicAuth);
        new NetworkTask(networkCallback).execute(request);
    }

    /**
     * request to add a firebase instanceId to the database: POST request to firebase/delete.json with authorization header and token in urlParams
     *
     * @param networkCallback callback for network response
     * @param instanceId      the instanceId of the device
     * @param username        name of user
     */
    public static void clearFirebaseInstanceId(NetworkCallback networkCallback, String instanceId, String username) {
        Request request = new Request("firebase/delete.json", RequestType.POST).addBodyParam("token", instanceId).addBodyParam("username", username);
        new NetworkTask(networkCallback).execute(request);
    }
}