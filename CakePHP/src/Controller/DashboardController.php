<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\Network\Email\Email;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Database\Type;
use App\Model\Entity\Reservation;
use Cake\I18n\Time;

require_once '/var/www/smartparking/src/Controller/php_sam.php';

/**
 * Dashboard Controller
 */
class DashboardController extends AppController
{
	
    /**
     * Index method
     * List all registered Parkinglots with status
     * Status is updated live with websockets/mqtt
     *
     * @return void
     */
    public function index()
    {
        //print_r($_SESSION['Auth']['User']['id']);
    	// set layout to special dashboard layout
    	//$this->layout = 'dashboard';
    	$this->viewBuilder()->setLayout('dashboard');
    	
    	// retrieve all parkinglots from database
        // and make available in view
//     	$parkinglots = TableRegistry::get('parkinglots', [
//     			'contain' => ['Sites', 'Gateways']
//     	]);


        $usersId = $_SESSION['Auth']['User']['id'];
        $roles = TableRegistry::get('UsersRoles');
        $query = $roles
            ->find('all',['conditions' => ['UsersRoles.user_id' => $usersId]])
            ->select(['role_id']);
        $q = $this->paginate($query);

        foreach ($q as $r){
            $roleId = $r->role_id;
        }

        $roleNameTable = TableRegistry::get('Roles');
        $roleName = $roleNameTable->get($roleId);
        //print_r($roleName->name);exit;
        $zones = TableRegistry::get('Zones');
        if($roleName->name == "Reservation Manager"){
            $userZoneTable = TableRegistry::get('UsersZones');
            $userZone = $userZoneTable->find('all',['conditions' => ['UsersZones.user_id' => $usersId]]);
            $zoneNumber = $this->paginate($userZone);
            //print_r($zoneNumber);
            foreach ($zoneNumber as $numberOfZone){
                $zoneOfUser = $numberOfZone->zone_id;
                //print_r($zoneOfUser);
            }
            $zonesQuery = $zones->find('all',['conditions' => ['Zones.id' => $zoneOfUser]])
                ->contain(['Sites' => ['Parkinglots']]);
            $name = $roleName->name;
            $this->set('nameRole', $name);
            $this->set('_serialize', ['nameRole']);
        }else if($roleName->name == "Facility Manager"){
            $userZoneTable = TableRegistry::get('UsersZones');
            $userZone = $userZoneTable->find('all',['conditions' => ['UsersZones.user_id' => $usersId]]);
            $zoneNumber = $this->paginate($userZone);
            //print_r($zoneNumber);
            foreach ($zoneNumber as $numberOfZone){
                $zoneOfUser = $numberOfZone->zone_id;
                //print_r($zoneOfUser);
            }
            $zonesQuery = $zones->find('all',['conditions' => ['Zones.id' => $zoneOfUser]])
                ->contain(['Sites' => ['Parkinglots']]);
            $name = $roleName->name;
            $this->set('nameRole', $name);
            $this->set('_serialize', ['nameRole']);
        }else {
            $zonesQuery = $zones->find('all')
                ->contain(['Sites' => ['Parkinglots']]);
            $name = $roleName->name;
            $this->set('nameRole', $name);
            $this->set('_serialize', ['nameRole']);

        }



/**
    	$parkinglots = TableRegistry::get('Parkinglots');
		$query = $parkinglots->find('all', [
		    'limit' => 200
        ])

        ->contain([
        'Sites' => ['Zones' => ['conditions' => ['Zones.id' => 1]]],'Gateways'
    ]);

    //debug($query);exit;

***/
    	$this->set('parkinglots', $this->paginate($zonesQuery));
        $this->set('_serialize', ['parkinglots']);
    }
    
}
