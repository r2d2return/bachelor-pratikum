package com.tud.kom.parkinglot.activities.login;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.tud.kom.parkinglot.network.NetworkCallback;
import com.tud.kom.parkinglot.network.RequestAbstraction;
import com.tud.kom.parkinglot.network.Response;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * The Login View Model
 */
public class LoginViewModel extends ViewModel implements NetworkCallback {
    public static final String UNAUTHORIZED = "unauthorized";
    public static final String EMPTY_OR_INVALID_USERNAME = "invalid email";
    public static final String EMPTY_OR_INVALID_PWD = "invalid password";
    private static final String TAG = LoginViewModel.class.getSimpleName();
    private MutableLiveData<String> error;
    private MutableLiveData<Boolean> waitingForNetwork;

    /**
     * Either returns an existing LiveData Object that stores an error or a new MutableLiveData-Object to store it.
     *
     * @return The MutableLiveData Object that stores errors
     */
    public LiveData<String> getError() {
        if (error == null) {
            error = new MutableLiveData<>();
        }
        return error;
    }

    /**
     * Either returns an existing LiveData Object that stores information if we are waiting for the network or a new
     * MutableLiveData-Object to store it.
     *
     * @return The MutableLiveData Object that stores the information if we are waiting for the network
     */
    public LiveData<Boolean> getWaitingForNetwork() {
        if (waitingForNetwork == null) {
            waitingForNetwork = new MutableLiveData<>();
        }
        return waitingForNetwork;
    }

    /**
     * Sets waitingForNetwork LiveDataObject. Creates it, if it doesnt exist yet
     *
     * @param value new value
     */
    public void setWaitingForNetwork(boolean value) {
        if (waitingForNetwork == null) {
            waitingForNetwork = new MutableLiveData<>();
        }
        waitingForNetwork.setValue(value);
    }

    /**
     * callback is called, when network response of the login requests arrives.
     *
     * @param response response the network response
     */
    @Override
    public void handleWebsiteResponse(@NonNull Response response) {
        if (response.getRespCode() == 200) {
            try {
                error.setValue(Integer.toString(new JSONObject(response.getResp()).getInt("id")));
            } catch (JSONException e) {
                e.printStackTrace();
                waitingForNetwork.setValue(false);
                error.setValue("ErrorCouldNotParseID: " + response);
            }
        }
        else if (response.getRespCode() == 401) {
            waitingForNetwork.setValue(false);
            error.setValue(UNAUTHORIZED);
        }
        else {
            waitingForNetwork.setValue(false);
            error.setValue("ErrorRespCodeNotAsExpected: " + response);
        }
    }

    /**
     * Returns a new Request to the NetworkMock with username name and password pwd
     *
     * @param name The username that should be added to the request
     * @param pwd  The password that should be added to the request
     */
    public void login(String name, String pwd) {
        if (name.isEmpty()) {
            error.setValue(EMPTY_OR_INVALID_USERNAME);
            return;
        }
        if (pwd.isEmpty()) {
            error.setValue(EMPTY_OR_INVALID_PWD);
            return;
        }
        waitingForNetwork.setValue(true);
        RequestAbstraction.loginUser(this, name, pwd);
    }
}