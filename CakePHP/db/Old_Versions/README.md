## Manage database for smartparking

# Files

- DB_Schema_SP.mwb - Model for MySQL Workbench
- DB_Scheme_SP.pdf - Poster to print then database scheme
- DB_Schema_SP_data.sql - script to create smartparking database scheme and load with TU Darmstadt default data
- DB_Schema_SP_tables_only.sql - script to create smartparking database scheme

# View current database structure
If you want to see (and print) the current database structure then view and print DB_Scheme_SP.pdf

# Update database structure
To update the database structure load DB_Schema_SP.mwb into your MySQL Workbench.
- File->Open Model->DB_Schema_SP.mwb
- make your changes
- File->Save Model
 
Create poster:
- File->Open Model->DB_Schema_SP.mwb
- File->Export->Export as single PDF

Create sql script for database scheme:
- File->Open Model->DB_Schema_SP.mwb
- File->Export->Forward engineuer CREATE SQL Script
- switch on: Generate DROP statements before each CRETAE Statement
- optional (to save the default data):  switch on: Generate INSERT statements for tables

# Create new database and load optional standard test data
If you want to re-init your database than execute DB_Schema_SP_data.sql or DB_Schema_SP_tables_only.sql

This file does
- drop old tables
- create new tables
- fill tables with standard values (only DB_Schema_SP_data.sql)

Be aware that all old data and tables will be deleted! Save your work (e.g. data) before you do the next steps.

Steps:
- Login in your PHPMyadmin in your browser (e.g. https://localhost/phpmyadmin)
- Select SMARTPARKING table at the left pane
- select tab STRUCTURE
- check on: Check all
- with selected: delete data or table -> DROP
- switch off: enable foreign key checks
- Do you want to execute the following query: YES (after then you have an empty database)

- select tab IMPORT
- Browse your computer: Browse...choose DB_Schema_SP_data.sql or DB_Schema_SP_tables_only.sql
- GO
- Voila, database (and optional default data) are back!


