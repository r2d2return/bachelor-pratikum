-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 09, 2017 at 12:55 AM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartparking`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_acl`
--

DROP TABLE IF EXISTS `auth_acl`;
CREATE TABLE `auth_acl` (
  `id` int(11) NOT NULL,
  `controller` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `actions` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '*',
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_acl`
--

INSERT INTO `auth_acl` (`id`, `controller`, `actions`, `role_id`) VALUES
(1, 'Reservations', '*', 1),
(2, 'Users', 'index', 2),
(3, 'Users', '*', 1),
(4, 'Dashboard', '*', 1),
(5, 'RemoteKeys', '*', 1),
(6, 'AuthAcl', '*', 1),
(7, 'Maps', '*', 1),
(8, 'Roles', '*', 1),
(9, 'Users', '*', 4),
(10, 'DatabaseLogs', '*', 1),
(13, 'Reservations', '*', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_acl`
--
ALTER TABLE `auth_acl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_acl`
--
ALTER TABLE `auth_acl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_acl`
--
ALTER TABLE `auth_acl`
  ADD CONSTRAINT `fk_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
