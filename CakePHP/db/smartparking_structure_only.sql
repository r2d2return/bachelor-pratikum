-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 10, 2018 at 11:25 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartparking`
--
USE DATABASE smartparking

-- --------------------------------------------------------

--
-- Table structure for table `apps`
--

CREATE TABLE `apps` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `secret` varchar(45) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `state` enum('ACTIVE','BLOCKED','DELETED') DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_acl`
--

CREATE TABLE `auth_acl` (
  `id` int(11) NOT NULL,
  `controller` varchar(200) CHARACTER SET utf8 NOT NULL,
  `actions` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '*',
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `calender`
--

CREATE TABLE `calender` (
  `recurringId` int(11) NOT NULL,
  `recurringTypeId` int(11) NOT NULL,
  `separationCount` int(11) DEFAULT NULL,
  `maxNumOfOccurences` int(11) DEFAULT NULL,
  `dayOfWeek` int(11) DEFAULT NULL,
  `WeekOfMonth` int(11) DEFAULT NULL,
  `DayOfMonth(11)` int(11) DEFAULT NULL,
  `MonthOfYear` int(11) DEFAULT NULL COMMENT 'Reservation Reccurances '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `calender_exceptions`
--

CREATE TABLE `calender_exceptions` (
  `id` int(11) NOT NULL,
  `res_id` int(11) DEFAULT NULL,
  `isScheduled` bit(1) DEFAULT NULL,
  `isCancelled` bit(1) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `isFullDayReservation` bit(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `database_logs`
--

CREATE TABLE `database_logs` (
  `id` int(11) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `message` text,
  `context` text,
  `created` timestamp NULL DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `hostname` varchar(50) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `refer` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `count` int(10) DEFAULT '0',
  `tenant` varchar(45) DEFAULT NULL,
  `action` enum('CREATE','UPDATE','DELETE','OTHER') DEFAULT 'OTHER',
  `userName` varchar(45) DEFAULT NULL,
  `userRole` varchar(45) DEFAULT NULL,
  `oldValue` varchar(45) DEFAULT NULL,
  `newValue` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `firebase`
--

CREATE TABLE `firebase` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `token` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gateways`
--

CREATE TABLE `gateways` (
  `id` int(11) NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 NOT NULL,
  `fqdn` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='table Kommentar\nlabel - Bezeichnung des Ortes\nfqdn URL für Gateway';

--
-- Table structure for table `issuedComands`
--

CREATE TABLE `issuedComands` (
  `id` int(11) NOT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `cmd` varchar(10) CHARACTER SET utf8 NOT NULL,
  `destNode` int(11) DEFAULT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verbatim` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` enum('SQARE','CIRCLE','ELLIPSE') DEFAULT NULL COMMENT 'SQUARE=four corners, CIRCLE=upperLeft-upperRight means diameter,  ELLIPSE=upperLeft-upperRight means greatest expansion, lowerLeft-lowerLeft means smallest expansion',
  `upperLeftLongitude` double DEFAULT NULL,
  `upperLeftLatitude` double DEFAULT NULL,
  `upperRightLongitude` double DEFAULT NULL,
  `upperRightLatitude` double DEFAULT NULL,
  `lowerLeftLongitude` double DEFAULT NULL,
  `lowerLeftLatitude` double DEFAULT NULL,
  `lowerRightLongitude` double DEFAULT NULL,
  `lowerRightLatitude` double DEFAULT NULL,
  `width` double DEFAULT NULL,
  `length` double DEFAULT NULL,
  `imagePath` varchar(255) DEFAULT NULL COMMENT 'pictures for location'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `parkinglots`
--

CREATE TABLE `parkinglots` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL COMMENT 'interne parkinglot id',
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `mqttchannel` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `status` enum('blocked','free','blocking','freeing','occupied','error') CHARACTER SET utf8 NOT NULL DEFAULT 'error' COMMENT 'current state BLOCKED=barrier up,FREE=barier down,no car, BLOCKING=barrier is moving up,FREEING=barrier is moving down, OCCUPIED=car present, hopefully barrier down, ERROR=see error code',
  `ip` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `hwid` varchar(18) CHARACTER SET utf8 DEFAULT NULL COMMENT 'mac adress ??',
  `lastStateChange` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'timestamp of last state change',
  `type` enum('SINGLE','BARRIER') COLLATE utf8_unicode_ci NOT NULL COMMENT 'barrier type: SINGLE=one barrier fr each parking space, BARRIER=one barrier for mass parking space (Schranke)',
  `locationId` int(11) DEFAULT NULL COMMENT 'reference to location place (coordinates)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `parkinglots_sites`
--

CREATE TABLE `parkinglots_sites` (
  `id` int(11) NOT NULL,
  `sitesId` int(11) NOT NULL,
  `parkingLotsId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `token` char(32) CHARACTER SET utf8 NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `parkinglot_id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `visitedPersonId` int(11) DEFAULT NULL,
  `licencePlate` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reservationState` enum('RESERVED','BOOKED','CANCELED','SUPERKEY') COLLATE utf8_unicode_ci NOT NULL COMMENT 'SUPERKEY=free/block without reservation, Hausmeisterschlüssel',
  `messageInfo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notifiedOnDevice` bit(1) DEFAULT 0,
  `isRecurring` enum('YES','NO') COLLATE utf8_unicode_ci DEFAULT NULL,
  `recurringID` int(11) DEFAULT NULL COMMENT 'see http://www.vertabelo.com/blog/technical-articles/again-and-again-managing-recurring-events-in-a-data-model',
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `reservations_users`
--

CREATE TABLE `reservations_users` (
  `id` int(11) NOT NULL,
  `reservations_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Parker\nAnfrager\nBesucher/Visitor\nBetreiber\nVerwalter\nAdmin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `sensorlogs`
--

CREATE TABLE `sensorlogs` (
  `id` int(11) NOT NULL,
  `parkinglot_id` int(11) DEFAULT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `z` int(11) DEFAULT NULL,
  `zThreshold` int(11) DEFAULT NULL,
  `state` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `supplyVoltage` float DEFAULT NULL,
  `temperature` int(11) DEFAULT NULL,
  `humidity` float DEFAULT NULL,
  `RSSI_gwRX` int(11) DEFAULT NULL,
  `RSSI_nodeRX_avg` int(11) DEFAULT NULL,
  `numRetries_NodeToGW` int(11) DEFAULT NULL,
  `numRxTimeouts` int(11) DEFAULT NULL,
  `errorCode` int(11) DEFAULT NULL,
  `filename` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verbatim` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `token` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `validTo` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'short name ',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'long name',
  `prefix` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `locationID` int(11) NOT NULL,
  `isPublic` enum('NO','YES') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO' COMMENT 'TRUE if connected parking lots are partly/full time public',
  `publicStart` datetime DEFAULT NULL,
  `publicEnd` datetime DEFAULT NULL,
  `isPublicRecurring` enum('YES','NO') COLLATE utf8_unicode_ci DEFAULT NULL,
  `recurringId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `tenants`
--

CREATE TABLE `tenants` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'short description',
  `label` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT 'long description'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `tenants_users`
--

CREATE TABLE `tenants_users` (
  `id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `timestamps`
--

CREATE TABLE `timestamps` (
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT 'username',
  `password` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'password\n',
  `created` datetime DEFAULT NULL COMMENT 'entry creation timestamp',
  `modified` datetime DEFAULT NULL COMMENT 'last modified',
  `email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'email adress',
  `mobileNumber` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'mobile number',
  `landlineNumber` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'fixed line number',
  `firstname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'firstname',
  `lastname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'lastname',
  `title` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'title like dr',
  `gender` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'male/female/other',
  `userState` enum('ACTIVE','CANCELED','BLOCKED','DELETED') COLLATE utf8_unicode_ci NOT NULL COMMENT 'ACTIVE=login allowed, CANCELED=INACTIVE,no login, BLOCKED=temporary inactive, no login, DELETED=finished, could be eraesed,no login',
  `company` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'company name',
  `department` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'department description',
  `street` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'street name and street number',
  `ZIP` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ZIP code'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `users_roles`
--

CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `users_zones`
--

CREATE TABLE `users_zones` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `keyName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '????',
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `zones`
--

CREATE TABLE `zones` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `zones_sites`
--

CREATE TABLE `zones_sites` (
  `id` int(11) NOT NULL,
  `zonesId` int(11) NOT NULL,
  `sitesId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apps`
--
ALTER TABLE `apps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_acl`
--
ALTER TABLE `auth_acl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_role_id_aa` (`role_id`);

--
-- Indexes for table `calender`
--
ALTER TABLE `calender`
  ADD PRIMARY KEY (`recurringId`,`recurringTypeId`);

--
-- Indexes for table `calender_exceptions`
--
ALTER TABLE `calender_exceptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `res_id_idx` (`res_id`);

--
-- Indexes for table `database_logs`
--
ALTER TABLE `database_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_user_id_dl` (`user_id`);

--
-- Indexes for table `firebase`
--
ALTER TABLE `firebase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gateways`
--
ALTER TABLE `gateways`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `idx_fk_site_id_gate` (`site_id`);

--
-- Indexes for table `issuedComands`
--
ALTER TABLE `issuedComands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `destNode` (`destNode`),
  ADD KEY `gateway_id_idx` (`gateway_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parkinglots`
--
ALTER TABLE `parkinglots`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pid` (`pid`),
  ADD KEY `pid_2` (`pid`),
  ADD KEY `pid_3` (`pid`),
  ADD KEY `idx_fk_gateway_id_park` (`gateway_id`),
  ADD KEY `idx_fk_locationId_park` (`locationId`);

--
-- Indexes for table `parkinglots_sites`
--
ALTER TABLE `parkinglots_sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_sitesId_parksites` (`sitesId`),
  ADD KEY `idx_fk_parkingLotId_parksites` (`parkingLotsId`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_parkinglot_id_res` (`parkinglot_id`),
  ADD KEY `idx_fk_recurringID_res` (`recurringID`),
  ADD KEY `idx_fk_users_id_res` (`users_id`),
  ADD KEY `idx_fk_visitedPersonId_res` (`visitedPersonId`);

--
-- Indexes for table `reservations_users`
--
ALTER TABLE `reservations_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_reservations_id_resusers` (`reservations_id`),
  ADD KEY `idx_fk_users_id_resusers` (`users_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sensorlogs`
--
ALTER TABLE `sensorlogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_parkinglot_id_sensor` (`parkinglot_id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_id_session` (`id`);
ALTER TABLE `session` ADD FULLTEXT KEY `idx_token_session` (`token`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_locationID_sites` (`locationID`),
  ADD KEY `idx_fk_recurringId_sites` (`recurringId`);

--
-- Indexes for table `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenants_users`
--
ALTER TABLE `tenants_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_tenant_id_tenusr` (`tenant_id`),
  ADD KEY `idx_fk_user_id_tenusr` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_role_id_usroles` (`role_id`),
  ADD KEY `idx_fk_user_id_usroles` (`user_id`);

--
-- Indexes for table `users_zones`
--
ALTER TABLE `users_zones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_user_id_uszones` (`user_id`),
  ADD KEY `idx_fk_zone_id_uszones` (`zone_id`);

--
-- Indexes for table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zones_sites`
--
ALTER TABLE `zones_sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_id_zonsites` (`id`,`zonesId`,`sitesId`),
  ADD KEY `idx_fk_zonesId_zonsites` (`zonesId`),
  ADD KEY `idx_fk_sitesId_zonsites` (`sitesId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apps`
--
ALTER TABLE `apps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_acl`
--
ALTER TABLE `auth_acl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `calender`
--
ALTER TABLE `calender`
  MODIFY `recurringId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `calender_exceptions`
--
ALTER TABLE `calender_exceptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `database_logs`
--
ALTER TABLE `database_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `firebase`
--
ALTER TABLE `firebase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gateways`
--
ALTER TABLE `gateways`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `issuedComands`
--
ALTER TABLE `issuedComands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `parkinglots`
--
ALTER TABLE `parkinglots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `parkinglots_sites`
--
ALTER TABLE `parkinglots_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `reservations_users`
--
ALTER TABLE `reservations_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sensorlogs`
--
ALTER TABLE `sensorlogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tenants`
--
ALTER TABLE `tenants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tenants_users`
--
ALTER TABLE `tenants_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users_zones`
--
ALTER TABLE `users_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `zones`
--
ALTER TABLE `zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `zones_sites`
--
ALTER TABLE `zones_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_acl`
--
ALTER TABLE `auth_acl`
  ADD CONSTRAINT `fk_role_id_aa` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `calender_exceptions`
--
ALTER TABLE `calender_exceptions`
  ADD CONSTRAINT `res_id` FOREIGN KEY (`res_id`) REFERENCES `calender` (`recurringId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `database_logs`
--
ALTER TABLE `database_logs`
  ADD CONSTRAINT `fk_user_id_dl` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gateways`
--
ALTER TABLE `gateways`
  ADD CONSTRAINT `fk_site_id_gate` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `issuedComands`
--
ALTER TABLE `issuedComands`
  ADD CONSTRAINT `gateway_id` FOREIGN KEY (`gateway_id`) REFERENCES `gateways` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `parkinglots`
--
ALTER TABLE `parkinglots`
  ADD CONSTRAINT `fk_gateway_id_park` FOREIGN KEY (`gateway_id`) REFERENCES `gateways` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_locationId_park` FOREIGN KEY (`locationId`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `parkinglots_sites`
--
ALTER TABLE `parkinglots_sites`
  ADD CONSTRAINT `fk_parkingLotId_parksites` FOREIGN KEY (`parkingLotsId`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sitesId_parksites` FOREIGN KEY (`sitesId`) REFERENCES `sites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `fk_parkinglot_id_res` FOREIGN KEY (`parkinglot_id`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recurringID_res` FOREIGN KEY (`recurringID`) REFERENCES `calender` (`recurringId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_id_res` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_visitedPersonId_res` FOREIGN KEY (`visitedPersonId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `reservations_users`
--
ALTER TABLE `reservations_users`
  ADD CONSTRAINT `fk_reservations_id_resusers` FOREIGN KEY (`reservations_id`) REFERENCES `reservations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_id_resusers` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sensorlogs`
--
ALTER TABLE `sensorlogs`
  ADD CONSTRAINT `fk_parkinglot_id_sensor` FOREIGN KEY (`parkinglot_id`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sites`
--
ALTER TABLE `sites`
  ADD CONSTRAINT `fk_locationID_sites` FOREIGN KEY (`locationID`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recurringId_sites` FOREIGN KEY (`recurringId`) REFERENCES `calender` (`recurringId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tenants_users`
--
ALTER TABLE `tenants_users`
  ADD CONSTRAINT `fk_tenant_id_tenusr` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_id_tenusr` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD CONSTRAINT `fk_role_id_usroles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_id_usroles` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_zones`
--
ALTER TABLE `users_zones`
  ADD CONSTRAINT `fk_user_id_uszones` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_zone_id_uszones` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zones_sites`
--
ALTER TABLE `zones_sites`
  ADD CONSTRAINT `fk_sitesId_zonsites` FOREIGN KEY (`sitesId`) REFERENCES `sites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_zonesId_zonsites` FOREIGN KEY (`zonesId`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
