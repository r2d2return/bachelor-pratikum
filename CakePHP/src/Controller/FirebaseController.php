<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Firebase Controller
 *
 */
class FirebaseController extends AppController
{
    

    /**
    * Get Method for FirebaseController
    * Returns the token of the authorized user
    */
    public function get(){
        $tokens =$this->Firebase->find()->where(['users_id'=> $this->Auth->user('id')])->all();
        $this->set('tokens', $tokens);
        $this->set('_serialize', ['tokens']);

        return;
    }

    /**
    * Get Method for FirebaseController
    * Returns the token of the authorized user
    */
    public function add()
    {
        $firebaseEntry = $this->Firebase->newEntity();
        if ($this->request->is('post')) {
            debug($firebaseEntry);
            $token = $this->request->getData()['token'];
            $users_id = $this->Auth->user('id');
            $firebaseEntry->token = $token;
            $firebaseEntry->users_id = $users_id;
            debug($firebaseEntry);
            if (!isset($token) || !isset($users_id) || $this->Firebase->save($firebaseEntry)) {
               $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Message' => 'Success']))
                                         ->withStatus(200);

                return $response;
            } else {
                $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Error' => 'Token could not be saved']))
                                         ->withStatus(400);

                return $response;
            }
        }else{
            $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Error' => 'Only Post requests allowed to this location']))
                                         ->withStatus(400);

            return $response;
        }
  
    }

    

    /**
     * Delete method
     *
     * @param string|null $id Zone id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $token = $this->request->getData()['token'];
        debug($token);
        $firebaseEntry = $this->Firebase->find()->where(['token' => $token])->first();
        debug($firebaseEntry);
        if(count($firebaseEntry)<1){
            $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Error' => 'Token not found']))
                                         ->withStatus(404);

            return $response;
        }
        if ($this->Firebase->delete($firebaseEntry)) {
            $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Message' => 'Success']))
                                         ->withStatus(200);

            return $response;
        } else {
            $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Error' => 'token could not be deleted']))
                                         ->withStatus(400);

            return $response;
        }
    }
}
