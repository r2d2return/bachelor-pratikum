<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New RemoteKey'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List RemoteKeys'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Parking Lots'), ['controller' => 'Parkinglots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sensorlogs'), ['controller' => 'Sensorlogs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
    </ul>
</div>
<div class="remoteKeys index large-10 medium-9 columns">
	<h3 style="margin-top:2em;">History</h3>
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('username') ?></th>
            <th><?= $this->Paginator->sort('mail') ?></th>
            <th><?= $this->Paginator->sort('valid_from') ?></th>
            <th><?= $this->Paginator->sort('valid_until') ?></th>
            <th><?= $this->Paginator->sort('parkinglot_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($remoteKeys as $remoteKey): ?>
        <tr>
            <td><?= $this->Number->format($remoteKey->id) ?></td>
            <td><?= h($remoteKey->username) ?></td>
            <td><?= h($remoteKey->mail) ?></td>
            <td><?= h($remoteKey->valid_from) ?></td>
            <td><?= h($remoteKey->valid_until) ?></td>
            <td>
                <?= $this->Html->link($remoteKey->parkinglot->description, ['controller' => 'Parkinglots', 'action' => 'view', $remoteKey->parkinglot->id])?>
            </td>
            <td class="actions">
                <?= $this->Html->image('view.gif', [ 'alt' => __('View'), 
                								     'url' => ['controller' => 'RemoteKeys', 
                								     'action' => 'view', $remoteKey->id]]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<?php $this->Html->addCrumb('Remote Keys', '/remoteKeys'); ?>
<?php $this->Html->addCrumb('History', ['controller' => 'RemoteKeys', 'action' => 'history']); ?>