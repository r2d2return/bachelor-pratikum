<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SitesController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;
/**
 * App\Controller\SitesController Test Case
 */
class SitesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.failed_login_attempts',
        'app.sites',
        'app.users',
        'app.parkinglots',
        'app.gateways',
        'app.roles',
        'app.users_roles',
        'app.auth_acl',
        'app.failed_login_attempts',
    ];

    public function setUp(){
        parent::setup();  
        //If Users are added in the UsersFixture, they should be included
        $this->global_users = TableRegistry::get('Users')->find()->toArray(); 
    }


    /** 
        CONFIGURATION
        ===============================================================================
    */
        //Define which keys should be checked to be included/excluded from a response -> See "Interface-Definition"
        public $json_return_keys = ['id', 
                            'name', 
                            'description', 
                            'prefix', 
                            'locationID', 
                            'isPublic',
                            'publicStart',
                            'publicEnd',
                            'isPublicRecurring',
                            'recurringId',
                            ];

        //Define which ROLES are allowed and not allowed to access the location /sites.json
         public $authorized_roles = [];                  
         public $unauthorized_roles = [ 'Admin','Guest', 'Visitor', 'Facility Manager', 'Reservation Manager'];                   


        public $global_users;

        /*
        ======================================================================================
        */
        
        // HELPERS
        function doesContain($array){
            foreach ($array as $key) {
                $this->assertResponseContains($key);
            }
        }

        function doesNOTContain($array){
            foreach ($array as $key) {
                $this->assertResponseNOTContains($key);
            }
        }

    /**
       ===============================================================================
    */

    /**
     * Test index method
     * Key Assertions: Requester is given no Information about the Database or the Users inside it while informing about the 
     * problem of not beeing Authenticated
     * For further details check the documentation
     * @return void
     */
    public function testIndexUnauthenticated()
    {
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not A User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        $this->get('/sites.json');
        //debug($this->_response->body());
        $this->assertResponseError();
        $this->assertResponseCode(401);
        $this->assertResponseNotEmpty();
        $this->assertResponseContains('Unauthorized');

        //Check if no fields according to the "Interface-Definition" are retrieved
        $this->doesNOTContain($this->json_return_keys);
        //------------------//

        $this->assertContentType('application/json');
    }


    /**
     * Test index method
     * Key Assertion: Requester is given no Information about the Database or the Users inside it while informing about
     * the problem of not beeing Authenticated
     * For further details check the documentation 
     * @return void
     */
    public function testIndexUnAuthorized()
    {
        //Fixtures are chosen so that username == password == role
        //Should contain all roles that are NOT allowed to access the requested function. Thus are authorized.
        $users = $this->unauthorized_roles;

        //Send a request for every role that is allowed to make the request
        foreach ($users as $user)  {
            //debug($user);
            $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => $user,
                    'PHP_AUTH_PW' => $user,
                ]
            ]);
            $this->get('/sites.json');
            //debug($this->_response->body());

            $this->assertResponseError();
            $this->assertResponseCode(403);
            $this->assertResponseNotEmpty();
            $this->assertResponseContains('You are not authorized to access that location.');

            $this->assertResponseNotContains($user);

            //Check if no fields according to the "Interface-Definition" are retrieved
            $this->doesNOTContain($this->json_return_keys);
            //------------------//
                    
           
            
            $this->assertContentType('application/json');
        }

        if(empty($users)){
            echo("\n\n \033[01;31m WARNING: \033[0m No meaningfull assertions were made in SitesController.testGetUserUnAuthorized()! \nIf every role is authorized to access /sites.json, ignore this message\n\n");
            $this->assertEquals(true, true);
        }
    }

    /**
     * Test index method
     * Key Assertion: Authorized requester is given exactly his and only his information about his corresponding User
     * For further details check the documentation
     * @return void
     */
    public function testIndexAuthorized()
    {
        //Fixtures are chosen so that username == password == role
        //Should contain all roles that are allowed to access the requested function. Thus are authorized.
        $users = $this->authorized_roles;

        //Send a request for every role that is allowed to make the request
        foreach ($users as $user)  {
            //debug($user);
            $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => $user,
                    'PHP_AUTH_PW' => $user,
                ]
            ]);

            $this->get('/sites.json');
            //debug($this->_response->body());
            $this->assertResponseOk();
            $this->assertResponseNotEmpty();
            $this->assertResponseCode(200);

            //Check if all fields according to the "Interface-Definition" are retrieved
            $this->doesContain($this->json_return_keys);
            //------------------//
            
           
            
            $this->assertContentType('application/json');
        }
        
        
        if(empty($users)){
            echo("\n\n \033[01;31m WARNING: \033[0m No meaningfull assertions were made in SitesController.testGetUserAuthorized()! \nIf no role is authorized to access /sites.json, ignore this message\n\n");
            $this->assertEquals(true, true);
            
        }
        
    }




    /**
     * Test view method
     *
     * @return void
     
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    */
}
