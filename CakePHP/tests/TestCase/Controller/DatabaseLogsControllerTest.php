<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DatabaseLogsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DatabaseLogsController Test Case
 */
class DatabaseLogsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.database_logs',
        'app.users',
        'app.tenants',
        'app.tenants_users',
        'app.roles',
        'app.auth_acl',
        'app.users_roles',
        'app.zones',
        'app.parkinglots',
        'app.gateways',
        'app.sites',
        'app.parkinglots_sites',
        'app.zones_sites',
        'app.issued_comands',
        'app.remote_keys',
        'app.reservations',
        'app.sensorlogs',
        'app.parkinglots_zones',
        'app.users_zones'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
