package com.tud.kom.parkinglot.model;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.Arrays;

public class ParkingLot implements Serializable {
    private int id;
    // pid;
    private String description;
    // status;
    private double lat;
    private double lon;
    private Site[] sites;

    // additional variables
    private boolean mOpen;

    public ParkingLot() {
        mOpen = false;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public LatLng getLatLng() {
        return new LatLng(lat, lon);
    }

    public void setLatLng(final LatLng latLng) {
        lat = latLng.latitude;
        lon = latLng.longitude;
    }

    public String getName() {
        return description;
    }

    public void setName(final String name) {
        description = name;
    }

    public Site[] getSites() {
        return sites;
    }

    public void setSites(final Site[] sites) {
        this.sites = sites;
    }

    public boolean isOpen() {
        return mOpen;
    }

    public void setOpen(final boolean open) {
        mOpen = open;
    }

    @Override
    public String toString() {
        return "ParkingLot{" + "id=" + id + ", description='" + description + '\'' + ", lat=" + lat + ", lon=" + lon + ", sites=" + Arrays.toString(sites) + ", mOpen=" + mOpen + '}';
    }
}
