<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ParkinglotsZonesFixture
 *
 */
class ParkinglotsZonesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    /*
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'parkinglot_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'zone_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];*/
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'parkinglot_id' => 1,
            'zone_id' => 1
        ],

        [
            'id' => 2,
            'parkinglot_id' => 2,
            'zone_id' => 2
        ],

        [
            'id' => 3,
            'parkinglot_id' => 3,
            'zone_id' => 3
        ],

        [
            'id' => 4,
            'parkinglot_id' => 4,
            'zone_id' => 4
        ],

        [
            'id' => 5,
            'parkinglot_id' => 5,
            'zone_id' => 5
        ]
    ];
}
