package com.tud.kom.parkinglot.notification.firebase;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.network.RequestAbstraction;

public class InstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = InstanceIdService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        String username = SavedPreferences.getPreference(this, SavedPreferences.Filename.USERNAME);
        String password = SavedPreferences.getPreference(this, SavedPreferences.Filename.PASSWORD);
        if (username == null || password == null)
            return;
        RequestAbstraction.sendFirebaseInstanceId(null, refreshedToken, username, password);
    }
}