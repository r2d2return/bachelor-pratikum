<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use App\Model\Table\RolesTable;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;
/**
 * Users Controller
 * manages the users of the smart parking system
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
	/**
	 * @see \Cake\Controller\Controller::beforeFilter()
	 */
	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		
		// Allow users to logout.
		// Do not add 'login' here cause this may distrub the framework
		$this->Auth->allow(['logout']);
	}
	
	/**
     * @codeCoverageIgnore
     */
	public function login()
	{
		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			if ($user) {
				$this->Auth->setUser($user);
				if($this->Auth->user('userState') == 'ACTIVE') {
					$userTable = TableRegistry::get('Users');
					$currentAuthUserRoles = $userTable->getRoles($this->Auth->user('id'));
					if($currentAuthUserRoles[0] == 'Reservation Manager') {
			          	return $this->redirect(
			            	['controller' => 'Reservations', 'action' => 'index']
			          );
			        } else {
			        	return $this->redirect($this->Auth->redirectUrl());
			        }
		        } else {
		        	$this->Auth->logout();
	            	$this->Flash->error(__('You are not authorized to login'));
		        }
			} else {
				$this->Flash->error(__('Invalid username or password. Please try again'));
			}
		}
	}
	
	/**
     * @codeCoverageIgnore
     */
	public function logout()
	{
		return $this->redirect($this->Auth->logout());
	}

	public function getUser()
	{
		$user = $this->Auth->identify();
		$this->set('user', $user);
		$this->set('_serialize', 'user');
	}
	
	/**
	 * Index method
	 *
     * @codeCoverageIgnore
     *
	 * @return void
	 */
	public function index()
	{
		$userRoles = $this->Users->getRoles($this->Auth->user('id'));
		if($userRoles[0] == "Admin") {
			$users = $this->Users->find('all', [ 'contain' => [ 'Roles', 'Zones']]);
			$this->set('users', $this->paginate($this->Users));
		}else {
			$users = $this->Users->find('all', [ 'contain' => [ 'Roles']])->matching('Zones', function ($q) {
				    return $q->where(['Zones.id' => $this->Users->zoneId($this->Auth->user('id'))]);
				});
			$this->set('users', $this->paginate($this->Users));
		}
		$this->set(compact('users','userRoles'));
		$this->set('_serialize', ['users']);
	}
	
	/**
	 * View method
	 * @codeCoverageIgnore
	 * @param string|null $id User id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		if (!$id) {
			throw new NotFoundException(__('Invalid user'));
		}
		
		$user = $this->Users->get($id, [
				'contain' => ['Roles', 'Zones', 'Tenants']
		]);

		$this->set('user', $user);
		$this->set('_serialize', ['user']);
	}
	
	/**
	 * Add method
	 * @codeCoverageIgnore
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$user = $this->Users->newEntity(['associated' => ['Roles', 'Tenants', 'Zones']]);
		$admin = 0;
		$reservationManager = 0;
		$facilityManager = 0;
		$error = 1;
		$expression = $this->Users->getRoles($this->Auth->user('id'));
		foreach ($expression as $value) {
		     if($value == "Admin") {
		     	$admin = 1;
		     }elseif ($value == "Reservation Manager") {
		     	$reservationManager = 1;
		     	$user->zones = $this->Users->Zones->find()->where(function ($exp, $q) {
	        	return $exp->in('Zones.id', $this->Users->zoneId($this->Auth->user('id')));
	    		})->toArray();
		     	$user->tenants = $this->Users->Tenants->find()->where(function ($exp, $q) {
	        	return $exp->in('Tenants.id', $this->Users->tenantId($this->Auth->user('id')));
	    		})->toArray();
		     }elseif ($value == "Facility Manager") {
		     	$facilityManager = 1;
		     	$user->zones = $this->Users->Zones->find()->where(function ($exp, $q) {
	        	return $exp->in('Zones.id', $this->Users->zoneId($this->Auth->user('id')));
	    		})->toArray();
		     	$user->tenants = $this->Users->Tenants->find()->where(function ($exp, $q) {
	        	return $exp->in('Tenants.id', $this->Users->tenantId($this->Auth->user('id')));
	    		})->toArray();
		     }else {
		     	$error = 0;
		     }
		}
		if($error == 0) {
			throw new ForbiddenException(__('This user does not have the rights to do this operation!'));
		} else {
			if ($this->request->is('post')) {
				$user = $this->Users->patchEntity($user, $this->request->data);

				$roles2 = $this->Users->Roles->find()->where(function ($exp, $q) {
	        	return $exp->in('Roles.id', $this->request->data['role']);
	    		});
				$user->roles = $roles2->toArray();
				if($admin == 1) {
					$user->tenants = $this->Users->Tenants->find()->where(function ($exp, $q) {
		        	return $exp->in('Tenants.id', $this->request->data['tenant']);
		    		})->toArray();
		    		$user->zones = $this->Users->Zones->find()->where(function ($exp, $q) {
		        	return $exp->in('Zones.id', $this->request->data['zone']);
		    		})->toArray();
				}

				if ($this->Users->save($user)) {

					$this->Flash->success(__('The user has been saved.'));

                    $newUser = $user['username'];
                    if($user['role'] == 1) {
                        $newRole = "Admin";
                    } else if($user['role'] == 2){
                        $newRole = "Guest";
                    } else if($user['role'] == 3){
                        $newRole = "Visitor";
                    } else if($user['role'] == 4){
                        $newRole = "Reservation Manager";
                    } else if($user['role'] == 5){
                        $newRole = "Facility Manager";
                    }
                    $newValue = $newUser." , ".$newRole;
                    Log::info('Add to databaselog.log',
                        [
                            'scope' => "operation",
                            'action' => "CREATE",
                            'tenant' => "TU Darmstadt",
                            'newValue' => "$newValue"
                        ]);
					return $this->redirect(['action' => 'index']);
				} else {
					$this->Flash->error(__('The user could not be saved. Please, try again.'));
				}
			}

			$roles = $this->Users->Roles->find('list');
			$zones = $this->Users->Zones->find('list');
			$tenants = $this->Users->Tenants->find('list');

			$reservationQuery = $this->Users->Roles->find('list')->where(function ($exp) {
			        return $exp
			            ->notEq('name', 'Admin')
			            ->notEq('name', 'Facility Manager');
			    })->order(['name' => 'ASC']);

			$facilityQuery = $this->Users->Roles->find('list')->where(function ($exp) {
			        return $exp
			            ->eq('name', 'Facility Manager');
			    })->order(['name' => 'ASC']);
		} 
		$gender = array("Male" => "Male", "Female" => "Female", "other" => "other");
		$state = array("ACTIVE" => "ACTIVE", "BLOCKED" => "BLOCKED", "CANCELED" => "CANCELED", "DELETED" => "DELETED");
		$this->set(compact('user', 'roles', 'zones', 'tenants', 'admin', 'reservationManager', 'facilityManager', 'facilityQuery', 'reservationQuery', 'gender', 'state'));
		$this->set('_serialize', ['user'], ['roles']);
	}
	
	/**
	 * Edit method
	 * @codeCoverageIgnore
	 * @param string|null $id User id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$user = $this->Users->get($id, [
				'contain' => ['Roles']
		]);

		$admin = 0;
		$reservationManager = 0;
		$facilityManager = 0;
		$error = 1;
		$expression = $this->Users->getRoles($this->Auth->user('id'));
		$oldUserState = $this->Users->getUserState($user->id);

		foreach ($expression as $key => $value) {
		     if($value == "Admin") {
		     	$admin = 1;
		     }elseif ($value == "Reservation Manager") {
		     	$reservationManager = 1;
		     	$user->zones = $this->Users->Zones->find()->where(function ($exp, $q) {
	        	return $exp->in('Zones.id', $this->Users->zoneId($this->Auth->user('id')));
	    		})->toArray();
		     	$user->tenants = $this->Users->Tenants->find()->where(function ($exp, $q) {
	        	return $exp->in('Tenants.id', $this->Users->tenantId($this->Auth->user('id')));
	    		})->toArray();
		     }elseif ($value == "Facility Manager") {
		     	$facilityManager = 1;
		     	$user->zones = $this->Users->Zones->find()->where(function ($exp, $q) {
	        	return $exp->in('Zones.id', $this->Users->zoneId($this->Auth->user('id')));
	    		})->toArray();
		     	$user->tenants = $this->Users->Tenants->find()->where(function ($exp, $q) {
	        	return $exp->in('Tenants.id', $this->Users->tenantId($this->Auth->user('id')));
	    		})->toArray();
		     }else {
		     	$error = 0;
		     }
		}
		if($error == 0) {
			throw new ForbiddenException(__('This user does not have the rights to do this operation!'));
		} else {
            $oldUser = $user['username'];
            $userId = $user['id'];
            $sql = "SELECT role_id FROM `users_roles` WHERE users_roles.user_id = $userId;";
            $conn = ConnectionManager::get('default');
            $smt = $conn->execute($sql);
            $rows = $smt->fetchAll('assoc');
            foreach ($rows as $row){
                //$oldRole = "";
                if ($row['role_id'] == 1){
                    $oldRole = "Admin";
                } else if($row['role_id'] == 2){
                    $oldRole = "Guest";
                } else if($row['role_id'] == 3){
                    $oldRole = "Visitor";
                } else if($row['role_id'] == 4){
                    $oldRole = "Reservation Manager";
                } else if($row['role_id'] == 5){
                    $oldRole = "Facility Manager";
                }
            }
            $oldValue = $oldUser." , ".$oldRole. " , ".$oldUserState['userState'];

			if ($this->request->is(['patch', 'post', 'put'])) {
				$user = $this->Users->patchEntity($user, $this->request->data);
				// $user->userState = $user->userState + 1; please check if application is working fine after you change some value in code!!!!!
                    //print_r($user);exit;
                $newUserState = $user->userState;
                

				$roles2 = $this->Users->Roles->find()->where(function ($exp, $q) {
	        	return $exp->in('Roles.id', $this->request->data['role']);
	    		});
				$user->roles = $roles2->toArray();

				if ($this->Users->save($user)) {
					$this->Flash->success(__('The user has been saved.'));
                    $newUser = $user['username'];
                    if($user['role'] == 1) {
                        $newRole = "Admin";
                    } else if($user['role'] == 2){
                        $newRole = "Guest";
                    } else if($user['role'] == 3){
                        $newRole = "Visitor";
                    } else if($user['role'] == 4){
                        $newRole = "Reservation Manager";
                    } else if($user['role'] == 5){
                        $newRole = "Facility Manager";
                    }
                    $newValue = $newUser." , ".$newRole. " , ".$newUserState;
                    Log::info('Add to databaselog.log',
                        [
                            'scope' => "operation",
                            'action' => "UPDATE",
                            'tenant' => "TU Darmstadt",
                            'oldValue' => "$oldValue",
                            'newValue' => "$newValue"
                        ]);
					return $this->redirect(['action' => 'index']);
				} else {
					$this->Flash->error(__('The user could not be saved. Please, try again.'));
				}
			}
			$roles = $this->Users->Roles->find('list');
			$zones = $this->Users->Zones->find('list');
			$tenants = $this->Users->Tenants->find('list');

			$reservationQuery = $this->Users->Roles->find('list')->where(function ($exp) {
			        return $exp
			            ->notEq('name', 'Admin')
			            ->notEq('name', 'Facility Manager');
			    })->order(['name' => 'ASC']);

			$facilityQuery = $this->Users->Roles->find('list')->where(function ($exp) {
			        return $exp
			            ->eq('name', 'Facility Manager');
			    })->order(['name' => 'ASC']);
		} 
		$gender = array("Male" => "Male", "Female" => "Female", "other" => "other");
		$state = array("ACTIVE" => "ACTIVE", "BLOCKED" => "BLOCKED", "CANCELED" => "CANCELED", "DELETED" => "DELETED");
		$this->set(compact('user', 'roles', 'zones', 'tenants', 'admin', 'reservationManager', 'facilityManager', 'facilityQuery', 'reservationQuery', 'gender', 'state'));
		$this->set('_serialize', ['user']);
	}
	
	/**
	 * Delete method
	 * @codeCoverageIgnore
	 * @param string|null $id User id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$user = $this->Users->get($id);
        $userId = $user['id'];
        $username = $user['username'];
        $sql = "SELECT role_id FROM `users_roles` WHERE users_roles.user_id = $userId;";
        $conn = ConnectionManager::get('default');
        $smt = $conn->execute($sql);
        $rows = $smt->fetchAll('assoc');
        foreach ($rows as $row){
            if($row['role_id'] == 1) {
                $userRole = "Admin";
            } else if($row['role_id'] == 2){
                $userRole = "Guest";
            } else if($row['role_id'] == 3){
                $userRole = "Visitor";
            } else if($row['role_id'] == 4){
                $userRole = "Reservation Manager";
            } else if($row['role_id'] == 5){
                $userRole = "Facility Manager";
            } else {$userRole = "";}
        }
		if ($this->Users->delete($user)) {
			$this->Flash->success(__('The user has been deleted.'));
            $oldValue = $username. " , " .$userRole;
            //print_r($oldValue);exit;
            Log::info('Add to databaselog.log',
                [
                    'scope' => "operation",
                    'action' => "DELETE",
                    'tenant' => "TU Darmstadt",
                    'oldValue' => "$oldValue"
                ]);
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
