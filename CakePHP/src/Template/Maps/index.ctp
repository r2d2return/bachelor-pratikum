
<div class="row">
        <div class="col-lg-12">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav navbar-top-links">
                            <li class="dropdown map-menu">
                                <?php foreach ($allSites as $site): ?>
                                    <a class="sites" data-site-id="<?php echo $site->id ?>"><?php echo $site->name ?></a>
                                <?php endforeach; ?>
                            </li>
                        </ul>
                    </div>
                 </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-lg-12">
        <div id="map" style="height: 500px; width: 100%;"></div>
    </div>
</div>

        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoJP28N5kPsDCdTTR5tX-MoF1MHfS5xkk&callback=initMap">
        </script>
<?php $this->Html->addCrumb('Maps', ['controller' => 'Maps', 'action' => 'index']); ?>