<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AuthAclFixture
 *
 */
class AuthAclFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'auth_acl';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    /*
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'controller' => ['type' => 'string', 'length' => 200, 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'actions' => ['type' => 'string', 'length' => 250, 'null' => false, 'default' => '*', 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'role_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];*/
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = array(
      array('id' => '1','controller' => 'Reservations','actions' => '*','role_id' => '1'),
      array('id' => '2','controller' => 'Users','actions' => 'index','role_id' => '2'),
      array('id' => '3','controller' => 'Users','actions' => '*','role_id' => '1'),
      array('id' => '4','controller' => 'Dashboard','actions' => '*','role_id' => '1'),
      array('id' => '5','controller' => 'RemoteKeys','actions' => '*','role_id' => '1'),
      array('id' => '6','controller' => 'AuthAcl','actions' => '*','role_id' => '1'),
      array('id' => '7','controller' => 'Maps','actions' => '*','role_id' => '1'),
      array('id' => '8','controller' => 'Roles','actions' => '*','role_id' => '1'),
      array('id' => '9','controller' => 'Users','actions' => '*','role_id' => '4'),
      array('id' => '10','controller' => 'DatabaseLogs','actions' => '*','role_id' => '1'),
      array('id' => '13','controller' => 'Reservations','actions' => '*','role_id' => '4'),
      array('id' => '14','controller' => 'Dashboard','actions' => '*','role_id' => '4'),
      array('id' => '18','controller' => 'Reservations','actions' => 'index, add_api, history','role_id' => '3'),
      array('id' => '19','controller' => 'Users','actions' => 'getUser','role_id' => '3'),

      array('id' => '20','controller' => 'Firebase','actions' => '*','role_id' => '3'),
      //array('id' => '20','controller' => 'Sites','actions' => '*','role_id' => '1')
      //array('id' => '20','controller' => 'Parking','actions' => '*','role_id' => '1')

    );

}
