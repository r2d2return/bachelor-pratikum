package com.tud.kom.parkinglot.model.parser;

import com.google.android.gms.maps.model.LatLng;
import com.tud.kom.parkinglot.model.ParkingLot;
import com.tud.kom.parkinglot.model.Reservation;
import com.tud.kom.parkinglot.model.Site;
import com.tud.kom.parkinglot.model.Zone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public abstract class ReservationParser {
    public static ArrayList<Reservation> parseReservationsArray(String jsonString) throws JSONException, ParseException {
        JSONArray jsonReservations = new JSONArray(jsonString);
        ArrayList<Reservation> reservations = new ArrayList<>();
        for (int i = 0; i < jsonReservations.length(); i++)
            reservations.add(parseReservation(jsonReservations.getJSONObject(i)));
        return reservations;
    }

    private static Reservation parseReservation(JSONObject jsonReservation) throws JSONException, ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.GERMAN);
        Reservation reservation = new Reservation();
        if (jsonReservation.has("id"))
            reservation.setId(jsonReservation.getInt("id"));
        if (jsonReservation.has("token"))
            reservation.setToken(jsonReservation.getString("token"));
        if (jsonReservation.has("start")) {
            String start = jsonReservation.getString("start");
            reservation.setStartTime(new Timestamp(format.parse(start).getTime()));
        }
        if (jsonReservation.has("end")) {
            String end = jsonReservation.getString("end");
            reservation.setEndTime(new Timestamp(format.parse(end).getTime()));
        }
        if (jsonReservation.has("notifiedOnDevice"))
            reservation.setNotified(jsonReservation.getBoolean("notifiedOnDevice"));
        reservation.setParkingLot(parseParkingLot(jsonReservation.getJSONObject("parkinglot")));
        return reservation;
    }

    private static ParkingLot parseParkingLot(JSONObject jsonParkingLot) throws JSONException {
        ParkingLot parkingLot = new ParkingLot();
        if (jsonParkingLot.has("id"))
            parkingLot.setId(jsonParkingLot.getInt("id"));
        if (jsonParkingLot.has("description"))
            parkingLot.setName(jsonParkingLot.getString("description"));
        if (jsonParkingLot.has("location")) {
            JSONObject jsonLocation = jsonParkingLot.getJSONObject("location");
            LatLng latLng = new LatLng(jsonLocation.getDouble("upperLeftLongitude"), jsonLocation.getDouble("upperLeftLatitude"));
            parkingLot.setLatLng(latLng);
        }
        parkingLot.setSites(parseSites(jsonParkingLot.getJSONArray("sites")));
        return parkingLot;
    }

    private static Site[] parseSites(JSONArray jsonSites) throws JSONException {
        Site[] sites = new Site[jsonSites.length()];
        for (int i = 0; i < jsonSites.length(); i++)
            sites[i] = parseSite(jsonSites.getJSONObject(i));
        return sites;
    }

    private static Site parseSite(JSONObject jsonSite) throws JSONException {
        Site site = new Site();
        if (jsonSite.has("id"))
            site.setId(jsonSite.getInt("id"));
        if (jsonSite.has("name"))
            site.setName(jsonSite.getString("name"));
        if (jsonSite.has("description"))
            site.setDescription(jsonSite.getString("description"));
        if (jsonSite.has("prefix"))
            site.setPrefix(jsonSite.getString("prefix"));
        if (jsonSite.has("zones"))
            site.setZones(parseZones(jsonSite.getJSONArray("zones")));
        return site;
    }

    private static Zone[] parseZones(JSONArray jsonZones) throws JSONException {
        Zone[] zones = new Zone[jsonZones.length()];
        for (int i = 0; i < jsonZones.length(); i++)
            zones[i] = parseZone(jsonZones.getJSONObject(i));
        return zones;
    }

    private static Zone parseZone(JSONObject jsonZone) throws JSONException {
        Zone zone = new Zone();
        zone.setId(jsonZone.getInt("id"));
        zone.setName(jsonZone.getString("name"));
        return zone;
    }
}