<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return !in_array($schema->columnType($field), ['binary', 'text']);
    })
    ->take(7);
%>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
            <li><?= $this->Html->link(__('New <%= $singularHumanName %>'), ['action' => 'add']) ?></li>
            <%
			    $done = [];
			    foreach ($associations as $type => $data):
			        foreach ($data as $alias => $details):
			            if (!empty($details['navLink']) && $details['controller'] !== $this->name && !in_array($details['controller'], $done)):
			%>
			        <li><?= $this->Html->link(__('List <%= $this->_pluralHumanName($alias) %>'), ['controller' => '<%= $details['controller'] %>', 'action' => 'index']) ?></li>
			        <li><?= $this->Html->link(__('New <%= $this->_singularHumanName($alias) %>'), ['controller' => '<%= $details['controller'] %>', 'action' => 'add']) ?></li>
			<%
			                $done[] = $details['controller'];
			            endif;
			        endforeach;
			    endforeach;
			%>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>

START

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Remotes List
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                        	<% foreach ($fields as $field): %>
					        	<th><?= $this->Paginator->sort('<%= $field %>') ?></th>
					   		<% endforeach; %>
					        <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>


						<?php $counter = 1; ?>
					    <?php foreach ($<%= $pluralVar %> as $<%= $singularVar %>): ?>
					        <tr role="row" class="gradeA <?php echo ($counter%2 == 0 ?  'odd': 'even'); ?>">
								<%        foreach ($fields as $field) {
								            $isKey = false;
								            if (!empty($associations['BelongsTo'])) {
								                foreach ($associations['BelongsTo'] as $alias => $details) {
								                    if ($field === $details['foreignKey']) {
								                        $isKey = true;
								%>
						            <td>
						            	
						                <?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?>
						            </td>
								<%
								                        break;
								                    }
								                }
								            }
					            if ($isKey !== true) {
					                if (!in_array($schema->columnType($field), ['integer', 'biginteger', 'decimal', 'float'])) {
					%>
					            <td><?= h($<%= $singularVar %>-><%= $field %>) ?></td>
					<%
					                } else {
					%>
					            <td class="sorting_1"><?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></td>
					<%
					                }
					            }
					        }
					
					        $pk = '$' . $singularVar . '->' . $primaryKey[0];
					%>
					            <td class="actions">
					                <?= $this->Html->link(__('View'), ['action' => 'view', <%= $pk %>]) ?>
					                <?= $this->Html->link(__('Edit'), ['action' => 'edit', <%= $pk %>]) ?>
					                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', <%= $pk %>], ['confirm' => __('Are you sure you want to delete # {0}?', <%= $pk %>)]) ?>
					            </td>
								            <td class="actions">
			                            <?= $this->Html->image('view.gif', [ 'alt' => __('View'),
			                								     'url' => ['controller' => 'RemoteKeys',
			                								     		   'action' => 'view', $remoteKey->id]]) ?>
			                            <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'),
			                								     'url' => ['controller' => 'RemoteKeys',
			                								     		   'action' => 'edit', $remoteKey->id]
			                								   ]) ?>
			                            <?= $this->Html->link(
			                					  $this->Html->image('mail.png', [ 'alt' => __('Resend'), 'width' => '16px', 'height' => '14px']),
			                					  ['controller' => 'RemoteKeys', 'action' => 'resend', $remoteKey->id],
			                					  ['confirm' => __('Are you sure you want to resend the remoteKey Email to  # {0}?', $remoteKey->username),
			                					   'escapeTitle' => false]
			                				     ) ?>
			                            <?= $this->Html->link(
			                					  $this->Html->image('delete.png', [ 'alt' => __('Delete')]),
			                					  ['controller' => 'RemoteKeys', 'action' => 'delete', $remoteKey->id],
			                					  ['confirm' => __('Are you sure you want to delete # {0}?', $remoteKey->id),
			                					   'escapeTitle' => false]
			                				     ) ?>
			                        </td>
					        </tr>
					
					    <?php endforeach; ?>
					    </tbody>
					    </table>
    
    
                        <?php $counter = 1; ?>
                        <?php foreach ($remoteKeys as $remoteKey): ?>
                        <tr role="row" class="gradeA <?php echo ($counter%2 == 0 ?  'odd': 'even'); ?>">
                        <td class="sorting_1"><?= $this->Number->format($remoteKey->id) ?></td>
                        <td><?= h($remoteKey->username) ?></td>
                        <td><?= h($remoteKey->mail) ?></td>
                        <td><?= h($remoteKey->valid_from) ?></td>
                        <td><?= h($remoteKey->valid_until) ?></td>
                        <td>
                            <?= $this->Html->link($remoteKey->parkinglot->description, ['controller' => 'Parkinglots', 'action' => 'view', $remoteKey->parkinglot->id])?>
                        </td>
                        <td class="actions">
                            <?= $this->Html->image('view.gif', [ 'alt' => __('View'),
                								     'url' => ['controller' => 'RemoteKeys',
                								     		   'action' => 'view', $remoteKey->id]]) ?>
                            <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'),
                								     'url' => ['controller' => 'RemoteKeys',
                								     		   'action' => 'edit', $remoteKey->id]
                								   ]) ?>
                            <?= $this->Html->link(
                					  $this->Html->image('mail.png', [ 'alt' => __('Resend'), 'width' => '16px', 'height' => '14px']),
                					  ['controller' => 'RemoteKeys', 'action' => 'resend', $remoteKey->id],
                					  ['confirm' => __('Are you sure you want to resend the remoteKey Email to  # {0}?', $remoteKey->username),
                					   'escapeTitle' => false]
                				     ) ?>
                            <?= $this->Html->link(
                					  $this->Html->image('delete.png', [ 'alt' => __('Delete')]),
                					  ['controller' => 'RemoteKeys', 'action' => 'delete', $remoteKey->id],
                					  ['confirm' => __('Are you sure you want to delete # {0}?', $remoteKey->id),
                					   'escapeTitle' => false]
                				     ) ?>
                        </td>
                    </tr>
                    <?php $counter++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p><?= $this->Paginator->counter() ?></p>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>

STOP


<div class="row">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
    <% foreach ($fields as $field): %>
        <th><?= $this->Paginator->sort('<%= $field %>') ?></th>
    <% endforeach; %>
        <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    
    
    
    <tbody>
    <?php foreach ($<%= $pluralVar %> as $<%= $singularVar %>): ?>
        <tr>
<%        foreach ($fields as $field) {
            $isKey = false;
            if (!empty($associations['BelongsTo'])) {
                foreach ($associations['BelongsTo'] as $alias => $details) {
                    if ($field === $details['foreignKey']) {
                        $isKey = true;
%>
            <td>
                <?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?>
            </td>
<%
                        break;
                    }
                }
            }
            if ($isKey !== true) {
                if (!in_array($schema->columnType($field), ['integer', 'biginteger', 'decimal', 'float'])) {
%>
            <td><?= h($<%= $singularVar %>-><%= $field %>) ?></td>
<%
                } else {
%>
            <td><?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></td>
<%
                }
            }
        }

        $pk = '$' . $singularVar . '->' . $primaryKey[0];
%>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', <%= $pk %>]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', <%= $pk %>]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', <%= $pk %>], ['confirm' => __('Are you sure you want to delete # {0}?', <%= $pk %>)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
