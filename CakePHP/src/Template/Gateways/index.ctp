<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Gateway'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sites'), ['controller' => 'Sites', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Site'), ['controller' => 'Sites', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Parkinglots'), ['controller' => 'Parkinglots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Parkinglot'), ['controller' => 'Parkinglots', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="gateways index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('label') ?></th>
            <th><?= $this->Paginator->sort('fqdn') ?></th>
            <th><?= $this->Paginator->sort('site_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($gateways as $gateway): ?>
        <tr>
            <td><?= $this->Number->format($gateway->id) ?></td>
            <td><?= h($gateway->label) ?></td>
            <td><?= h($gateway->fqdn) ?></td>
            <td>
                <?= $gateway->has('site') ? $this->Html->link($gateway->site->name, ['controller' => 'Sites', 'action' => 'view', $gateway->site->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $gateway->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gateway->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gateway->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gateway->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<?php $this->Html->addCrumb('Gateways', '/gateways'); ?>
<?php $this->Html->addCrumb('Gateways', ['controller' => 'Gateways', 'action' => 'index']); ?>