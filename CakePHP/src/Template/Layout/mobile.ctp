<?php

$cakeDescription = 'SmartParking@KOM';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    
    <?= $this->Html->css('jquery-ui.min.css') ?>
    <?= $this->Html->css('remoteKey.css') ?>
    
    <?= $this->Html->meta('icon') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    
    
    <?= $this->Html->script('jquery-2.1.4.min.js') ?>
    <?= $this->Html->script('jquery-ui.min.js') ?>
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('mqttws31.js') ?>
    <?= $this->Html->script('config.js') ?>
    
    <script type="text/javascript">
        var mqtt;
        var reconnectTimeout = 6000;
        function MQTTconnect() {
            
            if (typeof path == "undefined") {
                path = '/mqtt';
            }
            mqtt = new Paho.MQTT.Client(
                    host,
                    port,
                    path,
                    "mobileweb_" + parseInt(Math.random() * 1000, 10)
            );
            console.debug(mqtt);
            var options = {
            timeout: 3,
            useSSL: useTLS,
            cleanSession: cleansession,
            onSuccess: onConnect,
            onFailure: function (message) {
                $('#status').text("Connection failed: " + message.errorMessage + "Retrying");
                setTimeout(MQTTconnect, reconnectTimeout);
                }
            };
        
            mqtt.onConnectionLost = onConnectionLost;
            mqtt.onMessageArrived = onMessageArrived;
        
            if (username != null) {
                options.userName = username;
                options.password = password;
            }
            
            console.log("Host="+ host + ", port=" + port + ", path=" + path + " TLS = " + useTLS + " username=" + username + " password=" + password);
            mqtt.connect(options);
        }
    
        function onConnect() {
            $('#status').text('Connected to ' + host + ':' + port + path);
            // Connection succeeded; subscribe to our topic
            mqtt.subscribe(topic, {qos: 0});
            $('#topic').text(topic);
            initializeState();
        }
    
        function onConnectionLost(responseObject) {
            setTimeout(MQTTconnect, reconnectTimeout);
            $('#status').text("connection lost: " + responseObject.errorMessage + ". Reconnecting");
        };
        
        function sendcmdOpen(nodeId, gwid) {
           sendcmd(nodeId,"open", gwid);
        }
        
        function sendcmdClose(nodeId, gwid) {
           sendcmd(nodeId,"close", gwid);
        }
        
        function sendcmdRefresh(nodeId, gwid) {
           sendcmd(nodeId,"getState", gwid);
        }
        
        function sendcmdRecalibrate(nodeId, gwid) {
           sendcmd(nodeId,"recalibrate", gwid);
        }
        
        function sendcmd(nodeId, cmd, gwId) {
           try {
               mqtt.send("parking/"+gwId+"/"+nodeId+"/"+cmd, String(cmd), qos=0);
           } catch(err) {
               alert(err.message);
           }
        }
        
        function onMessageArrived(message) {
        
            var topic = message.destinationName;
            var splitTopic = topic.split("/");
            var payload = message.payloadString;
            
            if (splitTopic[2] == "state" ) {
               var stateId = "#state" + splitTopic[1];
               var errorMessageId = "#errorMessage" + splitTopic[1];
	       var closeButtonId = "#closeButton" + splitTopic[1];
	       var openButtonId = "#openButton" + splitTopic[1];
               if (splitTopic[3] == "F") {
                      $(stateId).text("frei");
                      $(stateId).css("color","lightgreen");
                      $(errorMessageId).text(payload);
                      $(stateId).show();
		      $(closeButtonId).show();
		      $(openButtonId).hide();
                } else if (splitTopic[3] == "B") {
                  $(stateId).text("reserviert");
                  $(stateId).css("color","rgb(255,102,102)");
                  $(errorMessageId).text(payload);
                  $(stateId).show();
		  $(closeButtonId).hide();
		  $(openButtonId).show();
                } else if (splitTopic[3] == "E") {
                  $(stateId).text("Fehler");
                  $(stateId).css("color","blue");
                  $(errorMessageId).text(payload);
		  $(closeButtonId).show();
		  $(openButtonId).show();
                } else if (splitTopic[3] == "P") {
                  $(stateId).text("belegt");
                  $(stateId).css("color","orange");
                  $(errorMessageId).text(payload);
		  $(closeButtonId).hide();
	          $(openButtonId).hide();
                } else if (splitTopic[3] == "U") {
                  $(stateId).text("Reserviere...bitte warten");
                  $(stateId).css("color","purple");
                  $(errorMessageId).text(payload);
                  $(closeButtonId).hide();
                  $(openButtonId).hide();
                } else if (splitTopic[3] == "D") {
                  $(stateId).text("Freigabe...bitte warten");
                  $(stateId).css("color","yellow");
                  $(errorMessageId).text(payload);
                  $(closeButtonId).hide();
                  $(openButtonId).hide();
               } else {
                    $(stateId).text("not connected");
                    $(divId).css("color","lightgrey");
		    $(closeButtonId).hide();
		    $(openButtonId).hide();
               }
            }
            
            if(splitTopic[1] == "sensor") {
                var sensorData = JSON.parse(payload);
                var zValueId = "#zValue" + sensorData.parkinglot_id;
                var zThreshId = "#zThresh" + sensorData.parkinglot_id;
                $(zValueId).text(sensorData.z);
                $(zThreshId).text(sensorData.zThreshold);
            }
            
            if ((splitTopic[0] == "parking") && (splitTopic.length == 3)) {
                var divId = "#parkinglotItem" + splitTopic[1];
                var stateId = "#state" + splitTopic[1];
                var errorMessageId = "#errorMessage" + splitTopic[1];
                if (splitTopic[2] == "open") {
                    $(stateId).text("freeing");
                    $(errorMessageId).text("");
                    $(divId).css("color","yellow");
                    //$('#stateImage').html("<img src='img/go_sign.jpg' width='100px' height='100px' />");
                    //$('#stateImage').show();
                    $(stateId).show();
                } else if (splitTopic[2] == "close") {
                  $(stateId).text("blocking");
                  $(errorMessageId).text("");
                  $(divId).css("color","purple");
                  $(stateId).show();
                } else {
                    $(stateId).text("unknown");
                    $(divId).css("color","lightgrey");
                    //$('#stateImage').hide();
                }              
            }; 
        }
        
        function dbStateToMqttState(dbstate) {
        switch(dbstate) {
            case "occupied":
                return "P";
                break;
            case "blocked":
                return "B";
                break;
            case "free":
                return "F";
                break;
            case "freeing":
                return "D";
                break;
            case "blocking":
                return "U";
                break;
            case "error":
                return "E";
                break;
            default:
                return "U";
        }
    }
        
        //$(document).ready(function() {
           //MQTTconnect();
         //});

    </script>
</head>
<body>
    <header id="remoteKeyHeader" style="text-align:center;">
    	<div>
            Willkommen an der TU Darmstadt.
            <h3>Smart Parking System <br />Mobile Control</h3>
        </div>
        <div style="background-color: #0000cc;width:100%;height:10px; float:clear;"/>
    </header>
    <div style="background-color: #000;width:100%;height:1px;margin-top:3px;"/>
    
            <?= $this->Flash->render() ?>
            <div>
                <?= $this->fetch('content') ?>
            </div>
     
    <footer id="remoteKeyFooter" style="text-align:center;display:none;">
        <hr />
        <div>
         <?= $this->Html->image('kom_logo.png'); ?>
      	 <!-- <?= $this->Html->image('kom_logo.png'); ?> -->
        </div>
    </footer>
   
</body>
</html>
