package com.tud.kom.parkinglot.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;
import java.sql.Timestamp;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ReservationTest {

    private Reservation res1;
    private Reservation res2;

    @Before
    public void setUp() {
        res1 = new Reservation();
        res2 = new Reservation();

    }

    @Test
    public void testEqualsNull() {
        assertFalse(res1.equals(null));
        assertTrue(res1.equals(res2));
        assertTrue(res2.equals(res1));
        assertTrue(res1.equals(res1));

    }

    @Test
    public void testEqualsID() {
        res1.setId(1);
        assertFalse(res1.equals(res2));
        assertFalse(res2.equals(res1));
        res2.setId(1);
        assertTrue(res1.equals(res2));
        assertTrue(res2.equals(res1));
        res1.setId(2);
        assertFalse(res1.equals(res2));
        assertFalse(res2.equals(res1));

    }

    @Test
    public void testEqualsStartTime() {
        Timestamp t1 = new Timestamp(10000);
        Timestamp t2 = new Timestamp(9000);
        res1.setStartTime(t1);
        assertFalse(res1.equals(res2));
        assertFalse(res2.equals(res1));
        res2.setStartTime(t1);
        assertTrue(res1.equals(res2));
        assertTrue(res2.equals(res1));
        res1.setStartTime(t2);
        assertFalse(res1.equals(res2));
        assertFalse(res2.equals(res1));

    }

    @Test
    public void testEqualsEndTime() {
        Timestamp t1 = new Timestamp(10000);
        Timestamp t2 = new Timestamp(9000);
        res1.setEndTime(t1);
        assertFalse(res1.equals(res2));
        assertFalse(res2.equals(res1));
        res2.setEndTime(t1);
        assertTrue(res1.equals(res2));
        assertTrue(res2.equals(res1));
        res1.setEndTime(t2);
        assertFalse(res1.equals(res2));
        assertFalse(res2.equals(res1));

    }

    @Test
    public void testEqualsParkinglot() {
        ParkingLot p1 = new ParkingLot();
        ParkingLot p2 = new ParkingLot();
        res1.setParkingLot(p1);
        assertFalse(res1.equals(res2));
        assertFalse(res2.equals(res1));
        res2.setParkingLot(p1);
        assertTrue(res1.equals(res2));
        assertTrue(res2.equals(res1));
        res1.setParkingLot(p2);
        assertFalse(res1.equals(res2));
        assertFalse(res2.equals(res1));

    }

    @Test
    public void testEqualsToken() {
        res1.setToken("token");
        assertFalse(res1.equals(res2));
        assertFalse(res2.equals(res1));
        res2.setToken("token");
        assertTrue(res1.equals(res2));
        assertTrue(res2.equals(res1));
        res1.setToken("token2");
        assertFalse(res1.equals(res2));
        assertFalse(res2.equals(res1));

    }

    @Test
    public void equals() {
        Reservation reservation1 = new Reservation();
        Reservation reservation2 = new Reservation();

        assertFalse(reservation1.equals(null));
        assertFalse(reservation2.equals(null));

        reservation1.setId(1);
        reservation2.setId(2);
        assertFalse(reservation1.equals(reservation2));

        reservation2.setId(1);
        reservation1.setEndTime(null);
        reservation2.setEndTime(new Timestamp(1));
        assertFalse(reservation1.equals(reservation2));

        reservation1.setEndTime(new Timestamp(0));
        assertFalse(reservation1.equals(reservation2));

        reservation2.setEndTime(new Timestamp(0));
        reservation1.setParkingLot(null);
        reservation2.setParkingLot(new ParkingLot());
        assertFalse(reservation1.equals(reservation2));

        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setId(1);
        reservation1.setParkingLot(parkingLot);
        assertFalse(reservation1.equals(reservation2));

        reservation2.setParkingLot(parkingLot);
        reservation1.setStartTime(null);
        reservation2.setStartTime(new Timestamp(1));
        assertFalse(reservation1.equals(reservation2));

        reservation1.setStartTime(new Timestamp(0));
        assertFalse(reservation1.equals(reservation2));

        reservation2.setStartTime(new Timestamp(0));
        reservation1.setToken(null);
        reservation2.setToken("Test2");
        assertFalse(reservation1.equals(reservation2));

        reservation1.setToken("Test1");
        assertFalse(reservation1.equals(reservation2));

        assertTrue(reservation1.equals(reservation1));
        assertFalse(reservation1.equals("Reservation"));

        reservation1.setId(1);
        reservation2.setId(1);
        reservation1.setEndTime(null);
        reservation2.setEndTime(null);
        assertFalse(reservation1.equals(reservation2));
    }

    @Test
    public void testHashcode() {
        Assert.assertEquals(0, res1.hashCode());
        res1.setToken("");
        res1.setParkingLot(new ParkingLot());
        res1.setEndTime(new Date(0));
        res1.setStartTime(new Date(0));
        Assert.assertEquals(res1.hashCode(), res1.hashCode());
    }

    @Test
    public void testToString() {
        Assert.assertNotNull(res1.toString());
    }
}