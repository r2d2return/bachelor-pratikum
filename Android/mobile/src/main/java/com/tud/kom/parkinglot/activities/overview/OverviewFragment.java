package com.tud.kom.parkinglot.activities.overview;

import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.activities.reservation.ReservationActivity;
import com.tud.kom.parkinglot.base.BaseRecyclerViewAdapter.OnItemClickedListener;
import com.tud.kom.parkinglot.model.Reservation;

import static com.tud.kom.parkinglot.activities.overview.OverviewViewModel.ERROR_CLOSE_PARKING_LOT;
import static com.tud.kom.parkinglot.activities.overview.OverviewViewModel.ERROR_DELETING_RESERVATION;
import static com.tud.kom.parkinglot.activities.overview.OverviewViewModel.ERROR_OPEN_PARKING_LOT;

/**
 * A fragment displaying a list of reservations and listening for time changes.
 */
public class OverviewFragment extends DialogFragment {

    private static IntentFilter sIntentFilter;

    static {
        sIntentFilter = new IntentFilter();
        sIntentFilter.addAction(Intent.ACTION_TIME_TICK);
        sIntentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        sIntentFilter.addAction(Intent.ACTION_TIME_CHANGED);
    }

    private ReservationAdapter mAdapter;
    private BroadcastReceiver mBroadcastReceiver;
    private RecyclerView mList;
    private OverviewViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overview, container, false);

        viewModel = ViewModelProviders.of(this, new OverviewViewModelFactory(getContext())).get(OverviewViewModel.class);

        mList = view.findViewById(R.id.list);
        TextView emptyView = view.findViewById(R.id.empty_view);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        mList.setLayoutManager(llm);

        TextView mTextEmptyList = view.findViewById(R.id.list_empty);

        viewModel.getData().observe(this, reservations -> {
            mAdapter.setData(reservations);
            mAdapter.changeReservationStatus();

            if (reservations == null || reservations.isEmpty()) {
                emptyView.setVisibility(View.VISIBLE);
            }
            else {
                emptyView.setVisibility(View.GONE);
            }
        });

        viewModel.getError().observe(this, this::onError);

        SwipeRefreshLayout refreshLayout = view.findViewById(R.id.swipe_layout);

        viewModel.isLoading().observe(this, refreshLayout::setRefreshing);

        viewModel.getReservation().observe(this, reservation -> mAdapter.changeParkingLotState(reservation));

        viewModel.getDeleted().observe(this, reservation -> {
            mAdapter.delete(reservation);

            if (mAdapter.getItemCount() == 0) {
                emptyView.setVisibility(View.VISIBLE);
            }
            else {
                emptyView.setVisibility(View.GONE);
            }
        });

        refreshLayout.setOnRefreshListener(this::loadData);

        view.findViewById(R.id.floatingActionButton).setOnClickListener(v -> onFabClicked());

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
                mAdapter.changeReservationStatus();
            }
        };

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter = new ReservationAdapter(false, new OnItemClickedListener<Reservation>() {
            @Override
            public void onDeleteClicked(final Reservation item) {
                viewModel.deleteReservation(item);
            }

            @Override
            public void onItemClicked(final Reservation item) {
                viewModel.listItemClicked(item);
            }
        });
        mList.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(mBroadcastReceiver, sIntentFilter);
        loadData();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mBroadcastReceiver);
    }

    /**
     * Get the current and future reservations of the app user.
     */
    private void loadData() {
        viewModel.loadData();
    }

    /**
     * Handle download or parsing errors.
     * Display the error message in a toast.
     *
     * @param error the error message
     */
    private void onError(final String error) {
        switch (error) {
            case ERROR_CLOSE_PARKING_LOT:
                Toast.makeText(getContext(), R.string.error_close_parking_lot, Toast.LENGTH_SHORT).show();
                break;
            case ERROR_DELETING_RESERVATION:
                Toast.makeText(getContext(), R.string.error_delete_reservation, Toast.LENGTH_SHORT).show();
                break;
            case ERROR_OPEN_PARKING_LOT:
                Toast.makeText(getContext(), R.string.error_open_parking_lot, Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(getContext(), R.string.internal_server_error, Toast.LENGTH_SHORT).show();


        }
    }

    /**
     * Start a new reservations activity.
     */
    private void onFabClicked() {
        Intent intent = new Intent(getActivity(), ReservationActivity.class);
        startActivity(intent);
    }
}
