<?php
/**
  * @var \App\View\AppView $this
  */
?>
<form>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
        <li class="btn btn-default"><?= $this->Html->link(__('New Auth Acl'), ['action' => 'add']) ?></li>
        <li class="btn btn-default"><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
        <li class="btn btn-default"><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
    </ul>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               <h3><?= __('Auth Acl') ?></h3>
            </div>
    
    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('controller') ?></th>
                <th scope="col"><?= $this->Paginator->sort('actions') ?></th>
                <th scope="col"><?= $this->Paginator->sort('role_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($authAcl as $authAcl): ?>
            <tr>
                <td><?= $this->Number->format($authAcl->id) ?></td>
                <td><?= h($authAcl->controller) ?></td>
                <td><?= h($authAcl->actions) ?></td>
                <td><?= $authAcl->has('role') ? $this->Html->link($authAcl->role->name, ['controller' => 'Roles', 'action' => 'view', $authAcl->role->id]) : '' ?></td>
                <td class="actions">

                    <?= $this->Html->image('view.gif', [ 'alt' => __('View'), 'url' => ['controller' => 'AuthAcl', 'action' => 'view', $authAcl->id]]) ?>

                    <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'), 'url' => ['controller' => 'AuthAcl', 'action' => 'edit', $authAcl->id]]) ?>

                    <?= $this->Form->postlink($this->Html->image('delete.png', [ 'alt' => __('Delete')]), ['controller' => 'AuthAcl', 'action' => 'delete', $authAcl->id], ['confirm' => __('Are you sure you want to delete Action->{0}, on Controller->{1} for Role->{2}', $authAcl->actions, $authAcl->controller, $authAcl->role_id), 'escapeTitle' => false]) ?>
                    
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    </div>
</div>
</div>
</form>
<?php $this->Html->addCrumb('AuthAcl', ['controller' => 'AuthAcl', 'action' => 'index']); ?>