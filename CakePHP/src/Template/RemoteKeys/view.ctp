<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit RemoteKey'), ['action' => 'edit', $remoteKey->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete RemoteKey'), ['action' => 'delete', $remoteKey->id], ['confirm' => __('Are you sure you want to delete # {0}?', $remoteKey->id)]) ?> </li>
        <li><?= $this->Html->link(__('List RemoteKeys'), ['controller' => 'RemoteKeys', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New RemoteKey'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="remoteKeys view large-10 medium-9 columns">
    <h2><?= h($remoteKey->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Username') ?></h6>
            <p><?= h($remoteKey->username) ?></p>
            <h6 class="subheader"><?= __('Email') ?></h6>
            <p><?= h($remoteKey->mail) ?></p>
            <h6 class="subheader"><?= __('Token') ?></h6>
            <p><?= h($remoteKey->token) ?></p>
            <h6 class="subheader"><?= __('Parking Lot') ?></h6>
            <p><?= $remoteKey->has('parkinglot') ? $this->Html->link($remoteKey->parkinglot->id, ['controller' => 'Parkinglots', 'action' => 'view', $remoteKey->parkinglot->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($remoteKey->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('valid from') ?></h6>
            <p><?= h($remoteKey->valid_from) ?></p>
            <h6 class="subheader"><?= __('valid until') ?></h6>
            <p><?= h($remoteKey->valid_until) ?></p>
        </div>
    </div>
</div>

<?php $this->Html->addCrumb('Remote Keys', '/remoteKeys'); ?>
<?php $this->Html->addCrumb('Details', ['controller' => 'RemoteKeys', 'action' => 'view']); ?>
