package com.tud.kom.parkinglot.network;

import android.os.AsyncTask;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.tud.kom.parkinglot.BuildConfig;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * send a request and get a response
 */
public class NetworkTask extends AsyncTask<Request, Boolean, Tuple<Request, Response>> {
    private static final String TAG = NetworkTask.class.getSimpleName();
    @VisibleForTesting
    public static String baseUrl = "https://www.tobiasmartine.de/api/";
    private NetworkCallback networkCallback;

    /**
     * instantiate new networktask
     *
     * @param networkCallback the callback for handling the reposonse
     */
    NetworkTask(NetworkCallback networkCallback) {
        this.networkCallback = networkCallback;
    }

    /**
     * network request on new thread
     *
     * @param requests must have 1 element: the request
     * @return the networkresponse
     */
    @Override
    protected Tuple<Request, Response> doInBackground(Request[] requests) {
        if (requests.length != 1)
            return new Tuple<>(null, new Response(-2, "doInBackground parameter bounds not 1: " + requests.length, null));
        HttpURLConnection conn = null;
        String resultString = null;
        int respCode = -1;
        String respMess = "";
        InputStream is = null;
        try {
            conn = (HttpURLConnection) new URL(baseUrl + requests[0].formattedUrlExtWithParams()).openConnection();
            conn.setRequestMethod(requests[0].getRequestType().name());
            conn.setRequestProperty("Accept", "application/json");
            conn.setReadTimeout(3000);
            conn.setConnectTimeout(5000);
            conn.setUseCaches(false);
            this.writeOutputs(conn, requests[0]);
            conn.connect();
            respCode = conn.getResponseCode();
            respMess = conn.getResponseMessage();
            if (respCode < 400)
                is = conn.getInputStream();
            else
                is = conn.getErrorStream();
            StringBuilder sb = new StringBuilder();
            for (int ch = is.read(); ch != -1; ch = is.read())
                sb.append((char) ch);
            resultString = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (Exception ignored) {
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
        return new Tuple<>(requests[0], new Response(respCode, respMess, resultString));
    }

    /**
     * after network request: response callback
     *
     * @param resp networkresponse
     */
    @Override
    protected void onPostExecute(Tuple<Request, Response> resp) {
        super.onPostExecute(resp);
        if (resp.getSecond().getRespCode() != 200)
            Log.e(TAG, "Bad Network Response: \n" + resp.getFirst() + "\n" + resp.getSecond());
        else if (BuildConfig.DEBUG)
            Log.v(TAG, "Good Network Response: \n" + resp.getFirst() + "\n" + resp.getSecond());

        if (this.networkCallback != null)
            this.networkCallback.handleWebsiteResponse(resp.getSecond());
    }

    /**
     * write out the parameters from the request into the httpurlconnection
     *
     * @param conn    httpurlconnection
     * @param request request with parameters
     */
    private void writeOutputs(HttpURLConnection conn, Request request) {
        //write out header params
        for (int i = 0; i < request.getHeaderParams().size(); i++)
            conn.setRequestProperty(request.getHeaderParams().get(i).getFirst(), request.getHeaderParams().get(i).getSecond());

        //write out body params
        String bodyParams = request.formattedBodyParams();
        if (bodyParams != null) {
            conn.setDoInput(true);
            OutputStream os = null;
            try {
                os = conn.getOutputStream();
                os.write(bodyParams.getBytes(StandardCharsets.UTF_8));
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                } catch (Exception ignored) {
                }
            }
        }
    }
}