package com.tud.kom.parkinglot.activities.history;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.support.annotation.NonNull;

import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.SavedPreferences.Filename;

public class HistoryViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private String uname, pwd;

    public HistoryViewModelFactory(Context c) {
        this.uname = SavedPreferences.getPreference(c, Filename.USERNAME);
        this.pwd = SavedPreferences.getPreference(c, Filename.PASSWORD);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new HistoryViewModel(uname, pwd);
    }
}
