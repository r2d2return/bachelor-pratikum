package com.tud.kom.parkinglot.activities.overview;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.base.BaseRecyclerViewAdapter;
import com.tud.kom.parkinglot.model.Reservation;

public class ReservationAdapter extends BaseRecyclerViewAdapter<Reservation, ReservationViewHolder> {

    private final boolean mSimpleItemView;

    /**
     * @param listener a listener to card clicked events
     */
    public ReservationAdapter(boolean simpleItemView, final OnItemClickedListener<Reservation> listener) {
        super(new ReservationDiffUtilsCallback(), listener);
        mSimpleItemView = simpleItemView;
    }

    public void changeParkingLotState(final Reservation reservation) {
        notifyItemChanged(mList.indexOf(reservation));
    }

    /**
     * Check for each reservation if it is currently active and if its state changed since the last call update the
     * item accordingly.
     */
    public void changeReservationStatus() {
        notifyDataSetChanged();
    }

    public void delete(final Reservation reservation) {
        int index = mList.indexOf(reservation);
        mList.remove(reservation);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemViewType(final int position) {
        if (mSimpleItemView) {
            return R.layout.history_item;
        }
        else {
            return position == 0 ? R.layout.view_first_card_item : R.layout.view_card_item;
        }
    }

    @Override
    public ReservationViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        ImageButton button = view.findViewById(R.id.button_delete);
        ReservationViewHolder viewHolder = new ReservationViewHolder(view, parent.getContext());
        if (mListener != null) {
            view.setOnClickListener(item -> mListener.onItemClicked(mList.get(viewHolder.getAdapterPosition())));
            if (button != null) {
                button.setOnClickListener(item -> alertDelete(button, viewHolder, parent));
            }
        }
        return viewHolder;
    }

    private void alertDelete(ImageButton button, ReservationViewHolder viewHolder, ViewGroup parent) {
        DialogInterface.OnClickListener dialogClickListener = (dialogInterface, i) -> {
            switch (i) {
                case DialogInterface.BUTTON_POSITIVE:
                    mListener.onDeleteClicked(mList.get(viewHolder.getAdapterPosition()));
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;

            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(parent.getContext());
        builder.setMessage(R.string.delete_confirmation).setPositiveButton(R.string.yes_string, dialogClickListener).setNegativeButton(R.string.no_string, dialogClickListener).show();

    }
}
