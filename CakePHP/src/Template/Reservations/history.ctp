<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">  
            <li class="btn btn-default"><?= $this->Html->link(__('New Reservation'), ['action' => 'add']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Reservations'), ['action' => 'index']) ?></li>
            <?php if($currentAuthUserRoles[0] == "Admin"): ?>
                <li class="btn btn-default"><?= $this->Html->link(__('List Parking Lots'), ['controller' => 'Parkinglots', 'action' => 'index']) ?></li>
                <li class="btn btn-default"><?= $this->Html->link(__('List Sensorlogs'), ['controller' => 'Sensorlogs', 'action' => 'index']) ?></li>
            <?php endif?>
            <li class="btn btn-default"><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>



<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                History
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr>
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('parkinglot_id') ?></th>
                            <th><?= $this->Paginator->sort(__('reservationState')) ?></th>
                            <th>Visitor</th>
                            <th>Host</th>
                            <th><?= $this->Paginator->sort('start') ?></th>
                            <th><?= $this->Paginator->sort('end') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                    <?php if($currentAuthUserRoles[0] == "Admin"): ?>
                        <tbody>
                            <?php $counter = 1; ?>
                            <?php foreach ($reservations as $reservation): ?>
                                        <tr role="row" class="gradeA <?php echo ($counter%2 == 0 ?  'odd': 'even'); ?>">
                                        <td class="sorting_1"><?= $this->Number->format($reservation->id) ?></td>
                                        <td><?= $this->Html->link($reservation->parkinglot->description, ['action' => 'view', $reservation->id])?></td>
                                        <td><?= h($reservation->reservationState) ?></td>
                                        <td><?= $this->Html->link($reservation->user->username, ['controller' => 'Users', 'action' => 'view', $reservation->user->id])?></td>
                                        <?php $temp = $reservation->user->id;
                                              $reservation->user->id = $reservation->visitedPersonId;
                                        ?>
                                        <td><?= $this->Html->link($reservation->visitedPersonId, ['controller' => 'Users', 'action' => 'view', $reservation->user->id])?></td>
                                        <?php $reservation->user->id = $temp; ?>
                                        <td><?= h($reservation->start) ?></td>
                                        <td><?= h($reservation->end) ?></td>
                                        <td class="actions center">
                                            <?= $this->Html->image('view.gif', [ 'alt' => __('View'),
                                                                     'url' => ['controller' => 'Reservations',
                                                                               'action' => 'view', $reservation->id]]) ?>
                                            <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'),
                                                                     'url' => ['controller' => 'Reservations',
                                                                               'action' => 'edit', $reservation->id]
                                                                   ]) ?>
                                            <?= $this->Html->link(
                                                      $this->Html->image('mail.png', [ 'alt' => __('Resend'), 'width' => '16px', 'height' => '14px']),
                                                      ['controller' => 'Reservations', 'action' => 'resend', $reservation->id],
                                                      ['confirm' => __('Are you sure you want to resend the reservation Email to  # {0}?', $reservation->user->username),
                                                       'escapeTitle' => false]
                                                     ) ?>
                                            <?= $this->Html->link(
                                                      $this->Html->image('delete.png', [ 'alt' => __('Delete')]),
                                                      ['controller' => 'Reservations', 'action' => 'delete', $reservation->id],
                                                      ['confirm' => __('Are you sure you want to delete # {0}?', $reservation->id),
                                                       'escapeTitle' => false]
                                                     ) ?>
                                        </td>
                                    </tr>
                            <?php $counter++; ?>
                        <?php endforeach; ?>
                    </tbody>
                <?php elseif($currentAuthUserRoles[0] == "Reservation Manager"): ?>
                        <tbody>
                            <?php $counter = 1; ?>
                            <?php foreach ($reservations as $reservation): ?>
                                <?php foreach ($parkinglots as $parkinglot): ?>
                                    <?php if($reservation->parkinglot->description == $parkinglot): ?>
                                        <tr role="row" class="gradeA <?php echo ($counter%2 == 0 ?  'odd': 'even'); ?>">
                                        <td class="sorting_1"><?= $this->Number->format($reservation->id) ?></td>
                                        <td><?= $this->Html->link($reservation->parkinglot->description, ['action' => 'view', $reservation->id])?></td>
                                        <td><?= h($reservation->reservationState) ?></td>
                                        <td><?= $this->Html->link($reservation->user->username, ['controller' => 'Users', 'action' => 'view', $reservation->user->id])?></td>
                                        <?php $temp = $reservation->user->id;
                                              $reservation->user->id = $reservation->visitedPersonId;
                                        ?>
                                        <td><?= $this->Html->link($reservation->visitedPersonId, ['controller' => 'Users', 'action' => 'view', $reservation->user->id])?></td>
                                        <?php $reservation->user->id = $temp; ?>
                                        <td><?= h($reservation->start) ?></td>
                                        <td><?= h($reservation->end) ?></td>
                                        <td class="actions center">
                                            <?= $this->Html->image('view.gif', [ 'alt' => __('View'),
                                                                     'url' => ['controller' => 'Reservations',
                                                                               'action' => 'view', $reservation->id]]) ?>
                                            <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'),
                                                                     'url' => ['controller' => 'Reservations',
                                                                               'action' => 'edit', $reservation->id]
                                                                   ]) ?>
                                            <?= $this->Html->link(
                                                      $this->Html->image('mail.png', [ 'alt' => __('Resend'), 'width' => '16px', 'height' => '14px']),
                                                      ['controller' => 'Reservations', 'action' => 'resend', $reservation->id],
                                                      ['confirm' => __('Are you sure you want to resend the reservation Email to  # {0}?', $reservation->user->username),
                                                       'escapeTitle' => false]
                                                     ) ?>
                                            <!-- <?= $this->Html->link(
                                                      $this->Html->image('delete.png', [ 'alt' => __('Delete')]),
                                                      ['controller' => 'Reservations', 'action' => 'delete', $reservation->id],
                                                      ['confirm' => __('Are you sure you want to delete # {0}?', $reservation->id),
                                                       'escapeTitle' => false]
                                                     ) ?> -->
                                        </td>
                                    </tr>
                                    <?php $counter++; ?>
                                <?php endif?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </tbody>
                <?php endif?>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p><?= $this->Paginator->counter() ?></p>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
        <!-- /.col-lg-12 -->
</div>

<?php $this->Html->addCrumb('Reservations', '/reservations'); ?>
<?php $this->Html->addCrumb('History', ['controller' => 'Reservations', 'action' => 'history']); ?>