<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
            <li class="btn btn-default"><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $parkinglot->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $parkinglot->id)]
            )?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Parkinglots'), ['action' => 'index']) ?></li>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Edit ParkingLot
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <?= $this->Form->create($parkinglot) ?>
                        <fieldset>
                            <legend><?= __('Add ParkingLot') ?></legend>
                            <?php

                                echo "<div class='form-group'>".$this->Form->input('pid', ['label' => 'PID:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('description', ['label' => 'Description:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('mqttchannel', ['label' => 'MQTT Channel:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('site', ['label' => 'Site:', 'class' => 'form-control'])."</div>";
                            ?>
                        </fieldset>
                        <?= $this->Form->button('Submit',  array('class' => 'btn btn-default')); ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php $this->Html->addCrumb('Parkinglots', '/parkinglots'); ?>
<?php $this->Html->addCrumb('Edit', ['controller' => 'Parkinglots', 'action' => 'edit']); ?>