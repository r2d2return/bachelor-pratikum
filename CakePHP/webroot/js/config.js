//host = '130.83.245.98';	// hostname or IP address
//host = 'test.mosquitto.org';
//host = 'echo.websocket.org';
host = 'smartparking.hornjak.de'
//port = 8011;
//port = 8080;
//port = 443;
//port = 9001;
port = 80;
topic = 'parking/#';		// topic to subscribe to
useTLS = true;
username = null;
password = null;
// username = "jjolie";
// password = "aa";

// path as in "scheme:[//[user:password@]host[:port]][/]path[?query][#fragment]"
//    defaults to "/mqtt"
//    may include query and fragment
//
// path = "/mqtt";
// path = "/data/cloud?device=12345";

cleansession = true;
