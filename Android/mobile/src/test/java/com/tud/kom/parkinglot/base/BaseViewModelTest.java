package com.tud.kom.parkinglot.base;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;

import com.tud.kom.parkinglot.network.NetworkCallback;
import com.tud.kom.parkinglot.network.Response;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;

public class BaseViewModelTest {
    @Rule
    public InstantTaskExecutorRule mRule = new InstantTaskExecutorRule();
    @Mock
    private Observer dataObserver;
    @Mock
    private Observer errorObserver;
    @Mock
    private Observer loadingObserver;
    private boolean mMakeNetworkCallCalled;
    private boolean mParseNetworkResponseCalled;
    private BaseViewModel mViewModel;
    private List responseList = new ArrayList();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mViewModel = new BaseViewModel("smart", "parking") {
            @Override
            protected void makeNetworkCall(final String username, final String password, final NetworkCallback callback) {
                mMakeNetworkCallCalled = true;
            }

            @NonNull
            @Override
            protected List parseNetworkResponse(final Response response) {
                mParseNetworkResponseCalled = true;
                return responseList;
            }
        };

        mParseNetworkResponseCalled = false;
        mMakeNetworkCallCalled = false;
    }

    @Test
    public void testGetData() {
        assertTrue(mViewModel.getData() != null);
        assertTrue(mViewModel.getData() != null);
    }

    @Test
    public void testGetError() {
        assertTrue(mViewModel.getError() != null);
        assertTrue(mViewModel.getError() != null);
    }

    @Test
    public void testGetLogout() {
        assertTrue(mViewModel.getLogout() != null);
        assertTrue(mViewModel.getLogout() != null);
    }

    @Test
    public void testHandleWebsiteResponse_failure_JSONException() {
        mViewModel = new BaseViewModel("smart", "parking") {
            @Override
            protected void makeNetworkCall(final String username, final String password, final NetworkCallback callback) {
                mMakeNetworkCallCalled = true;
            }

            @NonNull
            @Override
            protected List parseNetworkResponse(final Response response) throws JSONException {
                mParseNetworkResponseCalled = true;
                throw new JSONException("Test");
            }
        };

        mViewModel.isLoading().observeForever(loadingObserver);
        mViewModel.getError().observeForever(errorObserver);

        mViewModel.isLoading();
        mViewModel.getError();
        Response response = new Response(200, "", "");
        mViewModel.handleWebsiteResponse(response);

        verify(loadingObserver).onChanged(false);
        assertTrue(mParseNetworkResponseCalled);
        verify(errorObserver).onChanged("Test");
    }

    @Test
    public void testHandleWebsiteResponse_failure_ParseException() {
        mViewModel = new BaseViewModel("smart", "parking") {
            @Override
            protected void makeNetworkCall(final String username, final String password, final NetworkCallback callback) {
                mMakeNetworkCallCalled = true;
            }

            @NonNull
            @Override
            protected List parseNetworkResponse(final Response response) throws ParseException {
                mParseNetworkResponseCalled = true;
                throw new ParseException("Test", 3);
            }
        };

        mViewModel.isLoading().observeForever(loadingObserver);
        mViewModel.getError().observeForever(errorObserver);

        mViewModel.isLoading();
        mViewModel.getError();
        Response response = new Response(200, "", "");
        mViewModel.handleWebsiteResponse(response);

        verify(loadingObserver).onChanged(false);
        assertTrue(mParseNetworkResponseCalled);
        verify(errorObserver).onChanged("Test");
    }

    @Test
    public void testHandleWebsiteResponse_failure_responseBodyNull() {
        mViewModel.isLoading().observeForever(loadingObserver);
        mViewModel.getError().observeForever(errorObserver);

        mViewModel.isLoading();
        Response response = new Response(200, "", null);
        mViewModel.handleWebsiteResponse(response);

        verify(loadingObserver).onChanged(false);
        assertFalse(mParseNetworkResponseCalled);
        verify(errorObserver).onChanged("ErrorRespBodyIsNull");
    }

    @Test
    public void testHandleWebsiteResponse_failure_responseCode404() {
        mViewModel.isLoading().observeForever(loadingObserver);
        mViewModel.getError().observeForever(errorObserver);

        mViewModel.isLoading();
        Response response = new Response(404, "", null);
        mViewModel.handleWebsiteResponse(response);

        verify(loadingObserver).onChanged(false);
        assertFalse(mParseNetworkResponseCalled);
        verify(errorObserver).onChanged("ErrorRespCodeNotAsExpected: 404");
    }

    @Test
    public void testHandleWebsiteResponse_success() {
        mViewModel.isLoading().observeForever(loadingObserver);
        mViewModel.getData().observeForever(dataObserver);

        mViewModel.isLoading();
        Response response = new Response(200, "", "");
        mViewModel.handleWebsiteResponse(response);

        verify(loadingObserver).onChanged(false);
        assertTrue(mParseNetworkResponseCalled);
        verify(dataObserver).onChanged(responseList);
    }

    @Test
    public void testIsLoading() {
        assertTrue(mViewModel.isLoading() != null);
        assertTrue(mViewModel.isLoading() != null);
    }

    @Test
    public void testLoadData() {
        mViewModel.isLoading().observeForever(loadingObserver);

        mViewModel.loadData();
        verify(loadingObserver).onChanged(true);
        assertTrue(mMakeNetworkCallCalled);
    }
}