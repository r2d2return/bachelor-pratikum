<div id="loaderDiv" class="loader"></div>
<div id="remoteKeyContent" style="display: none;">
	<table style="margin: auto; text-align:left;">
		<tr>
			<td style="font-weight:bold;">Benutzer:</td>
			<td><?= h($remoteKey->user->username) ?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;">Parkplatz:</td>
			<td><?= h($remoteKey->parkinglot->description) ?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;">Parkbeginn:</td>
			<td><?= h($start) ?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;">Parkende:</td>
			<td><?= h($end) ?></td>
		</tr>
		<?php
			if ($isActive) {
		?>
		
		<tr>
		  <td style="font-weight: bold;">Parkplatzstatus:</td><td><span id="state<?php echo $remoteKey->parkinglot->pid ?>">verbinde...</span></td>
		</tr>
		
		<tr>
		  <td colspan="2">
		      <div>
	          <button id="openButton<?php echo $remoteKey->parkinglot->pid ?>" class="button" style="background-color: rgb(0,230,0);display: none;" onClick="sendcmdOpen(<?php echo $remoteKey->parkinglot->pid . "," . $remoteKey->parkinglot->gateway_id ?>)" >Parkplatz freigeben</button> <br />
                  <button id="closeButton<?php echo $remoteKey->parkinglot->pid ?>" class="button" style="background-color: rgb(230,0,0);display: none;" onClick="sendcmdClose(<?php echo $remoteKey->parkinglot->pid . "," . $remoteKey->parkinglot->gateway_id ?>)">Parkplatz sperren</button><br />
              </div>
		  </td>
		</tr>
		
		<!--
		<tr>
			<td colspan="2" style="text-align:center;background-color:rgb(0,230,0);font-weight:bold;font-size:2em;"><?= $this->Html->link('free',['controller' => 'Parking', 'action' => 'openRemoteKey', $remoteKey->token]); ?></td>
	    </tr>
	    <tr>
            <td colspan="2" style="text-align:center;background-color:rgb(230,0,0);font-weight:bold;font-size:2em;"><?= $this->Html->link('block',['controller' => 'Parking', 'action' => 'closeRemoteKey', $remoteKey->token]); ?></td>
        </tr>
        -->
	    <?php } else {
	    ?>
	    <tr>
			<td colspan="2" style="text-align:center;background-color:rgb(230,230,230);font-weight:bold;font-size:1.1em;">Freigabe nur während der Parkzeit erlaubt.</td>
	    </tr>
	    <?php } 
	    ?>
	</table>
	
	<!--
	<div align="center" style="width:100%;"><span id="status" align="center" style="text-align:center;align:center;"></span></div>
	
	<div align="center" style="width:100%;">
	z: <span id="zValue<?php echo $remoteKey->parkinglot->pid ?>" align="center" style="text-align:center;align:center;"></span><br />
	zThresh: <span id="zThresh<?php echo $remoteKey->parkinglot->pid ?>" align="center" style="text-align:center;align:center;">
	</div>
	-->


	
</div>
<script type="text/javascript">
          $(document).ready(function() {
            MQTTconnect();
            
            setTimeout(function() { 
                sendcmdRefresh(<?php echo $remoteKey->parkinglot->pid . "," . $remoteKey->parkinglot->gateway_id ?>);
            }
            ,30000);
          });
          
          function initializeState() {
            var parkinglot = <?php echo json_encode($remoteKey->parkinglot); ?>;
            console.log(parkinglot);
            try {
               mqtt.send("parking/"+parkinglot.pid+"/state/"+dbStateToMqttState(parkinglot.status), "0", qos=0);
               mqtt.send("parking/"+parkinglot.gateway_id+"/"+parkinglot.pid+"/getState/", "0", qos=0);
               } catch(err) {
                   console.log(err.message);
               }
            document.getElementById("loaderDiv").style.display = "none";
            //document.getElementById("remoteKeyHeader").style.display = "block";
            document.getElementById("remoteKeyContent").style.display = "block";
            document.getElementById("remoteKeyFooter").style.display = "block";
            //$('#remoteKeyLoader').css('visibility', 'hidden');
            //$('#remoteKeyContent').css('visibility', 'visible');
           }
</script>
<?php $this->Html->addCrumb('Parking', '/parking'); ?>
<?php $this->Html->addCrumb('Remote Key', ['controller' => 'Parking', 'action' => 'remotekey']); ?>