<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CalenderTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CalenderTable Test Case
 */
class CalenderTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CalenderTable
     */
    public $Calender;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.calender'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Calender') ? [] : ['className' => CalenderTable::class];
        $this->Calender = TableRegistry::get('Calender', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Calender);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
