<div id="main">
    <!--
    <span style="font-size:smaller;">Mqtt Broker connection status: <span id="status"></span> ; subscribed topic: <span
                id="topic"></span><br/></span>

    -->

    <?php foreach ($parkinglots as $zone): ?>

        <?php foreach ($zone->sites as $site): ?>

        <?php foreach ($site->parkinglots as $parkinglot): ?>

            <div id="parkinglotItem<?php echo $parkinglot->pid ?>" class="parkinglotItem">
        <?= h($parkinglot->description) ?><br/>
        <?= h($site->name) ?><br/>
        
		<?php if ($nameRole == "Admin"): ?>
        	Parkinglot_id: <?= $this->Number->format($parkinglot->pid) ?><br/>
        <?php endif; ?>
        <!-- Channel: <?= h($parkinglot->mqttchannel) ?><br /> -->
        State:
        <span id="state<?php echo $parkinglot->pid ?>">unknown</span><br/>
        <span id="errorMessage<?php echo $parkinglot->pid ?>"></span><br/>
        <span id="z<?php echo $parkinglot->pid ?>"></span><br/>
        <span id="temperature<?php echo $parkinglot->pid ?>"></span><br/>
        <span id="supplyVoltage<?php echo $parkinglot->pid ?>"></span><br/>

        	<button id="openButton<?php echo $parkinglot->pid ?>" class="button"
                onClick="sendcmdOpen(<?php echo $parkinglot->pid . "," . $parkinglot->gateway_id ?>)">open
        	</button> <br/>
        
        	<button id="closeButton<?php echo $parkinglot->pid ?>" class="button"
                onClick="sendcmdClose(<?php echo $parkinglot->pid . "," . $parkinglot->gateway_id ?>)">close
        	</button><br/>

                <?php if ($nameRole == "Admin"): ?>
                    <button id="refreshButton<?php echo $parkinglot->pid ?>" class="button"
                            onClick="sendcmdRefresh(<?php echo $parkinglot->pid . "," . $parkinglot->gateway_id ?>)">refresh
                    </button><br/>
                    <button id="recalibrateButton<?php echo $parkinglot->pid ?>" class="button"
                            onClick="sendcmdRecalibrate(<?php echo $parkinglot->pid . "," . $parkinglot->gateway_id ?>)">recalibrate
                    </button><br/>
                    <button id="rebootButton<?php echo $parkinglot->pid ?>" class="button"
                            onClick="sendcmdReboot(<?php echo $parkinglot->pid . "," . $parkinglot->gateway_id ?>)">reboot
                    </button><br/>
                <?php endif; ?>


        </div>
        <!--
        <table width="100px">
        	<td>
	            <tr>Parkinglot_id: <?= $this->Number->format($parkinglot->pid) ?></tr>
	            <tr>Description: <?= h($parkinglot->description) ?></tr>
	            <tr>Mqtt-Channel: <?= h($parkinglot->mqttchannel) ?></tr>
	            <tr>Gateway_id: <?= $this->Number->format($parkinglot->gateway_id) ?></tr>
            </td>
        </table>
        -->
    <?php endforeach; ?>
        <?php endforeach; ?>
    <?php endforeach; ?>
</div>

<?php if ($nameRole == "Admin"): ?>
    <div style="clear:both;position:relative;">MQTT message log:
        <ul id='ws' style="font-family: 'Courier New', Courier, monospace;"></ul>
    </div>
<?php endif; ?>
    <script type="text/javascript">
    function dbStateToMqttState(dbstate) {
        switch (dbstate) {
            case "occupied":
                return "P";
                break;
            case "blocked":
                return "B";
                break;
            case "free":
                return "F";
                break;
            case "freeing":
                return "D";
                break;
            case "blocking":
                return "U";
                break;
            case "error":
                return "E";
                break;
            default:
                return "U";
        }
    }
    $(document).ready(function () {
        MQTTconnect();
        // wait for connection to become stable
        // only possible through the use of callback function onConnect

    });

    function initializeBackgrounds() {
        var parkinglotsObject = <?php echo json_encode($parkinglots); ?>;
        console.log(parkinglotsObject);
        console.log(mqtt);
        if (mqtt.isConnected) {
            $.each(parkinglotsObject, function (key, parkinglot) {
                try {
                    // mqtt.send(msg);
                    //mqtt.send("parking/98/state/F", "0", qos=0);
                    mqtt.send("parking/" + parkinglot.pid + "/state/" + dbStateToMqttState(parkinglot.status), "0", qos = 0);
                } catch (err) {
                    console.log(err.message);
                }
            });
        }
    }
</script>

<?php $this->Html->addCrumb('Dashboard', ['controller' => 'Dashboard', 'action' => 'index']); ?>