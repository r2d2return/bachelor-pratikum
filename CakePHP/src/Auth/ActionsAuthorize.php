<?php
namespace App\Auth;

use Cake\Auth\BaseAuthorize;
use Cake\Network\Request;

class ActionsAuthorize extends BaseAuthorize
{
    public function authorize($user, Request $request)
    {
        debug("LALLALALLALALA");
        // Do things for custom authoriziation here.
    	$requestedController = $this->request->params['controller'];
    	$requestedAction = $this->request->params['action'];
    	$currentAuthUserRoles = $this->Users->getRoles($this->Auth->user('id'));
    	debug($currentAuthUserRoles);
    	
    	// Lookup roles required for requested action
    	// TODO
    	$conn = ConnectionManager::get('default');
    	$stmt = "SELECT roles.name FROM auth_acl LEFT JOIN roles ON roles.id = auth_acl.role_id
				WHERE auth_acl.controller='".$requestedController."'
					AND (auth_acl.actions LIKE '%".$requestedAction."%' OR auth_acl.actions LIKE '*')";
    	$allowedRoles = array_column($conn->query($stmt)->fetchAll('assoc'),'name');
    	debug($allowedRoles);
    	
    	// Check if user is allowed to access this Controller and this action
    	$rolesSufficientForAccess = array_intersect($currentAuthUserRoles, $allowedRoles);
    	debug($rolesSufficientForAccess);
    	
    	if (count($rolesSufficientForAccess) > 0) {
    		debug("Access Granted");
    	}
    }
}
?>