package com.tud.kom.parkinglot.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.RemoteInput;

import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.model.Reservation;
import com.tud.kom.parkinglot.notification.auto.MessageReplyIntentService;
import com.tud.kom.parkinglot.notification.openParkingLot.OpenParkingLotBroadcast;

import static android.support.v4.app.NotificationCompat.Builder;
import static android.support.v4.app.NotificationCompat.CarExtender;
import static android.support.v4.app.NotificationCompat.DEFAULT_ALL;
import static android.support.v4.app.NotificationCompat.PRIORITY_HIGH;
import static com.tud.kom.parkinglot.notification.openParkingLot.OpenParkingLotService.RESERVATION_EXTRA;

public abstract class ParkingLotNotification {
    public static final String EXTRA_CONVERSATION_ID = "conversation_id";
    public static final String EXTRA_VOICE_REPLY = "extra_voice_reply";
    private static final String TAG = ParkingLotNotification.class.getSimpleName();

    /**
     * Create and show a simple notification
     */
    public static void sendNotification(Service c, Reservation reservation) {
        final String CHANNEL_ID = "smartparking_opener";
        final int CONVERSATION_ID = 0;
        final String CONVERSATION_OTHERS_NAME = "Smart Parker";

        // android o
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, c.getString(R.string.channel_name), NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription(c.getString(R.string.channel_desc));
            NotificationManager notificationManager = c.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        // android auto
        RemoteInput remoteInput = new RemoteInput.Builder(EXTRA_VOICE_REPLY).setLabel(c.getString(R.string.reply_by_voice)).build();

        Intent replayIntent = new Intent(c, MessageReplyIntentService.class).putExtra(EXTRA_CONVERSATION_ID, CONVERSATION_ID).putExtra(RESERVATION_EXTRA, reservation);
        PendingIntent replyPendingIntent = PendingIntent.getService(c, CONVERSATION_ID, replayIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        CarExtender.UnreadConversation.Builder unreadConvBuilder = new CarExtender.UnreadConversation.Builder(CONVERSATION_OTHERS_NAME).setReplyAction(replyPendingIntent, remoteInput).addMessage(c.getString(R.string.auto_voice_text)).setLatestTimestamp(System.currentTimeMillis());

        // intent for opening parkinglot
        Intent intent = new Intent(c, OpenParkingLotBroadcast.class);
        intent.putExtra(RESERVATION_EXTRA, reservation);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(c, CONVERSATION_ID, intent, PendingIntent.FLAG_ONE_SHOT);

        // actual notification
        Builder notificationBuilder = new Builder(c, CHANNEL_ID).setDefaults(DEFAULT_ALL).setPriority(PRIORITY_HIGH).setSmallIcon(R.drawable.ic_launcher).setContentTitle(c.getString(R.string.notification_title)).setContentText(c.getString(R.string.notification_text))
                //setwhen
                .setAutoCancel(true).setContentIntent(pendingIntent).extend(new CarExtender().setUnreadConversation(unreadConvBuilder.build()).setColor(c.getResources().getColor(R.color.colorAccent)));

        // notification senden
        NotificationManager notificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(CONVERSATION_ID, notificationBuilder.build());
    }
}