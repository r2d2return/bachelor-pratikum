<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

/**
 * Users Model
 *
 * @property \App\Model\Table\DatabaseLogsTable|\Cake\ORM\Association\HasMany $DatabaseLogs
 * @property \App\Model\Table\TenantsTable|\Cake\ORM\Association\BelongsToMany $Tenants
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\BelongsToMany $Roles
 * @property \App\Model\Table\ZonesTable|\Cake\ORM\Association\BelongsToMany $Zones
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('DatabaseLogs', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsToMany('Tenants', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'tenant_id',
            'joinTable' => 'tenants_users'
        ]);
        $this->belongsToMany('Roles', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'role_id',
            'joinTable' => 'users_roles'
        ]);
        $this->belongsToMany('Zones', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'zone_id',
            'joinTable' => 'users_zones'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->allowEmpty('mobileNumber');

        $validator
            ->allowEmpty('landlineNumber');

        $validator
            ->allowEmpty('firstname');

        $validator
            ->allowEmpty('lastname');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('gender');

        $validator
            ->requirePresence('userState', 'create')
            ->notEmpty('userState');

        $validator
            ->allowEmpty('company');

        $validator
            ->allowEmpty('department');

        $validator
            ->allowEmpty('street');

        $validator
            ->allowEmpty('ZIP');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
    public function getRoles($userid) {
        $conn = ConnectionManager::get('default');
        
        if (empty($userid)) return false;
        $stmt = "SELECT roles.name
                FROM roles
                LEFT JOIN users_roles ON users_roles.role_id = roles.id
                WHERE users_roles.user_id=".$userid."
                ";
        
        $userroles= array_column($conn->query($stmt)->fetchAll('assoc'),'name');
        return $userroles;
    }

    public function getUserState($userid) {
        $conn = ConnectionManager::get('default');

        if (empty($userid)) return false;
        $stmt = "SELECT users.userState
                FROM users
                WHERE users.id=".$userid."
                ";

        $userState = $conn->query($stmt)->fetch('assoc');
        return $userState;
    }

    public function tenantName($userId) {
        $conn = ConnectionManager::get('default');
        if (empty($userId)) return false;
        $stmt = "SELECT tenants.name
                FROM tenants
                LEFT JOIN tenants_users ON tenants_users.tenant_id = tenants.id
                WHERE tenants_users.user_id=".$userId."
                ";
        
        $tenants_names= array_column($conn->query($stmt)->fetchAll('assoc'),'name');
        return $tenants_names;
    }

    public function tenantId($userId) {
        $conn = ConnectionManager::get('default');
        if (empty($userId)) return false;
        $stmt = "SELECT tenants.id
                FROM tenants
                LEFT JOIN tenants_users ON tenants_users.tenant_id = tenants.id
                WHERE tenants_users.user_id=".$userId."
                ";
        
        $tenants_id = array_column($conn->query($stmt)->fetchAll('assoc'),'id');
        $tenants_id = implode("", $tenants_id);
        return $tenants_id;
    }

    public function zoneId($userId) {
        $conn = ConnectionManager::get('default');
        if (empty($userId)) return false;
        $stmt = "SELECT zones.id
                FROM zones
                LEFT JOIN users_zones ON users_zones.zone_id = zones.id
                WHERE users_zones.user_id=".$userId."
                ";
        
        $zones_id= array_column($conn->query($stmt)->fetchAll('assoc'),'id');
        $zones_id = implode("", $zones_id);
        return $zones_id;
    }
    
    public function reservationVisitor($userId) {
        $query = $this->find('list', [
            'limit' => 200,
            'keyField' => 'id',
            'valueField' => 'username'
            ])->where(function ($exp) {
                    return $exp
                        ->eq('userState', 'ACTIVE');
                });
        $visitedId = $query->matching('Roles', function ($q) {
            return $q->where(function ($exp) {
                    $orConditions = $exp->or_([])
                        ->eq('Roles.name', 'Reservation Manager')
                        ->eq('Roles.name', 'Host')
                        ->eq('Roles.name', 'Visitor');
                    return $exp
                        ->add($orConditions);
                });
        });
        $visitedZone = $visitedId->matching('Zones', function ($q) use ($userId){
                    return $q->where(['Zones.id' => $this->zoneId($userId)]);
                    });
        return $visitedZone;
    }
    public function adminVisitor() {
        $visitor = $this->find('list', [
                    'limit' => 200,
                    'keyField' => 'id',
                    'valueField' => 'username'
                    ])->where(function ($exp) {
                            return $exp
                                ->eq('userState', 'ACTIVE');
                    });
        return $visitor;
    }
}

