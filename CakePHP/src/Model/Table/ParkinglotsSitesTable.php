<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ParkinglotsSites Model
 *
 * @method \App\Model\Entity\ParkinglotsSite get($primaryKey, $options = [])
 * @method \App\Model\Entity\ParkinglotsSite newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ParkinglotsSite[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ParkinglotsSite|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ParkinglotsSite patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ParkinglotsSite[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ParkinglotsSite findOrCreate($search, callable $callback = null, $options = [])
 */
class ParkinglotsSitesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('parkinglots_sites');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('sitesId')
            ->requirePresence('sitesId', 'create')
            ->notEmpty('sitesId');

        $validator
            ->integer('parkingLotsId')
            ->requirePresence('parkingLotsId', 'create')
            ->notEmpty('parkingLotsId');

        return $validator;
    }
}
