<?php
return [
    

    /**
     * Security and encryption configuration
     *
     * - salt - A random string used in security hashing methods.
     *   The salt value is also used as the encryption key.
     *   You should treat it as extremely sensitive data.
     */
    'Security' => [
        'firebase_auth_key' => 'key=AAAA8bqPe4Y:APA91bG8f9R79rwXFcR0hRtRFjIqaizqRoVAxJPt532dY6QyliHCKHvPl6Ttm25u251Ck2gQwRgNhzVZufSVSa3DTS06kSaVd9rP9ec3DjDB40yMjU9O9M-7trLObhKP3UHw2oEJ07Tt',

        'BFProtectionUnit'=>[
            'activated' => true,
            'mins_to_block'=> 1,
            'allowed_attempts'=>3,
            'error_text' => 'Your account is temporarily blocked due to too many login attempts. Please try again in about 1 minute'
        ]

    ]

    
];
