package com.tud.kom.parkinglot.model;

import org.junit.Assert;
import org.junit.Test;

public class SiteTest {
    @Test
    public void testDefaultIsNull() {
        Site s = new Site();
        Assert.assertNull(s.getZones());
        Assert.assertNull(s.getDescription());
        Assert.assertEquals(0, s.getId());
    }

    @Test
    public void testToString() {
        Assert.assertNotNull(new Site().toString());
    }
}
