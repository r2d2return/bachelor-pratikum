package com.tud.kom.parkinglot.notification.auto;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.tud.kom.parkinglot.notification.ParkingLotNotification;
import com.tud.kom.parkinglot.notification.openParkingLot.OpenParkingLotService;

import static com.tud.kom.parkinglot.notification.openParkingLot.OpenParkingLotService.RESERVATION_EXTRA;

public class MessageReplyIntentService extends IntentService {
    private static final String TAG = MessageReplyIntentService.class.getSimpleName();

    public MessageReplyIntentService() {
        super("");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        int conversationId = intent.getIntExtra(ParkingLotNotification.EXTRA_CONVERSATION_ID, -1);
        if (conversationId != -1) {
            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
            Intent intent1 = new Intent(getApplicationContext(), OpenParkingLotService.class);
            intent1.putExtra(RESERVATION_EXTRA, intent.getSerializableExtra(RESERVATION_EXTRA));
            startService(intent1);
        }
    }
}
