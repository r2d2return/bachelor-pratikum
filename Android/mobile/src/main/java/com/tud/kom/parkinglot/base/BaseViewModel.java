package com.tud.kom.parkinglot.base;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;

import com.tud.kom.parkinglot.network.NetworkCallback;
import com.tud.kom.parkinglot.network.Response;

import org.json.JSONException;

import java.text.ParseException;
import java.util.List;

public abstract class BaseViewModel<T> extends ViewModel implements NetworkCallback {

    private static final String TAG = BaseViewModel.class.getSimpleName();

    protected MutableLiveData<String> mError;

    protected MutableLiveData<Boolean> mLoading;

    protected String mPassword;

    protected String mUsername;

    private MutableLiveData<List<T>> mData;

    private MutableLiveData<Boolean> mLogout;

    public BaseViewModel(final String username, final String password) {
        this.mPassword = password;
        this.mUsername = username;
    }

    public LiveData<List<T>> getData() {
        if (mData == null) {
            mData = new MutableLiveData<>();
        }
        return mData;
    }

    public LiveData<String> getError() {
        if (mError == null) {
            mError = new MutableLiveData<>();
        }
        return mError;
    }

    public LiveData<Boolean> getLogout() {
        if (mLogout == null) {
            mLogout = new MutableLiveData<>();
            mLogout.setValue(false);
        }
        return mLogout;
    }

    @Override
    public void handleWebsiteResponse(final Response response) {
        mLoading.setValue(false);
        try {
            if (response.getRespCode() != 200) {
                mError.setValue("ErrorRespCodeNotAsExpected: " + response.getRespCode());
                return;
            }
            if (response.getResp() == null) {
                mError.setValue("ErrorRespBodyIsNull");
                return;
            }

            List<T> parsedResponse = parseNetworkResponse(response);
            mData.setValue(parsedResponse);
        } catch (JSONException | ParseException e) {
            Log.e(TAG, "ErrorJsonCouldntBeParsed: ", e);
            Log.e(TAG, "Response was: " + response.getResp());
            mError.setValue(e.getMessage());
        }
    }

    public LiveData<Boolean> isLoading() {
        if (mLoading == null) {
            mLoading = new MutableLiveData<>();
        }
        return mLoading;
    }

    public void loadData() {
        mLoading.setValue(true);
        makeNetworkCall(mUsername, mPassword, this);
    }

    protected abstract void makeNetworkCall(String username, String password, final NetworkCallback callback);

    @NonNull
    protected abstract List<T> parseNetworkResponse(final Response response) throws JSONException, ParseException;
}
