package com.tud.kom.parkinglot.activities.reservation;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.model.ParkingLot;
import com.tud.kom.parkinglot.model.Reservation;
import com.tud.kom.parkinglot.model.Site;
import com.tud.kom.parkinglot.model.Zone;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReservationActivity extends AppCompatActivity {
    public static final String EXTRA_PARKINGLOT_ID = "parkinglot_id";
    public static final String EXTRA_SITE_ID = "site_id";
    public static final String EXTRA_ZONE_ID = "zone_id";
    private static final String TAG = ReservationActivity.class.getSimpleName();
    private Calendar mEndCalendar;
    private ParkingLot mParkingLot;
    private int mParkingLotId;
    private boolean mPreselect;
    private Site mSite;
    private int mSiteId;
    private Spinner mSpinnerParkingLot;
    private Spinner mSpinnerSite;
    private Spinner mSpinnerZone;
    private Calendar mStartCalendar;
    private TextView mTextViewEndDate;
    private TextView mTextViewEndTime;
    private TextView mTextViewStartDate;
    private TextView mTextViewStartTime;
    private ReservationViewModel mViewModel;
    private Zone mZone;
    private int mZoneId;
    private Button mReserveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_add_reservation);

        Bundle extras = this.getIntent().getExtras();
        if (extras != null) {
            this.mZoneId = extras.getInt(EXTRA_ZONE_ID);
            this.mSiteId = extras.getInt(EXTRA_SITE_ID);
            this.mParkingLotId = extras.getInt(EXTRA_PARKINGLOT_ID);
            this.mPreselect = true;
        }

        this.mViewModel = ViewModelProviders.of(this, new ReservationViewModelFactory(this)).get(ReservationViewModel.class);

        this.mViewModel.getZones().observe(this, this::onZonesChanged);

        this.mViewModel.getSites().observe(this, this::onSitesChanged);

        this.mViewModel.getParkingLots().observe(this, this::onParkingLotsChanged);

        this.mViewModel.getStartCalendar().observe(this, this::onStartCalendarChanged);

        this.mViewModel.getEndCalendar().observe(this, this::onEndCalendarChanged);

        this.mViewModel.getError().observe(this, error -> {
            if (ReservationViewModel.RESERVATION_SAVED.equals(error)) {
                Toast.makeText(this, R.string.reservation_saved, Toast.LENGTH_LONG).show();
                this.finish();
            }
            else {
                Toast.makeText(this, R.string.reservation_not_saved, Toast.LENGTH_LONG).show();
            }
        });

        this.mSpinnerZone = this.findViewById(R.id.spinner_zone);
        this.mSpinnerZone.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                ReservationActivity.this.mZone = ((Zone) parent.getItemAtPosition(position));
                ReservationActivity.this.mViewModel.loadSites(ReservationActivity.this.mZone.getId());
                ReservationActivity.this.clearAdapter(ReservationActivity.this.mSpinnerSite);
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {

            }
        });
        this.mSpinnerZone.setAdapter(new ZoneAdapter(this, new ArrayList<>()));

        this.mSpinnerSite = this.findViewById(R.id.spinner_site);
        this.mSpinnerSite.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                ReservationActivity.this.mSite = ((Site) parent.getItemAtPosition(position));
                ReservationActivity.this.mViewModel.loadParkingLots(ReservationActivity.this.mSite.getId());
                ReservationActivity.this.clearAdapter(ReservationActivity.this.mSpinnerParkingLot);
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {

            }
        });
        this.mSpinnerSite.setAdapter(new SiteAdapter(this, new ArrayList<>()));

        this.mSpinnerParkingLot = this.findViewById(R.id.spinner_parking_lot);
        this.mSpinnerParkingLot.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                ReservationActivity.this.mParkingLot = ((ParkingLot) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {

            }
        });
        this.mSpinnerParkingLot.setAdapter(new ParkingLotAdapter(this, new ArrayList<>()));

        this.mTextViewStartDate = this.findViewById(R.id.text_view_start_date);
        this.mTextViewStartDate.setOnClickListener(view -> this.onStartDateClicked());

        this.mTextViewStartTime = this.findViewById(R.id.text_view_start_time);
        this.mTextViewStartTime.setOnClickListener(view -> this.onStartTimeClicked());

        this.mTextViewEndDate = this.findViewById(R.id.text_view_end_date);
        this.mTextViewEndDate.setOnClickListener(view -> this.onEndDateClicked());

        this.mTextViewEndTime = this.findViewById(R.id.text_view_end_time);
        this.mTextViewEndTime.setOnClickListener(view -> this.onEndTimeClicked());

        mReserveButton = this.findViewById(R.id.button_reserve);
        mReserveButton.setOnClickListener(v -> this.onReserveClicked());

        this.mViewModel.loadZones();
    }

    private void clearAdapter(Spinner spinner) {
        ArrayAdapter adapter = ((ArrayAdapter) spinner.getAdapter());
        adapter.clear();
        adapter.notifyDataSetChanged();
    }

    private void onEndCalendarChanged(final Calendar calendar) {
        this.mEndCalendar = calendar;

        this.mTextViewEndDate.setText(DateFormat.getDateFormat(this.getApplicationContext()).format(calendar.getTime()));
        this.mTextViewEndTime.setText(DateFormat.getTimeFormat(this.getApplicationContext()).format(calendar.getTime()));

        if (this.isEndTimeInvalid(calendar)) {
            this.mTextViewEndTime.setTextColor(this.getResources().getColor(R.color.reservation_time_invalid));
            mReserveButton.setEnabled(false);
        }
        else {
            this.mTextViewEndTime.setTextColor(this.getResources().getColor(R.color.reservation_time_valid));
            if (!isStartTimeInvalid(mStartCalendar)) {
                mReserveButton.setEnabled(true);
            }
        }
    }

    private void onEndDateClicked() {
        DatePickerDialog dialog = new DatePickerDialog(this, ((view, year, month, dayOfMonth) -> this.mViewModel.setEndCalendar(year, month, dayOfMonth)), this.mEndCalendar.get(Calendar.YEAR), this.mEndCalendar.get(Calendar.MONTH), this.mEndCalendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(this.mViewModel.getStartCalendar().getValue().getTimeInMillis());
        dialog.show();
    }

    private void onEndTimeClicked() {
        Dialog dialog = new TimePickerDialog(this, (view, hourOfDay, minute) -> this.mViewModel.setEndCalendar(hourOfDay, minute), this.mEndCalendar.get(Calendar.HOUR_OF_DAY), this.mEndCalendar.get(Calendar.MINUTE), true);
        dialog.show();
    }

    private void onParkingLotsChanged(final List<ParkingLot> parkingLots) {
        this.swapAdapterItems(parkingLots, this.mSpinnerParkingLot);

        if (this.mPreselect) {
            ParkingLotAdapter adapter = ((ParkingLotAdapter) this.mSpinnerParkingLot.getAdapter());
            int index = adapter.getIndexOfItemWithId(this.mParkingLotId);
            this.mSpinnerParkingLot.setSelection(index);
            this.mPreselect = false;
        }
    }

    private void onReserveClicked() {

        Reservation reservation = new Reservation();
        reservation.setStartTime(new Timestamp(this.mStartCalendar.getTimeInMillis()));
        reservation.setEndTime(new Timestamp(this.mEndCalendar.getTimeInMillis()));
        if (mParkingLot != null) {
            ParkingLot parkingLot = new ParkingLot();
            parkingLot.setId(this.mParkingLot.getId());
            reservation.setParkingLot(parkingLot);
            this.mViewModel.sendReservation(reservation);
        }
        else {
            Toast.makeText(this, R.string.no_parking_lot_selected, Toast.LENGTH_SHORT).show();
        }
    }

    private void onSitesChanged(final List<Site> sites) {
        this.swapAdapterItems(sites, this.mSpinnerSite);

        if (this.mPreselect) {
            SiteAdapter adapter = ((SiteAdapter) this.mSpinnerSite.getAdapter());
            int index = adapter.getIndexOfItemWithId(this.mSiteId);
            this.mSpinnerSite.setSelection(index);
        }
    }

    private void onStartCalendarChanged(final Calendar calendar) {
        this.mStartCalendar = calendar;

        this.mTextViewStartDate.setText(DateFormat.getDateFormat(this.getApplicationContext()).format(calendar.getTime()));
        this.mTextViewStartTime.setText(DateFormat.getTimeFormat(this.getApplicationContext()).format(calendar.getTime()));

        if (this.isStartTimeInvalid(calendar)) {
            this.mTextViewStartTime.setTextColor(this.getResources().getColor(R.color.reservation_time_invalid));
            mReserveButton.setEnabled(false);
        }
        else {
            this.mTextViewStartTime.setTextColor(this.getResources().getColor(R.color.reservation_time_valid));
            mReserveButton.setEnabled(true);
        }
    }

    private void onStartDateClicked() {
        DatePickerDialog dialog = new DatePickerDialog(this, ((view, year, month, dayOfMonth) -> this.mViewModel.setStartCalendar(year, month, dayOfMonth)), this.mStartCalendar.get(Calendar.YEAR), this.mStartCalendar.get(Calendar.MONTH), this.mStartCalendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(System.currentTimeMillis());
        dialog.show();
    }

    private void onStartTimeClicked() {
        Dialog dialog = new TimePickerDialog(this, (view, hourOfDay, minute) -> this.mViewModel.setStartCalendar(hourOfDay, minute), this.mStartCalendar.get(Calendar.HOUR_OF_DAY), this.mStartCalendar.get(Calendar.MINUTE), true);
        dialog.show();
    }

    private void onZonesChanged(final List<Zone> zones) {
        this.swapAdapterItems(zones, this.mSpinnerZone);

        if (this.mPreselect) {
            ZoneAdapter adapter = ((ZoneAdapter) this.mSpinnerZone.getAdapter());
            int index = adapter.getIndexOfItemWithId(this.mZoneId);
            this.mSpinnerZone.setSelection(index);
        }
    }

    private <T> void swapAdapterItems(List<T> items, Spinner spinner) {
        ArrayAdapter adapter = ((ArrayAdapter) spinner.getAdapter());
        adapter.clear();
        adapter.addAll(items);
        adapter.notifyDataSetChanged();
    }

    /**
     * Calculate whether the end time of a reservation is invalid.
     *
     * @param calendar the calender containing the EndTime
     * @return true if calender contains an invalid end time for a reservation, false else.
     */
    private boolean isEndTimeInvalid(final Calendar calendar) {
        return calendar.getTimeInMillis() <= this.mViewModel.getStartCalendar().getValue().getTimeInMillis() || this.isStartTimeInvalid(calendar);
    }

    /**
     * Calculate whether the start time of a reservation is invalid.
     * ((Math.floor((double) System.currentTimeMillis() / 60000)) * 60000) rounds down to minutes.
     *
     * @param calendar the calender containing the StartTime
     * @return true if calender contains an invalid start time for a reservation, false else.
     */
    private boolean isStartTimeInvalid(final Calendar calendar) {
        return calendar.getTimeInMillis() < ((Math.floor((double) System.currentTimeMillis() / 60000)) * 60000);
    }
}