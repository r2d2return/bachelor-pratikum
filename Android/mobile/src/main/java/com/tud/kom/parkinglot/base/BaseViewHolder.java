package com.tud.kom.parkinglot.base;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;

public abstract class BaseViewHolder<T> extends ViewHolder {

    public BaseViewHolder(final View itemView) {
        super(itemView);
    }

    public abstract void bind(T t);
}
