package com.tud.kom.parkinglot.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.nio.charset.StandardCharsets.UTF_8;

public class JsonUtils {

    public static String getJsonParkingLots(Context context, String... names) {
        String jsonString = createParkingLots(context, names.length);

        for (int i = 0; i < names.length; i++) {
            final String name = names[i];
            jsonString = jsonString.replace("testName" + i, name);
        }

        return jsonString;
    }

    /**
     * Creates a json string that matches the response of a request to .../reservation.json. The json
     * string contains only a single reservation.
     *
     * @param startDate the start date of the reservation
     * @param endDate   the end date of the reservation
     * @return a json string which matches a response of a request to /reservations.json
     */
    public static String getJsonReservation(Context context, Date startDate, Date endDate) {
        String jsonString = createReservationsJson(context, 1);
        String formattedStartDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(startDate);
        String formattedEndDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(endDate);
        jsonString = jsonString.replace("startDate0", formattedStartDate);
        jsonString = jsonString.replace("endDate0", formattedEndDate);

        return jsonString;
    }

    /**
     * Creates a json string that matches the response of a request to .../reservation.json.
     *
     * @param numberOfReservations the number of reservations the json string should include
     * @param startDates           the start dates of the reservations. <code>startDate.length</code> must match
     *                             <code>numberOfReservations</code>
     * @param endDates             the end dates of the reservations. <code>endDate.length</code> must match
     *                             <code>numberOfReservations</code>
     * @return a json string which matches a response of a request to /reservations.json
     */
    public static String getJsonReservations(Context context, int numberOfReservations, Date[] startDates, Date[] endDates) {
        String jsonString = createReservationsJson(context, numberOfReservations);

        for (int i = 0; i < numberOfReservations; i++) {
            String startDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(startDates[i]);
            String endDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(startDates[i]);
            jsonString = jsonString.replace("startDate" + i, startDate);
            jsonString = jsonString.replace("endDate" + i, endDate);
        }
        return jsonString;
    }

    /**
     * Creates a json string that matches the response of a request to .../reservation.json.
     *
     * @param numberOfReservations the number of reservations the json string should include
     * @param startEndDates        the start and end dates of the reservations in an alternating pattern. Example:
     *                             <code>getJsonReservations(2, startDate1, endDate1, startDate2, endDate2);</code>
     * @return a json string which matches a response of a request to /reservations.json
     */
    public static String getJsonReservations(Context context, int numberOfReservations, Date... startEndDates) {
        String jsonString = createReservationsJson(context, numberOfReservations);

        for (int i = 0, z = 0; i < numberOfReservations; i++, z += 2) {
            String startDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(startEndDates[z]);
            String endDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(startEndDates[z + 1]);
            jsonString = jsonString.replace("startDate" + i, startDate);
            jsonString = jsonString.replace("endDate" + i, endDate);
        }

        return jsonString;
    }

    public static String getJsonSites(Context context, String... names) {
        String jsonString = createSites(context, names.length);

        for (int i = 0; i < names.length; i++) {
            final String name = names[i];
            jsonString = jsonString.replace("testName" + i, name);
        }

        return jsonString;
    }

    public static String getJsonZones(Context context, String... names) {
        String jsonString = createZones(context, names.length);

        for (int i = 0; i < names.length; i++) {
            final String name = names[i];
            jsonString = jsonString.replace("testName" + i, name);
        }

        return jsonString;
    }

    private static String createParkingLots(final Context context, final int length) {
        try {
            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < length; i++) {
                String parkingLot = getJsonFromFile("parkinglot.json", context);
                parkingLot = parkingLot.replace("\"testId\"", String.valueOf(i)).replace("testName", "testName" + i);

                JSONObject jsonParkingLot = new JSONObject(parkingLot);
                jsonArray.put(i, jsonParkingLot);
            }
            return jsonArray.toString();
        } catch (JSONException e) {
            Log.e("JsonUtils", "createReservationsJson: ", e);
            return null;
        }
    }

    /**
     * Creates a json object which contains a json array with the tag "reservations".
     *
     * @param numberOfReservations how many reservations the json array should include
     * @return a json object containing a json array of reservations
     */
    private static String createReservationsJson(Context context, int numberOfReservations) {
        try {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < numberOfReservations; i++) {
                String reservation = getJsonFromFile("reservation.json", context);
                reservation = reservation.replace("startDate", "startDate" + i);
                reservation = reservation.replace("endDate", "endDate" + i);

                JSONObject jsonReservation = new JSONObject(reservation);
                jsonArray.put(i, jsonReservation);
            }
            return jsonArray.toString();
        } catch (Exception e) {
            Log.e("JsonUtils", "createReservationsJson: ", e);
            return null;
        }
    }

    private static String createSites(final Context context, final int length) {
        try {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < length; i++) {
                String site = getJsonFromFile("site.json", context);
                site = site.replace("\"testId\"", String.valueOf(i)).replace("testName", "testName" + i);

                JSONObject jsonSite = new JSONObject(site);
                jsonArray.put(i, jsonSite);
            }
            return jsonArray.toString();
        } catch (JSONException e) {
            Log.e("JsonUtils", "createReservationsJson: ", e);
            return null;
        }
    }

    private static String createZones(final Context context, final int length) {
        try {
            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < length; i++) {
                String zone = getJsonFromFile("zone.json", context);
                zone = zone.replace("\"testId\"", String.valueOf(i)).replace("testName", "testName" + i);

                JSONObject jsonZone = new JSONObject(zone);
                jsonArray.put(i, jsonZone);
            }
            return jsonArray.toString();
        } catch (JSONException e) {
            Log.e("JsonUtils", "createReservationsJson: ", e);
            return null;
        }
    }

    /**
     * Reads a json string from a .json file in the assets folder. Code from https://github.com/andrzejchm/RESTMock/blob/master/android/src/main/java/io/appflate/restmock/android/AndroidAssetsFileParser.java.
     *
     * @param path    the name of the .json file
     * @param context the test context
     * @return a json string
     */
    @NonNull
    private static String getJsonFromFile(String path, Context context) {
        BufferedReader br = null;

        try {
            br = new BufferedReader(new InputStreamReader(context.getAssets().open(path), UTF_8));
            String line;
            StringBuilder text = new StringBuilder();

            while ((line = br.readLine()) != null) {
                text.append(line);
            }
            br.close();
            return text.toString();
        } catch (IOException e) {
            Log.e("JsonUtils", "getJsonFromFile: ", e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Log.e("JsonUtils", "getJsonFromFile: ", e);
                }
            }
        }

        return null;
    }
}
