package com.tud.kom.parkinglot.activities.history;

import android.support.annotation.NonNull;

import com.tud.kom.parkinglot.base.BaseViewModel;
import com.tud.kom.parkinglot.model.Reservation;
import com.tud.kom.parkinglot.model.parser.ReservationParser;
import com.tud.kom.parkinglot.network.NetworkCallback;
import com.tud.kom.parkinglot.network.RequestAbstraction;
import com.tud.kom.parkinglot.network.Response;

import org.json.JSONException;

import java.text.ParseException;
import java.util.List;

public class HistoryViewModel extends BaseViewModel<Reservation> {

    private static final String TAG = HistoryViewModel.class.getSimpleName();

    public HistoryViewModel(final String password, final String username) {
        super(password, username);
    }

    @Override
    protected void makeNetworkCall(final String username, final String password, final NetworkCallback callback) {
        RequestAbstraction.getHistory(callback, username, password);
    }

    @NonNull
    @Override
    protected List<Reservation> parseNetworkResponse(final Response response) throws JSONException, ParseException {
        return ReservationParser.parseReservationsArray(response.getResp());
    }
}
