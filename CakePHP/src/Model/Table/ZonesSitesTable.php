<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ZonesSites Model
 *
 * @method \App\Model\Entity\ZonesSite get($primaryKey, $options = [])
 * @method \App\Model\Entity\ZonesSite newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ZonesSite[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ZonesSite|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ZonesSite patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ZonesSite[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ZonesSite findOrCreate($search, callable $callback = null, $options = [])
 */
class ZonesSitesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('zones_sites');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('zonesId')
            ->requirePresence('zonesId', 'create')
            ->notEmpty('zonesId');

        $validator
            ->integer('sitesId')
            ->requirePresence('sitesId', 'create')
            ->notEmpty('sitesId');

        return $validator;
    }
}
