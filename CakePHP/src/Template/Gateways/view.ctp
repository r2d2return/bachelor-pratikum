<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Gateway'), ['action' => 'edit', $gateway->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Gateway'), ['action' => 'delete', $gateway->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gateway->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Gateways'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gateway'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sites'), ['controller' => 'Sites', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Site'), ['controller' => 'Sites', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Parkinglots'), ['controller' => 'Parkinglots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parkinglot'), ['controller' => 'Parkinglots', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="gateways view large-10 medium-9 columns">
    <h2><?= h($gateway->label) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Label') ?></h6>
            <p><?= h($gateway->label) ?></p>
            <h6 class="subheader"><?= __('Fqdn') ?></h6>
            <p><?= h($gateway->fqdn) ?></p>
            <h6 class="subheader"><?= __('Site') ?></h6>
            <p><?= $gateway->has('site') ? $this->Html->link($gateway->site->name, ['controller' => 'Sites', 'action' => 'view', $gateway->site->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($gateway->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Parkinglots') ?></h4>
    <?php if (!empty($gateway->parkinglots)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Pid') ?></th>
            <th><?= __('Description') ?></th>
            <th><?= __('Mqttchannel') ?></th>
            <th><?= __('Site Id') ?></th>
            <th><?= __('Gateway Id') ?></th>
            <th><?= __('Status') ?></th>
            <th><?= __('Lat1') ?></th>
            <th><?= __('Lon1') ?></th>
            <th><?= __('Lat2') ?></th>
            <th><?= __('Lon2') ?></th>
            <th><?= __('Lat3') ?></th>
            <th><?= __('Lon3') ?></th>
            <th><?= __('Lat4') ?></th>
            <th><?= __('Lon4') ?></th>
            <th><?= __('Ip') ?></th>
            <th><?= __('Hwid') ?></th>
            <th><?= __('LastStateChange') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($gateway->parkinglots as $parkinglots): ?>
        <tr>
            <td><?= h($parkinglots->id) ?></td>
            <td><?= h($parkinglots->pid) ?></td>
            <td><?= h($parkinglots->description) ?></td>
            <td><?= h($parkinglots->mqttchannel) ?></td>
            <td><?= h($parkinglots->site_id) ?></td>
            <td><?= h($parkinglots->gateway_id) ?></td>
            <td><?= h($parkinglots->status) ?></td>
            <td><?= h($parkinglots->lat1) ?></td>
            <td><?= h($parkinglots->lon1) ?></td>
            <td><?= h($parkinglots->lat2) ?></td>
            <td><?= h($parkinglots->lon2) ?></td>
            <td><?= h($parkinglots->lat3) ?></td>
            <td><?= h($parkinglots->lon3) ?></td>
            <td><?= h($parkinglots->lat4) ?></td>
            <td><?= h($parkinglots->lon4) ?></td>
            <td><?= h($parkinglots->ip) ?></td>
            <td><?= h($parkinglots->hwid) ?></td>
            <td><?= h($parkinglots->lastStateChange) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Parkinglots', 'action' => 'view', $parkinglots->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Parkinglots', 'action' => 'edit', $parkinglots->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Parkinglots', 'action' => 'delete', $parkinglots->id], ['confirm' => __('Are you sure you want to delete # {0}?', $parkinglots->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<?php $this->Html->addCrumb('Gateways', '/gateways'); ?>
<?php $this->Html->addCrumb('Details', ['controller' => 'Gateways', 'action' => 'view']); ?>