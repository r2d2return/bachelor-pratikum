-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 16, 2018 at 09:11 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartparking`
--

-- --------------------------------------------------------

--
-- Table structure for table `apps`
--

CREATE TABLE `apps` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `secret` varchar(45) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `state` enum('ACTIVE','BLOCKED','DELETED') DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_acl`
--

CREATE TABLE `auth_acl` (
  `id` int(11) NOT NULL,
  `controller` varchar(200) CHARACTER SET utf8 NOT NULL,
  `actions` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '*',
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_acl`
--

INSERT INTO `auth_acl` (`id`, `controller`, `actions`, `role_id`) VALUES
(1, 'Reservations', '*', 1),
(2, 'Users', 'index', 2),
(3, 'Users', '*', 1),
(4, 'Dashboard', '*', 1),
(5, 'RemoteKeys', '*', 1),
(6, 'AuthAcl', '*', 1),
(7, 'Maps', '*', 1),
(8, 'Roles', '*', 1),
(9, 'Users', '*', 4),
(10, 'DatabaseLogs', '*', 1),
(13, 'Reservations', '*', 4),
(14, 'Dashboard', '*', 4),
(18, 'Reservations', 'index,view,add,add_api,history, sendFirebaseMessage', 3),
(19, 'Users, index', 'getUser', 3),
(20, 'Sites', 'index', 3),
(21, 'Sites', 'view', 3),
(22, 'Zones', 'index', 3),
(23, 'Zones', 'view', 3),
(24, 'Parkinglots', 'index', 3),
(25, 'Parkinglots', 'view', 3),
(26, 'Firebase', '*', 3);

-- --------------------------------------------------------

--
-- Table structure for table `calender`
--

CREATE TABLE `calender` (
  `recurringId` int(11) NOT NULL,
  `recurringTypeId` int(11) NOT NULL,
  `separationCount` int(11) DEFAULT NULL,
  `maxNumOfOccurences` int(11) DEFAULT NULL,
  `dayOfWeek` int(11) DEFAULT NULL,
  `WeekOfMonth` int(11) DEFAULT NULL,
  `DayOfMonth(11)` int(11) DEFAULT NULL,
  `MonthOfYear` int(11) DEFAULT NULL COMMENT 'Reservation Reccurances '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `calender`
--

INSERT INTO `calender` (`recurringId`, `recurringTypeId`, `separationCount`, `maxNumOfOccurences`, `dayOfWeek`, `WeekOfMonth`, `DayOfMonth(11)`, `MonthOfYear`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `calender_exceptions`
--

CREATE TABLE `calender_exceptions` (
  `id` int(11) NOT NULL,
  `res_id` int(11) DEFAULT NULL,
  `isScheduled` bit(1) DEFAULT NULL,
  `isCancelled` bit(1) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `isFullDayReservation` bit(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `database_logs`
--

CREATE TABLE `database_logs` (
  `id` int(11) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `message` text,
  `context` text,
  `created` timestamp NULL DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `hostname` varchar(50) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `refer` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `count` int(10) DEFAULT '0',
  `tenant` varchar(45) DEFAULT NULL,
  `action` enum('CREATE','UPDATE','DELETE','OTHER') DEFAULT 'OTHER',
  `userName` varchar(45) DEFAULT NULL,
  `userRole` varchar(45) DEFAULT NULL,
  `oldValue` varchar(45) DEFAULT NULL,
  `newValue` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `database_logs`
--

INSERT INTO `database_logs` (`id`, `type`, `message`, `context`, `created`, `ip`, `hostname`, `uri`, `refer`, `user_agent`, `count`, `tenant`, `action`, `userName`, `userRole`, `oldValue`, `newValue`, `user_id`) VALUES
(23, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => NewCustomer , Reservation Manager\n    [newValue] => BrandNewCustomer , Guest\n)', '2017-08-26 22:14:27', '::1', 'localhost', '/users/edit/5', 'https://localhost/users/edit/5', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'NewCustomer , Reservation Manager', 'BrandNewCustomer , Guest', 3),
(24, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => nhkdi , Visitor\n)', '2017-08-26 22:15:50', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'nhkdi , Visitor', 3),
(25, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nhkdi , Visitor\n    [newValue] => NewFacility , Facility Manager\n)', '2017-08-26 22:16:49', '::1', 'localhost', '/users/edit/33', 'https://localhost/users/edit/33', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'nhkdi , Visitor', 'NewFacility , Facility Manager', 3),
(26, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => NewFacility , Facility Manager\n    [newValue] => ndhaknd , Reservation Manager\n)', '2017-08-26 22:18:39', '::1', 'localhost', '/users/edit/33', 'https://localhost/users/edit/33', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'NewFacility , Facility Manager', 'ndhaknd , Reservation Manager', 3),
(27, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => ksmjs , Visitor\n)', '2017-08-26 22:54:59', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'ksmjs , Visitor', 3),
(28, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => dacaa , Reservation Manager\n)', '2017-08-26 22:56:13', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'dacaa , Reservation Manager', 3),
(29, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => mah , \n)', '2017-08-26 23:03:05', '::1', 'localhost', '/users/delete/6', 'https://localhost/users', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'mah , ', 'NULL', 3),
(30, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Fac , Facility Manager\n)', '2017-08-26 23:04:05', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'Fac , Facility Manager', 3),
(31, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => Fac , Facility Manager\n)', '2017-08-26 23:04:11', '::1', 'localhost', '/users/delete/36', 'https://localhost/users', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'Fac , Facility Manager', 'NULL', 3),
(32, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Reservation , Reservation Manager\n)', '2017-08-28 14:14:01', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(33, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => guest , Admin\n)', '2017-08-28 14:15:22', '::1', 'localhost', '/users/delete/38', 'https://localhost/users', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'guest , Admin', 'NULL', 3),
(34, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Reservation , Reservation Manager\n    [newValue] => Visitor , Visitor\n)', '2017-08-28 14:16:26', '::1', 'localhost', '/users/edit/39', 'https://localhost/users/edit/39', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'Reservation , Reservation Manager', 'Visitor , Visitor', 3),
(35, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Facility , Facility Manager\n)', '2017-08-28 14:17:31', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'Facility , Facility Manager', 3),
(36, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => vis , Visitor\n)', '2017-08-28 17:55:50', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'vis , Visitor', 3),
(37, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => vis , Visitor\n    [newValue] => facilit , Facility Manager\n)', '2017-08-28 17:56:32', '::1', 'localhost', '/users/edit/41', 'https://localhost/users/edit/41', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'vis , Visitor', 'facilit , Facility Manager', 3),
(38, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => facilit , Facility Manager\n)', '2017-08-28 17:57:03', '::1', 'localhost', '/users/delete/41', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'facilit , Facility Manager', 'NULL', 3),
(39, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Facility , Facility Manager\n    [newValue] => FacilityAdmin , Admin\n)', '2017-08-29 00:42:52', '::1', 'localhost', '/users/edit/40', 'https://localhost/users/edit/40', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(40, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => FacilityAdmin , Admin\n    [newValue] => Facility , Facility Manager\n)', '2017-08-29 00:43:59', '::1', 'localhost', '/users/edit/40', 'https://localhost/users/edit/40', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'FacilityAdmin , Admin', 'Facility , Facility Manager', 3),
(41, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => res , Reservation Manager\n)', '2017-08-29 14:35:27', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'res , Reservation Manager', 3),
(42, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => visme , Visitor\n)', '2017-08-29 14:41:43', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'res', 'Reservation Manager', 'NULL', 'visme , Visitor', 41),
(43, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => newVisi , Visitor\n)', '2017-08-30 20:00:42', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(44, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => newVisi , Visitor\n)', '2017-08-30 20:02:08', '::1', 'localhost', '/users/delete/43', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'newVisi , Visitor', 'NULL', 3),
(45, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => visme , Visitor\n    [newValue] => vismejdhd , Reservation Manager\n)', '2017-08-30 20:02:36', '::1', 'localhost', '/users/edit/42', 'https://localhost/users/edit/42', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'visme , Visitor', 'vismejdhd , Reservation Manager', 3),
(46, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => gu , Guest\n)', '2017-08-30 20:03:04', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'gu , Guest', 3),
(47, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => gu , Guest\n)', '2017-08-30 20:31:30', '::1', 'localhost', '/users/delete/44', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'gu , Guest', 'NULL', 3),
(48, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => vismejdhd , Reservation Manager\n)', '2017-08-30 20:32:18', '::1', 'localhost', '/users/delete/42', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'vismejdhd , Reservation Manager', 'NULL', 3),
(49, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => res , Reservation Manager\n    [newValue] => resd , Guest\n)', '2017-08-30 20:55:05', '::1', 'localhost', '/users/edit/41', 'https://localhost/users/edit/41', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'res , Reservation Manager', 'resd , Guest', 3),
(50, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => resd , Guest\n)', '2017-08-30 21:01:21', '::1', 'localhost', '/users/delete/41', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'resd , Guest', 'NULL', 3),
(51, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => nfh , Reservation Manager\n)', '2017-08-30 21:05:26', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'nfh , Reservation Manager', 3),
(52, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => nhdkan , Facility Manager\n)', '2017-08-30 21:13:26', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'nhdkan , Facility Manager', 3),
(53, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nfh , Reservation Manager\n    [newValue] => nfhddada , Guest\n)', '2017-08-30 21:13:50', '::1', 'localhost', '/users/edit/45', 'https://localhost/users/edit/45', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'nfh , Reservation Manager', 'nfhddada , Guest', 3),
(54, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => nfhddada , Guest\n)', '2017-08-30 21:14:03', '::1', 'localhost', '/users/delete/45', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'nfhddada , Guest', 'NULL', 3),
(55, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nhdkan , Facility Manager\n    [newValue] => nhd , Admin\n)', '2017-08-31 13:12:29', '::1', 'localhost', '/users/edit/46', 'https://localhost/users/edit/46', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'nhdkan , Facility Manager', 'nhd , Admin', 3),
(56, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nhd , Admin\n    [newValue] => noone , Visitor\n)', '2017-09-05 13:06:13', '::1', 'localhost', '/users/edit/46', 'https://localhost/users/edit/46', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(57, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => ris , Reservation Manager\n)', '2017-09-05 13:13:02', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(58, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => riservations , Facility Manager\n    [newValue] => riservations , Facility Manager\n)', '2017-09-05 14:13:17', '::1', 'localhost', '/users/edit/47', 'https://localhost/users/edit/47', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(59, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => riservations , Facility Manager\n    [newValue] => guest , Guest\n)', '2017-09-05 14:32:19', '::1', 'localhost', '/users/edit/47', 'https://localhost/users/edit/47', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'riservations , Facility Manager', 'guest , Guest', 3),
(60, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => guest , Guest\n    [newValue] => newVisistor , Visitor\n)', '2017-09-05 14:48:43', '::1', 'localhost', '/users/edit/47', 'https://localhost/users/edit/47', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'guest , Guest', 'newVisistor , Visitor', 3),
(61, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => noone , Visitor\n    [newValue] => newGuestore , Guest\n)', '2017-09-05 15:54:11', '::1', 'localhost', '/users/edit/46', 'https://localhost/users/edit/46', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'noone , Visitor', 'newGuestore , Guest', 3),
(62, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => kjdu , Reservation Manager\n)', '2017-09-06 13:45:06', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'kjdu , Reservation Manager', 3),
(63, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => kjdu , Reservation Manager\n    [newValue] => kikjd , Visitor\n)', '2017-09-06 13:45:24', '::1', 'localhost', '/users/edit/48', 'https://localhost/users/edit/48', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'kjdu , Reservation Manager', 'kikjd , Visitor', 3),
(64, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => kikjd , Visitor\n)', '2017-09-06 13:45:34', '::1', 'localhost', '/users/delete/48', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'kikjd , Visitor', 'NULL', 3),
(65, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => newVksn , Visitor\n)', '2017-09-06 14:00:38', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'newVksn , Visitor', 3),
(66, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => newVksn , Visitor\n    [newValue] => newGuestlooo , Guest\n)', '2017-09-06 14:00:57', '::1', 'localhost', '/users/edit/49', 'https://localhost/users/edit/49', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'newVksn , Visitor', 'newGuestlooo , Guest', 3),
(67, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => newGuestlooo , Guest\n)', '2017-09-06 14:01:08', '::1', 'localhost', '/users/delete/49', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'newGuestlooo , Guest', 'NULL', 3),
(68, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => res , Reservation Manager\n)', '2017-09-09 02:16:30', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'res , Reservation Manager', 3),
(69, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => test , Visitor\n)', '2017-12-08 16:23:42', '127.0.0.1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'test , Visitor', 3),
(70, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Visitor , ACTIVE\n    [newValue] => test , Admin , ACTIVE\n)', '2017-12-08 16:34:21', '127.0.0.1', 'localhost:8765', '/users/edit/51', 'http://localhost:8765/users/edit/51', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'test , Visitor , ACTIVE', 'test , Admin , ACTIVE', 3),
(71, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin\n)', '2017-12-14 20:31:02', '::1', 'localhost:8765', '/users/delete/51', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'test , Admin', NULL, 3),
(72, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => app , \n)', '2017-12-14 20:33:56', '::1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'app , ', 3),
(73, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => app ,  , ACTIVE\n    [newValue] => app , Admin , ACTIVE\n)', '2017-12-14 20:36:31', '::1', 'localhost:8765', '/users/edit/52', 'http://localhost:8765/users/edit/52', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'app ,  , ACTIVE', 'app , Admin , ACTIVE', 3),
(74, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => app , Admin , ACTIVE\n    [newValue] => app ,  , ACTIVE\n)', '2017-12-14 20:37:42', '::1', 'localhost:8765', '/users/edit/52', 'http://localhost:8765/users/edit/52', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'app , Admin , ACTIVE', 'app ,  , ACTIVE', 3),
(75, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => vis , Visitor\n)', '2017-12-14 20:44:57', '::1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'vis , Visitor', 3),
(76, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => app , \n)', '2017-12-15 17:08:13', '127.0.0.1', 'localhost:8765', '/users/delete/52', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'app , ', NULL, 3),
(77, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => vis , Visitor\n)', '2017-12-15 17:08:17', '127.0.0.1', 'localhost:8765', '/users/delete/53', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'vis , Visitor', NULL, 3),
(78, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => app , Visitor\n)', '2017-12-15 17:12:19', '127.0.0.1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'app , Visitor', 3),
(79, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => admin , Admin\n)', '2018-01-07 23:32:09', '::1', 'localhost:8765', '/users/delete/4', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'admin , Admin', NULL, 3),
(80, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Test , Admin\n)', '2018-01-08 19:01:14', '::1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'Admin', 'Admin', NULL, 'Test , Admin', 1),
(81, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => Test , Admin\n)', '2018-01-08 19:01:22', '::1', 'localhost:8765', '/users/delete/6', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'Admin', 'Admin', 'Test , Admin', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `failed_login_attempts`
--

CREATE TABLE `failed_login_attempts` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `firebase`
--

CREATE TABLE `firebase` (
  `users_id` int(11) NOT NULL,
  `token` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gateways`
--

CREATE TABLE `gateways` (
  `id` int(11) NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 NOT NULL,
  `fqdn` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='table Kommentar\nlabel - Bezeichnung des Ortes\nfqdn URL für Gateway';

--
-- Dumping data for table `gateways`
--

INSERT INTO `gateways` (`id`, `label`, `fqdn`, `site_id`) VALUES
(1, 'RTST', 'rtst.gateway.smartparking.hornjak.de', 1),
(2, 'MST', 'mst.gateway.smartparking.hornjak.de', 4);

-- --------------------------------------------------------

--
-- Table structure for table `issuedComands`
--

CREATE TABLE `issuedComands` (
  `id` int(11) NOT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `cmd` varchar(10) CHARACTER SET utf8 NOT NULL,
  `destNode` int(11) DEFAULT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verbatim` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` enum('SQARE','CIRCLE','ELLIPSE') DEFAULT NULL COMMENT 'SQUARE=four corners, CIRCLE=upperLeft-upperRight means diameter,  ELLIPSE=upperLeft-upperRight means greatest expansion, lowerLeft-lowerLeft means smallest expansion',
  `upperLeftLongitude` double DEFAULT NULL,
  `upperLeftLatitude` double DEFAULT NULL,
  `upperRightLongitude` double DEFAULT NULL,
  `upperRightLatitude` double DEFAULT NULL,
  `lowerLeftLongitude` double DEFAULT NULL,
  `lowerLeftLatitude` double DEFAULT NULL,
  `lowerRightLongitude` double DEFAULT NULL,
  `lowerRightLatitude` double DEFAULT NULL,
  `width` double DEFAULT NULL,
  `length` double DEFAULT NULL,
  `imagePath` varchar(255) DEFAULT NULL COMMENT 'pictures for location'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `description`, `type`, `upperLeftLongitude`, `upperLeftLatitude`, `upperRightLongitude`, `upperRightLatitude`, `lowerLeftLongitude`, `lowerLeftLatitude`, `lowerRightLongitude`, `lowerRightLatitude`, `width`, `length`, `imagePath`) VALUES
(1, 'Lorem ipsum dolor sit amet', 'CIRCLE', 10, 50, 10, 50, 10, 50, 10, 50, 1, 1, 'Lorem ipsum dolor sit amet'),
(2, 'Lorem ipsum dolor sit amet', 'CIRCLE', 10, 55, 15, 55, 15, 55, 15, 55, 1, 1, 'Lorem ipsum dolor sit amet'),
(3, 'Lorem ipsum dolor sit amet', 'CIRCLE', 20, 60, 20, 60, 20, 60, 20, 60, 1, 1, 'Lorem ipsum dolor sit amet'),
(4, 'Lorem ipsum dolor sit amet', 'CIRCLE', 25, 65, 25, 65, 25, 65, 25, 65, 1, 1, 'Lorem ipsum dolor sit amet'),
(5, 'Lorem ipsum dolor sit amet', 'CIRCLE', 30, 70, 30, 70, 30, 70, 30, 70, 1, 1, 'Lorem ipsum dolor sit amet');

-- --------------------------------------------------------

--
-- Table structure for table `parkinglots`
--

CREATE TABLE `parkinglots` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL COMMENT 'interne parkinglot id',
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `mqttchannel` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `status` enum('blocked','free','blocking','freeing','occupied','error') CHARACTER SET utf8 NOT NULL DEFAULT 'error' COMMENT 'current state BLOCKED=barrier up,FREE=barier down,no car, BLOCKING=barrier is moving up,FREEING=barrier is moving down, OCCUPIED=car present, hopefully barrier down, ERROR=see error code',
  `ip` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `hwid` varchar(18) CHARACTER SET utf8 DEFAULT NULL COMMENT 'mac adress ??',
  `lastStateChange` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'timestamp of last state change',
  `type` enum('SINGLE','BARRIER') COLLATE utf8_unicode_ci NOT NULL COMMENT 'barrier type: SINGLE=one barrier fr each parking space, BARRIER=one barrier for mass parking space (Schranke)',
  `locationId` int(11) DEFAULT NULL COMMENT 'reference to location place (coordinates)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parkinglots`
--

INSERT INTO `parkinglots` (`id`, `pid`, `description`, `mqttchannel`, `gateway_id`, `status`, `ip`, `hwid`, `lastStateChange`, `type`, `locationId`) VALUES
(1, 1, 'Parkplatz 1', 'parking/1', 1, 'free', 'Lorem ipsum d', 'Lorem ipsum dolo', '2018-02-03 16:09:27', 'SINGLE', 1),
(2, 2, 'Parkplatz 2', 'parking/2', 1, 'free', 'Lorem ipsum d', 'Lorem ipsum dolo', '2018-02-03 16:09:27', 'SINGLE', 1),
(3, 3, 'Parkplatz 3', 'parking/3', 1, 'free', 'Lorem ipsum d', 'Lorem ipsum dolo', '2018-02-03 16:09:27', 'SINGLE', 1),
(4, 4, 'Parkplatz 4', 'parking/4', 1, 'free', 'Lorem ipsum d', 'Lorem ipsum dolo', '2018-02-03 16:09:27', 'SINGLE', 1),
(5, 5, 'Parkplatz 5', 'parking/5', 1, 'free', 'Lorem ipsum d', 'Lorem ipsum dolo', '2018-02-03 16:09:27', 'SINGLE', 1);

-- --------------------------------------------------------

--
-- Table structure for table `parkinglots_sites`
--

CREATE TABLE `parkinglots_sites` (
  `id` int(11) NOT NULL,
  `sitesId` int(11) NOT NULL,
  `parkingLotsId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parkinglots_sites`
--

INSERT INTO `parkinglots_sites` (`id`, `sitesId`, `parkingLotsId`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `token` char(32) CHARACTER SET utf8 NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `parkinglot_id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `visitedPersonId` int(11) DEFAULT NULL,
  `licencePlate` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reservationState` enum('RESERVED','BOOKED','CANCELED','SUPERKEY') COLLATE utf8_unicode_ci NOT NULL COMMENT 'SUPERKEY=free/block without reservation, Hausmeisterschlüssel',
  `messageInfo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isRecurring` enum('YES','NO') COLLATE utf8_unicode_ci DEFAULT NULL,
  `recurringID` int(11) DEFAULT NULL COMMENT 'see http://www.vertabelo.com/blog/technical-articles/again-and-again-managing-recurring-events-in-a-data-model',
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `token`, `start`, `end`, `parkinglot_id`, `users_id`, `visitedPersonId`, `licencePlate`, `reservationState`, `messageInfo`, `isRecurring`, `recurringID`, `created`) VALUES
(1, 'activeToken1', '2017-08-23 12:04:00', '2021-08-23 11:04:00', 1, 1, 1, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(2, 'inValidToken1', '2017-08-23 12:04:00', '2017-08-23 11:04:00', 1, 1, 1, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(3, 'inActiveToken1', '2022-08-23 12:04:00', '2022-08-24 11:04:00', 1, 1, 1, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(4, 'activeToken2', '2017-08-23 12:04:00', '2021-08-23 11:04:00', 2, 2, 2, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(5, 'inValidToken2', '2017-08-23 12:04:00', '2017-08-23 11:04:00', 2, 2, 2, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(6, 'inActiveToken2', '2022-08-23 12:04:00', '2022-08-24 11:04:00', 2, 2, 2, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(7, 'activeToken3', '2017-08-23 12:04:00', '2021-08-23 11:04:00', 3, 3, 3, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(8, 'inValidToken3', '2017-08-23 12:04:00', '2017-08-23 11:04:00', 3, 3, 3, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(9, 'inActiveToken3', '2022-08-23 12:04:00', '2022-08-24 11:04:00', 3, 3, 3, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(10, 'activeToken4', '2017-08-23 12:04:00', '2021-08-23 11:04:00', 4, 4, 4, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(11, 'inValidToken4', '2017-08-23 12:04:00', '2017-08-23 11:04:00', 4, 4, 4, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(12, 'inActiveToken4', '2022-08-23 12:04:00', '2022-08-24 11:04:00', 4, 4, 4, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(13, 'activeToken5', '2017-08-23 12:04:00', '2021-08-23 11:04:00', 5, 5, 5, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(14, 'inValidToken5', '2017-08-23 12:04:00', '2017-08-23 11:04:00', 5, 5, 5, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00'),
(15, 'inActiveToken5', '2022-08-23 12:04:00', '2022-08-24 11:04:00', 5, 5, 5, 'Lorem ipsum dolor sit amet', 'RESERVED', 'Lorem ipsum dolor sit amet', 'NO', 1, '2017-08-23 11:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `reservations_users`
--

CREATE TABLE `reservations_users` (
  `id` int(11) NOT NULL,
  `reservations_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Parker\nAnfrager\nBesucher/Visitor\nBetreiber\nVerwalter\nAdmin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Guest'),
(3, 'Visitor'),
(4, 'Reservation Manager'),
(5, 'Facility Manager');

-- --------------------------------------------------------

--
-- Table structure for table `sensorlogs`
--

CREATE TABLE `sensorlogs` (
  `id` int(11) NOT NULL,
  `parkinglot_id` int(11) DEFAULT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `z` int(11) DEFAULT NULL,
  `zThreshold` int(11) DEFAULT NULL,
  `state` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `supplyVoltage` float DEFAULT NULL,
  `temperature` int(11) DEFAULT NULL,
  `humidity` float DEFAULT NULL,
  `RSSI_gwRX` int(11) DEFAULT NULL,
  `RSSI_nodeRX_avg` int(11) DEFAULT NULL,
  `numRetries_NodeToGW` int(11) DEFAULT NULL,
  `numRxTimeouts` int(11) DEFAULT NULL,
  `errorCode` int(11) DEFAULT NULL,
  `filename` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verbatim` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `token` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `validTo` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'short name ',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'long name',
  `prefix` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `locationID` int(11) NOT NULL,
  `isPublic` enum('NO','YES') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO' COMMENT 'TRUE if connected parking lots are partly/full time public',
  `publicStart` datetime DEFAULT NULL,
  `publicEnd` datetime DEFAULT NULL,
  `isPublicRecurring` enum('YES','NO') COLLATE utf8_unicode_ci DEFAULT NULL,
  `recurringId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `name`, `description`, `prefix`, `locationID`, `isPublic`, `publicStart`, `publicEnd`, `isPublicRecurring`, `recurringId`) VALUES
(1, 'Site von 1', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 1, 'NO', '2017-09-11 18:20:20', '2018-09-11 18:20:20', 'NO', 1),
(2, 'Site von 2', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 2, 'NO', '2017-09-11 18:20:20', '2018-09-11 18:20:20', 'NO', 1),
(3, 'Site von 3', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 3, 'NO', '2017-09-11 18:20:20', '2018-09-11 18:20:20', 'NO', 1),
(4, 'Site von 4', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 4, 'NO', '2017-09-11 18:20:20', '2018-09-11 18:20:20', 'NO', 1),
(5, 'Site von 5', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 5, 'NO', '2017-09-11 18:20:20', '2018-09-11 18:20:20', 'NO', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE `tenants` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'short description',
  `label` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT 'long description'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`id`, `name`, `label`) VALUES
(1, 'TU Darmstadt', 'TUD'),
(2, 'Some Consultant', 'SCON'),
(3, 'KOM_Student_Group', 'KOMSG');

-- --------------------------------------------------------

--
-- Table structure for table `tenants_users`
--

CREATE TABLE `tenants_users` (
  `id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tenants_users`
--

INSERT INTO `tenants_users` (`id`, `tenant_id`, `user_id`) VALUES
(1, 1, 3),
(2, 2, 50),
(6, 1, 54);

-- --------------------------------------------------------

--
-- Table structure for table `timestamps`
--

CREATE TABLE `timestamps` (
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT 'username',
  `password` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'password\n',
  `created` datetime DEFAULT NULL COMMENT 'entry creation timestamp',
  `modified` datetime DEFAULT NULL COMMENT 'last modified',
  `email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'email adress',
  `mobileNumber` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'mobile number',
  `landlineNumber` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'fixed line number',
  `firstname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'firstname',
  `lastname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'lastname',
  `title` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'title like dr',
  `gender` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'male/female/other',
  `userState` enum('ACTIVE','CANCELED','BLOCKED','DELETED') COLLATE utf8_unicode_ci NOT NULL COMMENT 'ACTIVE=login allowed, CANCELED=INACTIVE,no login, BLOCKED=temporary inactive, no login, DELETED=finished, could be eraesed,no login',
  `company` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'company name',
  `department` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'department description',
  `street` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'street name and street number',
  `ZIP` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ZIP code'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created`, `modified`, `email`, `mobileNumber`, `landlineNumber`, `firstname`, `lastname`, `title`, `gender`, `userState`, `company`, `department`, `street`, `ZIP`) VALUES
(1, 'Admin', '$2y$10$u2gZyKyGsf8EGiSRRnf9JOvMEZLOUSe6a03za7M3iNnqGihXdeVZG', '2017-08-23 11:06:46', '2017-08-23 11:06:46', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'ACTIVE', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet'),
(2, 'Guest', '$2y$10$sIupxf/MhuxHYPnA7CS2c.BmyLvtIDcd/KEC7he8NfnjE1Z7Cqowq', '2017-08-23 11:06:46', '2017-08-23 11:06:46', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'ACTIVE', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet'),
(3, 'Visitor', '$2y$10$tweLZbnwCa6a6caEY2Lis.r3L1LTG5aDfV/XvJYYYqV1Lz5ln6wrW', '2017-08-23 11:06:46', '2017-08-23 11:06:46', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'ACTIVE', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet'),
(4, 'Reservation Manager', '$2y$10$TOLMQ6rizgSU7PQnp1TtWeBT8z7gTBb2weu7kZzYVpf6Z3AKGbzne', '2017-08-23 11:06:46', '2017-08-23 11:06:46', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'ACTIVE', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet'),
(5, 'Facility Manager', '$2y$10$z.SBla.4m6m0FfD/8NP4S.h8lzBWT6CrBYh2VzSB6u4YkojdmOgQi', '2017-08-23 11:06:46', '2017-08-23 11:06:46', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'ACTIVE', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet');

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `users_zones`
--

CREATE TABLE `users_zones` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `keyName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '????',
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_zones`
--

INSERT INTO `users_zones` (`id`, `user_id`, `zone_id`, `keyName`, `start`, `end`) VALUES
(1, 1, 1, 'Lorem ipsum dolor sit amet', '2017-08-23 11:07:11', '2017-08-23 11:07:11'),
(2, 2, 2, 'Lorem ipsum dolor sit amet', '2017-08-23 11:07:11', '2017-08-23 11:07:11'),
(3, 3, 3, 'Lorem ipsum dolor sit amet', '2017-08-23 11:07:11', '2017-08-23 11:07:11'),
(4, 4, 4, 'Lorem ipsum dolor sit amet', '2017-08-23 11:07:11', '2017-08-23 11:07:11'),
(5, 5, 5, 'Lorem ipsum dolor sit amet', '2017-08-23 11:07:11', '2017-08-23 11:07:11');

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE `zones` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`id`, `name`, `description`) VALUES
(1, 'Zone 1', 'Description Zone 1'),
(2, 'Zone 2', 'Description Zone 2'),
(3, 'Zone 3', 'Description Zone 3'),
(4, 'Zone 4', 'Description Zone 4'),
(5, 'Zone 5', 'Description Zone 5');

-- --------------------------------------------------------

--
-- Table structure for table `zones_sites`
--

CREATE TABLE `zones_sites` (
  `id` int(11) NOT NULL,
  `zonesId` int(11) NOT NULL,
  `sitesId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zones_sites`
--

INSERT INTO `zones_sites` (`id`, `zonesId`, `sitesId`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apps`
--
ALTER TABLE `apps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_acl`
--
ALTER TABLE `auth_acl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_role_id_aa` (`role_id`);

--
-- Indexes for table `calender`
--
ALTER TABLE `calender`
  ADD PRIMARY KEY (`recurringId`,`recurringTypeId`);

--
-- Indexes for table `calender_exceptions`
--
ALTER TABLE `calender_exceptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `res_id_idx` (`res_id`);

--
-- Indexes for table `database_logs`
--
ALTER TABLE `database_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_user_id_dl` (`user_id`);

--
-- Indexes for table `failed_login_attempts`
--
ALTER TABLE `failed_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `firebase`
--
ALTER TABLE `firebase`
  ADD PRIMARY KEY (`users_id`,`token`);

--
-- Indexes for table `gateways`
--
ALTER TABLE `gateways`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `idx_fk_site_id_gate` (`site_id`);

--
-- Indexes for table `issuedComands`
--
ALTER TABLE `issuedComands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `destNode` (`destNode`),
  ADD KEY `gateway_id_idx` (`gateway_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parkinglots`
--
ALTER TABLE `parkinglots`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pid` (`pid`),
  ADD KEY `pid_2` (`pid`),
  ADD KEY `pid_3` (`pid`),
  ADD KEY `idx_fk_gateway_id_park` (`gateway_id`),
  ADD KEY `idx_fk_locationId_park` (`locationId`);

--
-- Indexes for table `parkinglots_sites`
--
ALTER TABLE `parkinglots_sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_sitesId_parksites` (`sitesId`),
  ADD KEY `idx_fk_parkingLotId_parksites` (`parkingLotsId`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_parkinglot_id_res` (`parkinglot_id`),
  ADD KEY `idx_fk_recurringID_res` (`recurringID`),
  ADD KEY `idx_fk_users_id_res` (`users_id`),
  ADD KEY `idx_fk_visitedPersonId_res` (`visitedPersonId`);

--
-- Indexes for table `reservations_users`
--
ALTER TABLE `reservations_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_reservations_id_resusers` (`reservations_id`),
  ADD KEY `idx_fk_users_id_resusers` (`users_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sensorlogs`
--
ALTER TABLE `sensorlogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_parkinglot_id_sensor` (`parkinglot_id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_id_session` (`id`);
ALTER TABLE `session` ADD FULLTEXT KEY `idx_token_session` (`token`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_locationID_sites` (`locationID`),
  ADD KEY `idx_fk_recurringId_sites` (`recurringId`);

--
-- Indexes for table `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenants_users`
--
ALTER TABLE `tenants_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_tenant_id_tenusr` (`tenant_id`),
  ADD KEY `idx_fk_user_id_tenusr` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_role_id_usroles` (`role_id`),
  ADD KEY `idx_fk_user_id_usroles` (`user_id`);

--
-- Indexes for table `users_zones`
--
ALTER TABLE `users_zones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_user_id_uszones` (`user_id`),
  ADD KEY `idx_fk_zone_id_uszones` (`zone_id`);

--
-- Indexes for table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zones_sites`
--
ALTER TABLE `zones_sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_id_zonsites` (`id`,`zonesId`,`sitesId`),
  ADD KEY `idx_fk_zonesId_zonsites` (`zonesId`),
  ADD KEY `idx_fk_sitesId_zonsites` (`sitesId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apps`
--
ALTER TABLE `apps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_acl`
--
ALTER TABLE `auth_acl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `calender`
--
ALTER TABLE `calender`
  MODIFY `recurringId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `calender_exceptions`
--
ALTER TABLE `calender_exceptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `database_logs`
--
ALTER TABLE `database_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `failed_login_attempts`
--
ALTER TABLE `failed_login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gateways`
--
ALTER TABLE `gateways`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `issuedComands`
--
ALTER TABLE `issuedComands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `parkinglots`
--
ALTER TABLE `parkinglots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `parkinglots_sites`
--
ALTER TABLE `parkinglots_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `reservations_users`
--
ALTER TABLE `reservations_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sensorlogs`
--
ALTER TABLE `sensorlogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tenants`
--
ALTER TABLE `tenants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tenants_users`
--
ALTER TABLE `tenants_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users_zones`
--
ALTER TABLE `users_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `zones`
--
ALTER TABLE `zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `zones_sites`
--
ALTER TABLE `zones_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_acl`
--
ALTER TABLE `auth_acl`
  ADD CONSTRAINT `fk_role_id_aa` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `calender_exceptions`
--
ALTER TABLE `calender_exceptions`
  ADD CONSTRAINT `res_id` FOREIGN KEY (`res_id`) REFERENCES `calender` (`recurringId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `database_logs`
--
ALTER TABLE `database_logs`
  ADD CONSTRAINT `fk_user_id_dl` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gateways`
--
ALTER TABLE `gateways`
  ADD CONSTRAINT `fk_site_id_gate` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `issuedComands`
--
ALTER TABLE `issuedComands`
  ADD CONSTRAINT `gateway_id` FOREIGN KEY (`gateway_id`) REFERENCES `gateways` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `parkinglots`
--
ALTER TABLE `parkinglots`
  ADD CONSTRAINT `fk_gateway_id_park` FOREIGN KEY (`gateway_id`) REFERENCES `gateways` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_locationId_park` FOREIGN KEY (`locationId`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `parkinglots_sites`
--
ALTER TABLE `parkinglots_sites`
  ADD CONSTRAINT `fk_parkingLotId_parksites` FOREIGN KEY (`parkingLotsId`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sitesId_parksites` FOREIGN KEY (`sitesId`) REFERENCES `sites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `fk_parkinglot_id_res` FOREIGN KEY (`parkinglot_id`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recurringID_res` FOREIGN KEY (`recurringID`) REFERENCES `calender` (`recurringId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_id_res` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_visitedPersonId_res` FOREIGN KEY (`visitedPersonId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `reservations_users`
--
ALTER TABLE `reservations_users`
  ADD CONSTRAINT `fk_reservations_id_resusers` FOREIGN KEY (`reservations_id`) REFERENCES `reservations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_id_resusers` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sensorlogs`
--
ALTER TABLE `sensorlogs`
  ADD CONSTRAINT `fk_parkinglot_id_sensor` FOREIGN KEY (`parkinglot_id`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sites`
--
ALTER TABLE `sites`
  ADD CONSTRAINT `fk_locationID_sites` FOREIGN KEY (`locationID`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recurringId_sites` FOREIGN KEY (`recurringId`) REFERENCES `calender` (`recurringId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tenants_users`
--
ALTER TABLE `tenants_users`
  ADD CONSTRAINT `fk_tenant_id_tenusr` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_id_tenusr` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD CONSTRAINT `fk_role_id_usroles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_id_usroles` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_zones`
--
ALTER TABLE `users_zones`
  ADD CONSTRAINT `fk_user_id_uszones` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_zone_id_uszones` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zones_sites`
--
ALTER TABLE `zones_sites`
  ADD CONSTRAINT `fk_sitesId_zonsites` FOREIGN KEY (`sitesId`) REFERENCES `sites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_zonesId_zonsites` FOREIGN KEY (`zonesId`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
