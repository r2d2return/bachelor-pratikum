package com.tud.kom.parkinglot.notification.openParkingLot;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import static com.tud.kom.parkinglot.notification.openParkingLot.OpenParkingLotService.RESERVATION_EXTRA;

public class OpenParkingLotBroadcast extends WakefulBroadcastReceiver {
    private static final String TAG = OpenParkingLotBroadcast.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent parkingLotService = new Intent(context, OpenParkingLotService.class);
        parkingLotService.putExtra(RESERVATION_EXTRA, intent.getSerializableExtra(RESERVATION_EXTRA));
        startWakefulService(context, parkingLotService);
    }
}
