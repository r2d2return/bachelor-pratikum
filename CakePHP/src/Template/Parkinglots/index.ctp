<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
            <li class="btn btn-default"><?= $this->Html->link(__('New Parkinglot'), ['action' => 'add']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Reservations'), ['controller' => 'Reservations', 'action' => 'index']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Sensorlogs'), ['controller' => 'Sensorlogs', 'action' => 'index']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Parkinglot List
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('pid') ?></th>
                            <th><?= $this->Paginator->sort('description','Description or Number') ?></th>
                            <th><?= $this->Paginator->sort('site_id', 'Site') ?></th>
                            <th><?= $this->Paginator->sort('status') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $counter = 1; ?>
                        <?php foreach ($parkinglots as $parkinglot): ?>
                        <tr role="row" class="gradeA <?php echo ($counter%2 == 0 ?  'odd': 'even'); ?>">
                        <td class="sorting_1"><?= $this->Number->format($parkinglot->id) ?></td>
                        <td><?= $this->Number->format($parkinglot->pid) ?></td>
                        <td><?= h($parkinglot->description) ?></td>
                        <!-- <td><?= $this->Number->format($parkinglot->site_id) ?></td> -->
                        <td><?= h($parkinglot->site->name) ?></td>
                        <?php
                           switch($parkinglot->status) {
                                case "blocked":
                                    $pcolor = "red";
                                    break;
                                case "occupied":
                                    $pcolor = "orange";
                                    break;
                                case "free":
                                    $pcolor = "lightgreen";
                                    break;
                                case "freeing":
                                    $pcolor = "yellow";
                                    break;
                                case "blocking":
                                    $pcolor = "lavender";
                                    break;
                                default:
                                    $pcolor = "white";
                            }
                        ?>
                        <td style="background-color:<?php echo $pcolor; ?>"><?= h($parkinglot->status) ?></td>
                        <td class="actions center">
                            <?= $this->Html->image('view.gif', [ 'alt' => __('View'),
	                								     'url' => ['controller' => 'Parkinglots',
	                								     		   'action' => 'view', $parkinglot->id]]) ?>
                            <?= $this->Html->image('log.png', [ 'alt' => __('Log'),
                								          'url' => ['controller' => 'Sensorlogs',
                								     		   'action' => 'view', $parkinglot->pid]
                								   ]) ?>
                            <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'),
	                								     'url' => ['controller' => 'Parkinglots',
	                								     		   'action' => 'edit', $parkinglot->id]
	                								   ]) ?>
                            <?= $this->Html->link(
	                					  $this->Html->image('delete.png', [ 'alt' => __('Delete')]),
	                					  ['controller' => 'Parkinglots', 'action' => 'delete', $parkinglot->id],
	                					  ['confirm' => __('Are you sure you want to delete # {0}?', $parkinglot->id),
	                					   'escapeTitle' => false]
	                				     ) ?>
                        </td>
                    </tr>
                    <?php $counter++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p><?= $this->Paginator->counter() ?></p>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<?php $this->Html->addCrumb('Parkinglots', ['controller' => 'Parkinglots', 'action' => 'index']); ?>