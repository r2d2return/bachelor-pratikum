<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\Network\Email\Email;
use Cake\ORM\TableRegistry;
use Cake\Database\Type;
use App\Model\Entity\ReservationRequest;
use Cake\I18n\Time;


/**
 * ReservationRequests Controller
 *
 * @property \App\Model\Table\ReservationRequestsTable $ReservationRequests
 */
class ReservationRequestsController extends AppController
{

    /**
     * Index method
     * List all active reservations whose end-time has not yet passed
     *
     * @return void
     */
    public function index()
    {
        // get now date in valid format
        $nowtime = Time::now()->i18nFormat('YYYY-MM-dd HH:mm:ss');
        $this->paginate = [
            'conditions' => ['ReservationRequests.end >=' => $nowtime,
                'ReservationRequests.status !=' => 'accept'],
            'order' => [
            'ReservationRequests.start' => 'desc']
        ];

        $this->set('reservationRequests', $this->paginate($this->ReservationRequests));
        $this->set('_serialize', ['reservationRequests']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reservationRequest = $this->ReservationRequests->newEntity();
        if ($this->request->is('post')) {
            $reservationRequest = $this->ReservationRequests->patchEntity($reservationRequest, $this->request->data);
            if ($this->ReservationRequests->save($reservationRequest)) {
                $this->Flash->success(__('The reservation is requested'));
                
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Please try again'));
            }
        }

        // hand over values to view
        $this->set(compact('reservationRequest'));
        $this->set('_serialize', ['reservationRequest']);
    }


    /**
     * Delete method
     *
     * @param string|null $id Reservation request id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['get','post','delete']);
        $reservationRequest = $this->ReservationRequests->get($id);
        if ($this->ReservationRequests->delete($reservationRequest)) {
            $this->Flash->success(__('The reservation request has been deleted.'));
        } else {
            $this->Flash->error(__('The reservation request could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Reservation id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function accept($id = null)
    {
        $reservationRequest = $this->ReservationRequests->get($id);
        $loggedInUser = $this->Auth->user();
        $this->loadModel('Reservations');
        $reservation = $this->Reservations->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reservation = $this->Reservations->patchEntity($reservation, $this->request->data);

            $check = $this->ReservationRequests->updateAll(
                array('status' => "accept"),
                array('id' => $id)
            );

            if (true) {
                if ($this->Reservations->save($reservation)) {
                    $this->Flash->success(__('The reservation has been saved and the visitor was notified of any changes to his booking.'));
                    // get full reservation object
                    $reservation = $this->Reservations->get($reservation->id, [
                        'contain' => ['Visitors', 'Parkinglots']
                    ]);
                    // send reservation change notification
                    $this->sendReservationMail($reservation, true);
                    // update message for client
                    $this->mqttMessage(
                        $reservation->id.";".
                        $reservation->parkinglot->pid.";".
                        $this->getTimeAsISO8106($reservation->start).";".
                        $this->getTimeAsISO8106($reservation->end).";2"
                        ,$reservation->parkinglot->mqttchannel. "/reservation");
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The reservation could not be saved. Please, try again.'));
                }
            } else {
                $this->Flash->error(__('The reservation could not be saved. Please, try again.'));
            }
        }

        $visitors = $this->Reservations->Visitors->find('list', ['limit' => 200]);
        $parkinglots = $this->Reservations->Parkinglots->find('list', ['limit' => 200]);
        $token = uniqid(); //uuid_create();
        // hand over values to view
        $this->set(compact('reservationRequest', 'loggedInUser', 'parkinglots','token', 'visitors', 'reservation'));
        $this->set('_serialize', ['reservation']);

        //$this->set(array('reservationRequest'=> $reservationRequest, 'loggedInUser' => $loggedInUser, ));
        //$this->set('_serialize', array('reservationRequest', 'loggedInUser'));
    }

    /**
     * helper get time function
     * get Time in ISO8106 format
     *
     * @param DateTime $ctime
     * @return DateTime - time in ISO8106 format string
     */
    private function getTimeAsISO8106($ctime) {
        $temptime = new \DateTime(substr($ctime,0,8));
        $temptime->setTime((int)substr($ctime,9,10), (int)substr($ctime,12,13));
        return $temptime->format('c');
    }
   
}
