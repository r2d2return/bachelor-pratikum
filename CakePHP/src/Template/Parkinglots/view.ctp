<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
            <?php if($currentAuthUserRoles[0] == "Admin"): ?>
                <li class="btn btn-default"><?= $this->Html->link(__('Edit Parkinglot'), ['action' => 'edit', $parkinglot->id]) ?></li>
                <li class="btn btn-default"><?= $this->Form->postLink(__('Delete Parkinglot'), ['action' => 'delete', $parkinglot->id], ['confirm' => __('Are you sure you want to delete # {0}?', $parkinglot->id)]) ?></li>
                <li class="btn btn-default"><?= $this->Html->link(__('List Parkinglots'), ['action' => 'index']) ?></li>
                <li class="btn btn-default"><?= $this->Html->link(__('New Parkinglot'), ['action' => 'add']) ?></li>
            <?php elseif($currentAuthUserRoles[0] == "Reservation Manager"): ?>
                <li class="btn btn-default"><?= $this->Html->link(__('List Reservations'), ['controller' => 'Reservations','action' => 'index']) ?></li>
                <li class="btn btn-default"><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
            <?php endif?>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                User ID: <?= h($parkinglot->id) ?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr role="row" class="gradeA odd">
                            <th><?= __('Id') ?></th>
                            <td><?= $this->Number->format($parkinglot->id) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Description') ?></th>
                            <td><?= h($parkinglot->description) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Mqttchannel') ?></th>
                            <td><?= h($parkinglot->mqttchannel) ?></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php $this->Html->addCrumb('Parkinglots', '/parkinglots'); ?>
<?php $this->Html->addCrumb('Details', ['controller' => 'Parkinglots', 'action' => 'view']); ?>