<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CalenderExceptionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CalenderExceptionsTable Test Case
 */
class CalenderExceptionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CalenderExceptionsTable
     */
    public $CalenderExceptions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.calender_exceptions',
        'app.res'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CalenderExceptions') ? [] : ['className' => CalenderExceptionsTable::class];
        $this->CalenderExceptions = TableRegistry::get('CalenderExceptions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CalenderExceptions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
