<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sensorlogs Model
 *
 * @property \App\Model\Table\ParkinglotsTable|\Cake\ORM\Association\BelongsTo $Parkinglots
 *
 * @method \App\Model\Entity\Sensorlog get($primaryKey, $options = [])
 * @method \App\Model\Entity\Sensorlog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Sensorlog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Sensorlog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sensorlog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Sensorlog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Sensorlog findOrCreate($search, callable $callback = null, $options = [])
 */
class SensorlogsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sensorlogs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Parkinglots', [
            'foreignKey' => 'parkinglot_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('x')
            ->allowEmpty('x');

        $validator
            ->integer('y')
            ->allowEmpty('y');

        $validator
            ->integer('z')
            ->allowEmpty('z');

        $validator
            ->integer('zThreshold')
            ->allowEmpty('zThreshold');

        $validator
            ->allowEmpty('state');

        $validator
            ->numeric('supplyVoltage')
            ->allowEmpty('supplyVoltage');

        $validator
            ->integer('temperature')
            ->allowEmpty('temperature');

        $validator
            ->numeric('humidity')
            ->allowEmpty('humidity');

        $validator
            ->integer('RSSI_gwRX')
            ->allowEmpty('RSSI_gwRX');

        $validator
            ->integer('RSSI_nodeRX_avg')
            ->allowEmpty('RSSI_nodeRX_avg');

        $validator
            ->integer('numRetries_NodeToGW')
            ->allowEmpty('numRetries_NodeToGW');

        $validator
            ->integer('numRxTimeouts')
            ->allowEmpty('numRxTimeouts');

        $validator
            ->integer('errorCode')
            ->allowEmpty('errorCode');

        $validator
            ->allowEmpty('filename');

        $validator
            ->dateTime('logtime')
            ->requirePresence('logtime', 'create')
            ->notEmpty('logtime');

        $validator
            ->allowEmpty('verbatim');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parkinglot_id'], 'Parkinglots'));

        return $rules;
    }
}
