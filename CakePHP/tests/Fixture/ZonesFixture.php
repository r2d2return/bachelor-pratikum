<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ZonesFixture
 *
 */
class ZonesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'description' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'name' => 'Zone 1',
            'description' => 'Zone of User 1'
        ],

        [
            'id' => 2,
            'name' => 'Zone 2',
            'description' => 'Zone of User 2'
        ],

        [
            'id' => 3,
            'name' => 'Zone 3',
            'description' => 'Zone of User 3'
        ],

        [
            'id' => 4,
            'name' => 'Zone 4',
            'description' => 'Zone of User 4'
        ],

        [
            'id' => 5,
            'name' => 'Zone 5',
            'description' => 'Zone of User 5'
        ]
    ];
}
