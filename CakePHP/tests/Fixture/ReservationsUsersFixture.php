<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ReservationsUsersFixture
 *
 */
class ReservationsUsersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    /*
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'reservations_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'users_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'reservations_id_idx' => ['type' => 'index', 'columns' => ['reservations_id'], 'length' => []],
            'users_id_idx' => ['type' => 'index', 'columns' => ['users_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'resuse_reservations_id' => ['type' => 'foreign', 'columns' => ['reservations_id'], 'references' => ['reservations', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'resuse_users_id' => ['type' => 'foreign', 'columns' => ['users_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];*/
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'reservations_id' => 1,
            'users_id' => 1
        ],

         [
            'id' => 2,
            'reservations_id' => 2,
            'users_id' => 1
        ],

         [
            'id' => 3,
            'reservations_id' => 3,
            'users_id' => 1
        ],

        [
            'id' => 4,
            'reservations_id' => 4,
            'users_id' => 2
        ],

         [
            'id' => 5,
            'reservations_id' => 5,
            'users_id' => 2
        ],

         [
            'id' => 6,
            'reservations_id' => 6,
            'users_id' => 2
        ],


        [
            'id' => 7,
            'reservations_id' => 7,
            'users_id' => 3
        ],

         [
            'id' => 8,
            'reservations_id' => 8,
            'users_id' => 3
        ],

         [
            'id' => 9,
            'reservations_id' => 9,
            'users_id' => 3
        ],


        [
            'id' => 10,
            'reservations_id' => 10,
            'users_id' => 4
        ],

         [
            'id' => 11,
            'reservations_id' => 11,
            'users_id' => 4
        ],

         [
            'id' => 12,
            'reservations_id' => 12,
            'users_id' => 4
        ],


        [
            'id' => 13,
            'reservations_id' => 13,
            'users_id' => 5
        ],

         [
            'id' => 14,
            'reservations_id' => 14,
            'users_id' => 5
        ],

         [
            'id' => 15,
            'reservations_id' => 15,
            'users_id' => 5
        ],

      


        


    ];
}
