<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ZonesSitesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ZonesSitesTable Test Case
 */
class ZonesSitesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ZonesSitesTable
     */
    public $ZonesSites;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.zones_sites'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ZonesSites') ? [] : ['className' => ZonesSitesTable::class];
        $this->ZonesSites = TableRegistry::get('ZonesSites', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ZonesSites);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
