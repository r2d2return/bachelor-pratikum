package com.tud.kom.parkinglot.activities.history;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.activities.overview.ReservationAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    private ReservationAdapter mAdapter;

    private RecyclerView mList;

    private HistoryViewModel mViewModel;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        mViewModel = ViewModelProviders.of(this, new HistoryViewModelFactory(getContext())).get(HistoryViewModel.class);

        TextView mTextEmptyList = view.findViewById(R.id.list_empty);

        mViewModel.getData().observe(this, reservations -> {
            mAdapter.setData(reservations);
            if (reservations.isEmpty()) {
                mTextEmptyList.setVisibility(View.VISIBLE);
            }
            else {
                mTextEmptyList.setVisibility(View.GONE);
            }
        });

        mViewModel.getError().observe(this, this::onError);

        SwipeRefreshLayout swipeLayout = view.findViewById(R.id.swipe_layout);
        swipeLayout.setOnRefreshListener(() -> mViewModel.loadData());
        mViewModel.isLoading().observe(this, swipeLayout::setRefreshing);

        mList = view.findViewById(R.id.list);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        mList.setLayoutManager(llm);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter = new ReservationAdapter(true, null);
        mList.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        mViewModel.loadData();
    }

    /**
     * Handle download or parsing errors.
     * <p>
     * Display the error message in a toast.
     *
     * @param error the error message
     */
    private void onError(final String error) {
        Toast.makeText(getContext(), R.string.internal_server_error, Toast.LENGTH_SHORT).show();
    }
}
