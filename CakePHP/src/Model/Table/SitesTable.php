<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sites Model
 *
 * @property \App\Model\Table\GatewaysTable|\Cake\ORM\Association\HasMany $Gateways
 * @property \App\Model\Table\ParkinglotsTable|\Cake\ORM\Association\BelongsToMany $Parkinglots
 * @property \App\Model\Table\ZonesTable|\Cake\ORM\Association\BelongsToMany $Zones
 *
 * @method \App\Model\Entity\Site get($primaryKey, $options = [])
 * @method \App\Model\Entity\Site newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Site[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Site|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Site patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Site[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Site findOrCreate($search, callable $callback = null, $options = [])
 */
class SitesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sites');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Gateways', [
            'foreignKey' => 'site_id'
        ]);
        $this->belongsToMany('Parkinglots', [
            'foreignKey' => 'sitesId',
            'targetForeignKey' => 'parkingLotsId',
            'joinTable' => 'parkinglots_sites'
        ]);
        $this->belongsToMany('Zones', [
            'foreignKey' => 'sitesId',
            'targetForeignKey' => 'zonesId',
            'joinTable' => 'zones_sites'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->scalar('prefix')
            ->allowEmpty('prefix');

        $validator
            ->integer('locationID')
            ->requirePresence('locationID', 'create')
            ->notEmpty('locationID');

        $validator
            ->scalar('isPublic')
            ->allowEmpty('isPublic');

        $validator
            ->dateTime('publicStart')
            ->allowEmpty('publicStart');

        $validator
            ->dateTime('publicEnd')
            ->allowEmpty('publicEnd');

        $validator
            ->scalar('isPublicRecurring')
            ->allowEmpty('isPublicRecurring');

        $validator
            ->integer('recurringId')
            ->allowEmpty('recurringId');

        return $validator;
    }
}
