<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3><?= __('Database Logs') ?></h3>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('tenant') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('action') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('userName') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('userRole') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('oldValue') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('newValue') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $counter = 1; ?>
                    <?php foreach ($databaseLogs as $databaseLog): ?>
                        <tr role="row" class="gradeA <?php echo($counter % 2 == 0 ? 'odd' : 'even'); ?>">
                            <td><?= h($databaseLog->id) ?></td>
                            <td><?= h($databaseLog->created) ?></td>
                            <td><?= h($databaseLog->tenant) ?></td>
                            <td><?= h($databaseLog->action) ?></td>
                            <td><?= h($databaseLog->userName) ?></td>
                            <td><?= h($databaseLog->userRole) ?></td>
                            <td><?= h($databaseLog->oldValue) ?></td>
                            <td><?= h($databaseLog->newValue) ?></td>
                            <td><?= $databaseLog->has('user_id') ? $this->Html->link($databaseLog->user_id, ['controller' => 'Users', 'action' => 'view', $databaseLog->user_id]) : '' ?></td>
                            <td style="text-align: center">
                                <?= $this->Html->image('view.gif', ['alt' => __('View'), 'url' => ['controller' => 'databaseLogs', 'action' => 'view', $databaseLog->id]]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('first')) ?>
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                        <?= $this->Paginator->last(__('last') . ' >>') ?>
                    </ul>
                    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>
