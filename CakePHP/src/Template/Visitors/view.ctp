<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Visitor'), ['action' => 'edit', $visitor->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Visitor'), ['action' => 'delete', $visitor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $visitor->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Visitors'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Visitor'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Reservations'), ['controller' => 'Reservations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reservation'), ['controller' => 'Reservations', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="visitors view large-10 medium-9 columns">
    <h2><?= h($visitor->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Title') ?></h6>
            <p><?= h($visitor->title) ?></p>
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($visitor->name) ?></p>
            <h6 class="subheader"><?= __('Mail') ?></h6>
            <p><?= h($visitor->mail) ?></p>
            <h6 class="subheader"><?= __('Mobile') ?></h6>
            <p><?= h($visitor->mobile) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($visitor->id) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Gender') ?></h6>
            <?= $this->Text->autoParagraph(h($visitor->gender)) ?>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Reservations') ?></h4>
    <?php if (!empty($visitor->reservations)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Visitor Id') ?></th>
            <th><?= __('Token') ?></th>
            <th><?= __('Start') ?></th>
            <th><?= __('End') ?></th>
            <th><?= __('VisitedPersonName') ?></th>
            <th><?= __('ParkingLot Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($visitor->reservations as $reservations): ?>
        <tr>
            <td><?= h($reservations->id) ?></td>
            <td><?= h($reservations->visitor_id) ?></td>
            <td><?= h($reservations->token) ?></td>
            <td><?= h($reservations->start) ?></td>
            <td><?= h($reservations->end) ?></td>
            <td><?= h($reservations->visitedPersonName) ?></td>
            <td><?= h($reservations->parkinglot_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Reservations', 'action' => 'view', $reservations->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Reservations', 'action' => 'edit', $reservations->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Reservations', 'action' => 'delete', $reservations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reservations->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<?php $this->Html->addCrumb('Visitors', '/visitors'); ?>
<?php $this->Html->addCrumb('Details', ['controller' => 'Visitors', 'action' => 'view']); ?>