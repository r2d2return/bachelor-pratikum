<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
    <div class="col-lg-12">
    <h1 class="page-header"><?= __('Actions') ?></h1>
    <ul class="side-nav">
        <li class="btn btn-default"><?= $this->Html->link(__('List Roles'), ['action' => 'index']) ?></li>
        <li class="btn btn-default"><?= $this->Html->link(__('List Auth Acl'), ['controller' => 'AuthAcl', 'action' => 'index']) ?></li>
        <li class="btn btn-default"><?= $this->Html->link(__('New Auth Acl'), ['controller' => 'AuthAcl', 'action' => 'add']) ?></li>
        <li class="btn btn-default"><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li class="btn btn-default"><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li class="btn btn-default"><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $role->id],
                ['confirm' => __('Are you sure you want to delete {0}?', $role->name)]
            )
        ?></li>
    </ul>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Reservation
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <?= $this->Form->create($role) ?>
                        <fieldset>
                            <legend><?= __('Edit Role') ?></legend>
                            <?php
                                echo "<div class='form-group'>".$this->Form->input('name', ['label' => 'Name:', 'class' => 'form-control'])."</div>";
                                /* echo "<div class='form-group'>".$this->Form->input('user_id', array('type' => 'select',
                                                                'options' => $users,'class' => 'form-control'
                                                                ))."</div>"; */
                            ?>
                        </fieldset>
                        <?= $this->Form->button('Submit',  array('class' => 'btn btn-default')); ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php $this->Html->addCrumb('Roles', '/roles'); ?>
<?php $this->Html->addCrumb('Edit', ['controller' => 'Roles', 'action' => 'edit']); ?>