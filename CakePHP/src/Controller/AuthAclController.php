<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AuthAcl Controller
 *
 * @property \App\Model\Table\AuthAclTable $AuthAcl
 *
 * @method \App\Model\Entity\AuthAcl[] paginate($object = null, array $settings = [])
 */
class AuthAclController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Roles']
        ];
        $authAcl = $this->paginate($this->AuthAcl);
        $this->set(compact('authAcl'));
        $this->set('_serialize', ['authAcl']);
    }

    /**
     * View method
     *
     * @param string|null $id Auth Acl id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $authAcl = $this->AuthAcl->get($id, [
            'contain' => ['Roles']
        ]);
        $authRoles = $this->AuthAcl->authRoles($authAcl->role_id);
        $this->set(compact('authAcl', 'authRoles'));
        $this->set('_serialize', ['authAcl']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $authAcl = $this->AuthAcl->newEntity();
        if ($this->request->is('post')) {
            $authAcl = $this->AuthAcl->patchEntity($authAcl, $this->request->getData());
            if ($this->AuthAcl->save($authAcl)) {
                $this->Flash->success(__('The auth acl has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The auth acl could not be saved. Please, try again.'));
        }
        $roles = $this->AuthAcl->Roles->find('list', ['limit' => 200]);
        $this->set(compact('authAcl', 'roles'));
        $this->set('_serialize', ['authAcl']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Auth Acl id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $authAcl = $this->AuthAcl->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $authAcl = $this->AuthAcl->patchEntity($authAcl, $this->request->getData());
            if ($this->AuthAcl->save($authAcl)) {
                $this->Flash->success(__('The auth acl has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The auth acl could not be saved. Please, try again.'));
        }
        $roles = $this->AuthAcl->Roles->find('list', ['limit' => 200]);
        $authRoles = $this->AuthAcl->authRoles($authAcl->role_id);

        $this->set(compact('authAcl', 'roles', 'authRoles'));
        $this->set('_serialize', ['authAcl']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Auth Acl id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $authAcl = $this->AuthAcl->get($id);
        if ($this->AuthAcl->delete($authAcl)) {
            $this->Flash->success(__('The auth acl has been deleted.'));
        } else {
            $this->Flash->error(__('The auth acl could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
