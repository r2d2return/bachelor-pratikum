var mqtt;
        var reconnectTimeout = 2000;
        var mqttIsConnected = false;
    
        function MQTTconnect() {
            if (typeof path == "undefined") {
                //path = '/mqtt';
                path = '';
            }
            mqtt = new Paho.MQTT.Client(
                    host,
                    port,
                    path,
                    "webDB_" + parseInt(Math.random() * 1000, 10)
            );
            var options = {
            timeout: 3,
            useSSL: useTLS,
            cleanSession: cleansession,
            onSuccess: onConnect,
            onFailure: function (message) {
                $('#status').text("Connection failed: " + message.errorMessage + "Retrying");
                setTimeout(MQTTconnect, reconnectTimeout);
                }
            };
        
            mqtt.onConnectionLost = onConnectionLost;
            mqtt.onMessageArrived = onMessageArrived;
        
            if (username != null) {
                options.userName = username;
                options.password = password;
            }
            
            console.log("Host="+ host + ", port=" + port + ", path=" + path + " TLS = " + useTLS + " username=" + username + " password=" + password);
            mqtt.connect(options);
        }
    
        function onConnect() {
            $('#status').text('Connected to ' + host + ':' + port + path);
            // Connection succeeded; subscribe to our topic
            mqtt.subscribe(topic, {qos: 0});
            $('#topic').text(topic);
            mqttIsConnected = true;
            initializeBackgrounds();
        }
    
        function onConnectionLost(responseObject) {
            setTimeout(MQTTconnect, reconnectTimeout);
            $('#status').text("connection lost: " + responseObject.errorMessage + ". Reconnecting");
        };
        
        function sendcmdOpen(nodeId, gwid) {
           sendcmd(nodeId,"open", gwid);
        }
        
        function sendcmdClose(nodeId, gwid) {
           sendcmd(nodeId,"close", gwid);
        }
        
        function sendcmdRefresh(nodeId, gwid) {
           sendcmd(nodeId, "getState", gwid);
        }

        function sendcmdReboot(nodeId, gwid) {
               sendcmd(nodeId,"reboot", gwid);
            }
        
        function sendcmdRecalibrate(nodeId, gwid) {
           sendcmd(nodeId,"recalibrate", gwid);
        }
        
        function sendcmd(nodeId, cmd, gwId) {
           try {
               mqtt.send("parking/"+gwId+"/"+nodeId+"/"+cmd, String(cmd), qos=0);
           } catch(err) {
               alert(err.message);
           }
        }
        
        function onMessageArrived(message) {
        
            var topic = message.destinationName;
            var splitTopic = topic.split("/");
            var payload = message.payloadString;
            
            $('#ws').prepend('<li>' + topic + ' = ' + payload + '</li>');
            
            if (splitTopic[2] == "state" ) {
               var divId = "#parkinglotItem" + splitTopic[1];
               var stateId = "#state" + splitTopic[1];
               var errorMessageId = "#errorMessage" + splitTopic[1];
               if (splitTopic[3] == "F") {
                      $(stateId).text("free");
                      $(divId).css("background-color","lightgreen");
                      $(errorMessageId).text(payload);
                      $(stateId).show();
                } else if (splitTopic[3] == "B") {
                  $(stateId).text("blocked");
                  $(divId).css("background-color","rgb(255,102,102)");
                  $(errorMessageId).text(payload);
                  $(stateId).show();
                } else if (splitTopic[3] == "E") {
                  $(stateId).text("Error");
                  $(divId).css("background-color","blue");
                  //$(divId).effect("pulsate", {times: 10}, 2000);
                  $(errorMessageId).text(payload);
                } else if (splitTopic[3] == "P") {
                  $(stateId).text("car present");
                  $(divId).css("background-color","orange");
                  //$(divId).effect("pulsate", {times: 10}, 2000);
                  $(errorMessageId).text(payload);
            } else if (splitTopic[3] == "D") {
          $(stateId).text("freeing");
                  $(divId).css("background-color","yellow");
          $(errorMessageId).text(payload);
        } else if (splitTopic[3] == "U") {
          $(stateId).text("blocking");
                  $(divId).css("background-color","rgb(255,204,255)");
                  $(errorMessageId).text(payload);
               } else {
                    $(stateId).text("unknown");
                    $(divId).css("background-color","lightgrey");
                }
            };       
        }