package com.tud.kom.parkinglot.activities.overview;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.support.annotation.NonNull;

import com.tud.kom.parkinglot.SavedPreferences;

/**
 * A factory that creates a new {@link OverviewViewModel} with the saved username and password.
 */
public class OverviewViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private String uname, pwd;


    /**
     * @param c The context of which the SavedPreferences "username" and "password" are requested
     */
    public OverviewViewModelFactory(Context c) {
        this.uname = SavedPreferences.getPreference(c, SavedPreferences.Filename.USERNAME);
        this.pwd = SavedPreferences.getPreference(c, SavedPreferences.Filename.PASSWORD);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new OverviewViewModel(uname, pwd);
    }
}
