<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Gateway Entity
 *
 * @property int $id
 * @property string $label
 * @property string $fqdn
 * @property int $site_id
 *
 * @property \App\Model\Entity\Site $site
 * @property \App\Model\Entity\IssuedComand[] $issued_comands
 * @property \App\Model\Entity\Parkinglot[] $parkinglots
 */
class Gateway extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
