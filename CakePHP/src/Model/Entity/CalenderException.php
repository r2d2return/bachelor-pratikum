<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CalenderException Entity
 *
 * @property int $id
 * @property int $res_id
 * @property string $isScheduled
 * @property string $isCancelled
 * @property \Cake\I18n\Time $start
 * @property \Cake\I18n\Time $end
 * @property string $isFullDayReservation
 * @property \Cake\I18n\Time $createdDate
 *
 * @property \App\Model\Entity\Re $re
 */
class CalenderException extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
