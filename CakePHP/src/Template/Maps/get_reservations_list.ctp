<div class="row">
    <div class="col-lg-12">
        <div id="md-sub-menu">
            <ul>
                <li><a class="modal-status" data-reservation-parking-id="<?php echo $parkingId; ?>">Status</a></li>
                <li><a class="modal-reservation active" data-reservation-parking-id="<?php echo $parkingId; ?>">Reservation</a></li>
                <li><a class="modal-reservation-request" data-reservation-parking-id="<?php echo $parkingId; ?>">Reservation Request</a></li>
             </ul>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>From</th>
                                <th>Reserved For</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $counter = 1; ?>
                            <?php foreach($reservationList as $reservation):
                           // print_r($reservation);exit;?>
                            <tr role="row" class="gradeA <?php echo ($counter%2 == 0 ?  'odd': 'even'); ?>">
                            <td class="sorting_1"><?php echo $reservation['start']; ?></td>
                            <td><?php echo $reservation['Visitors']['name']; ?></td>
                            <td class="actions center">
                                <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'), 'data-reservation-id' => $reservation['id'], 'data-parking-id' => $parkingId, 'class' => 'edit-reservation']) ?>
                                <?= $this->Html->image('delete.png', [ 'alt' => __('Delete'), 'data-reservation-id' => $reservation['id'], 'data-parking-id' => $parkingId, 'class' => 'delete-reservation' ]) ?>
                            </td>
                        </tr>
                        <?php $counter++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div>
                    <a class="reservation-add-modal btn btn-default">Reservation Add</a>
                </div>
            </div>
        </div>
    </div>
</div>