package com.tud.kom.parkinglot.notification.location;

import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.tud.kom.parkinglot.network.Response;
import com.tud.kom.parkinglot.utils.ExternalFileUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowService;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
public class LocationJobServiceTest {
    @Mock
    private GoogleApiClient mApiClient;
    @Mock
    private FusedLocationProviderApi mLocationProviderApi;
    private LocationJobService mService;
    private ShadowService mShadowService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mService = Robolectric.setupService(LocationJobService.class);
        mService.mGoogleApiClient = mApiClient;
        mService.mFusedLocationApi = mLocationProviderApi;
        mShadowService = shadowOf(mService);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testArrived_false() {
        Response response = new Response(200, "", ExternalFileUtils.getFileFromPath(this, "reservations.json"));
        mService.handleWebsiteResponse(response);

        final Location test = new Location("Test");
        test.setLatitude(100);
        test.setLongitude(100);
        assertFalse(mService.arrived(test));
    }

    @Test
    public void testArrived_true() {
        Response response = new Response(200, "", ExternalFileUtils.getFileFromPath(this, "reservations.json"));
        mService.handleWebsiteResponse(response);

        final Location test = new Location("Test");
        test.setLatitude(1);
        test.setLongitude(1);
        assertTrue(mService.arrived(test));
    }

    @Test
    public void testEndOfReservationTime_false() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, 1);

        String jsonReservations = ExternalFileUtils.getFileFromPath(this, "reservations.json");
        final String formattedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(calendar.getTime());
        jsonReservations = jsonReservations.replace("2018-02-23T14:13:08", formattedDate);

        Response response = new Response(200, "", jsonReservations);
        mService.handleWebsiteResponse(response);

        assertFalse(mService.endOfReservationTime());
    }

    @Test
    public void testEndOfReservationTime_true() {
        Response response = new Response(200, "", ExternalFileUtils.getFileFromPath(this, "reservations.json"));
        mService.handleWebsiteResponse(response);

        assertTrue(mService.endOfReservationTime());
    }

    @Test
    public void testHandleWebsiteResponse_jsonException() {
        Response response = new Response(200, "", "{[]æsdw");
        mService.handleWebsiteResponse(response);

        assertTrue(mShadowService.isStoppedBySelf());
    }

    @Test
    public void testHandleWebsiteResponse_responscodeNot200() {
        Response response = new Response(400, "", ExternalFileUtils.getFileFromPath(this, "reservations.json"));
        mService.handleWebsiteResponse(response);

        assertTrue(mShadowService.isStoppedBySelf());
    }

    @Test
    public void testHandleWebsiteResponse_success() {
        Response response = new Response(200, "", ExternalFileUtils.getFileFromPath(this, "reservations.json"));
        mService.handleWebsiteResponse(response);

        verify(mApiClient).connect();
    }

    @Test
    public void testOnConnected_coarseLocation() {
        mShadowService.grantPermissions(ACCESS_COARSE_LOCATION);
        mService.onConnected(new Bundle());
        verify(mLocationProviderApi).requestLocationUpdates(any(), any(), (LocationListener) any());
    }

    @Test
    public void testOnConnected_fineLocation() {
        mShadowService.grantPermissions(ACCESS_FINE_LOCATION);
        mService.onConnected(new Bundle());
        verify(mLocationProviderApi).requestLocationUpdates(any(), any(), (LocationListener) any());
    }

    @Test
    public void testOnConnected_noPermissionGranted() {
        mService.onConnected(new Bundle());
        assertTrue(mShadowService.isStoppedBySelf());
    }

    @Test
    public void testOnConnectionFailed() {
        mService.onConnectionFailed(new ConnectionResult(0));
        assertTrue(mShadowService.isStoppedBySelf());
    }

    @Test
    public void testOnCreate_noPermissionGranted() {
        mService.onCreate();
        assertTrue(mShadowService.isStoppedBySelf());
    }

    @Ignore
    @Test
    public void testOnDestroy() {
        mService.onDestroy();
        verify(mLocationProviderApi).removeLocationUpdates(any(), (LocationListener) any());
        assertTrue(mShadowService.isForegroundStopped());
    }

    @Test
    public void testOnLocationChanged_arrived() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, 1);

        String jsonReservations = ExternalFileUtils.getFileFromPath(this, "reservations.json");
        final String formattedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(calendar.getTime());
        jsonReservations = jsonReservations.replace("2018-02-23T14:13:08", formattedDate);

        Response response = new Response(200, "", jsonReservations);
        mService.handleWebsiteResponse(response);

        final Location test = new Location("Test");
        test.setLongitude(1);
        test.setLatitude(1);

        mService.onLocationChanged(test);

        verify(mLocationProviderApi).removeLocationUpdates(any(), (LocationListener) any());
        assertTrue(mShadowService.isStoppedBySelf());
    }

    @Test
    public void testOnLocationChanged_endOfReservationTime() {
        Response response = new Response(200, "", ExternalFileUtils.getFileFromPath(this, "reservations.json"));
        mService.handleWebsiteResponse(response);

        final Location test = new Location("Test");

        mService.onLocationChanged(test);

        verify(mLocationProviderApi).removeLocationUpdates(any(), (LocationListener) any());
        assertTrue(mShadowService.isStoppedBySelf());
    }
}