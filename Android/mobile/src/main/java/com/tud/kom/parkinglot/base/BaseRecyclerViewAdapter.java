package com.tud.kom.parkinglot.base;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView.Adapter;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerViewAdapter<T, VH extends BaseViewHolder<T>> extends Adapter<VH> {

    protected List<T> mList = new ArrayList<>();
    protected OnItemClickedListener<T> mListener;
    private BaseDiffUtilsCallback<T> mDiffUtilCallback;

    public BaseRecyclerViewAdapter(BaseDiffUtilsCallback<T> diffUtilCallback, final OnItemClickedListener<T> listener) {
        mDiffUtilCallback = diffUtilCallback;
        mListener = listener;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onBindViewHolder(final VH holder, final int position) {
        holder.bind(mList.get(position));
    }

    public void setData(List<T> data) {
        mDiffUtilCallback.setListOld(mList);
        mDiffUtilCallback.setListNew(data);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(mDiffUtilCallback);
        mList = data;
        diffResult.dispatchUpdatesTo(this);
    }

    public interface OnItemClickedListener<T> {

        void onDeleteClicked(T item);

        void onItemClicked(T item);
    }
}
