<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DatabaseLogsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DatabaseLogsTable Test Case
 */
class DatabaseLogsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DatabaseLogsTable
     */
    public $DatabaseLogs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.database_logs',
        'app.users',
        'app.tenants',
        'app.tenants_users',
        'app.roles',
        'app.auth_acl',
        'app.users_roles',
        'app.zones',
        'app.parkinglots',
        'app.gateways',
        'app.sites',
        'app.parkinglots_sites',
        'app.zones_sites',
        'app.issued_comands',
        'app.remote_keys',
        'app.reservations',
        'app.sensorlogs',
        'app.parkinglots_zones',
        'app.users_zones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DatabaseLogs') ? [] : ['className' => DatabaseLogsTable::class];
        $this->DatabaseLogs = TableRegistry::get('DatabaseLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DatabaseLogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
