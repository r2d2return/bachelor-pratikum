package com.tud.kom.parkinglot;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import com.tud.kom.parkinglot.activities.overview.OverviewViewModel;
import com.tud.kom.parkinglot.model.Reservation;
import com.tud.kom.parkinglot.network.Response;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.LinkedList;
import java.util.List;

public class OverviewViewModelTest {

    @Rule
    public InstantTaskExecutorRule mRule = new InstantTaskExecutorRule();

    @Mock
    private Observer<List<Reservation>> dataObserver;

    @Mock
    private Observer<String> errorObserver;

    @Mock
    private Observer<Boolean> loadingObserver;

    private OverviewViewModel mOverviewViewModel;

    @Mock
    private Observer<Boolean> waitForNetworkObserver;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);

        mOverviewViewModel = new OverviewViewModel("app", "app");
        mOverviewViewModel.getError().observeForever(errorObserver);
        mOverviewViewModel.getData().observeForever(dataObserver);
        mOverviewViewModel.isLoading().observeForever(loadingObserver);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testEmptyReservations() {
        Response r = new Response(200, "OK", "[]");
        mOverviewViewModel.handleWebsiteResponse(r);
        Mockito.verify(dataObserver).onChanged(new LinkedList<>());
    }

    @Test
    public void testError401() {
        Response r = new Response(401, "OK", null);
        mOverviewViewModel.handleWebsiteResponse(r);
        Mockito.verify(errorObserver).onChanged("ErrorRespCodeNotAsExpected: " + r.getRespCode());
    }

    @Test
    public void testInvalidJsonResponse() {
        Response r = new Response(200, "OK", "{\n" + "    \"falsch!!!\": []\n" + "}");
        mOverviewViewModel.handleWebsiteResponse(r);
        Mockito.verify(errorObserver).onChanged("A JSONArray text must start with '[' at 1 [character 2 line 1]");
    }

    @Test
    public void testMalformedJsonResponse() {
        Response r = new Response(200, "OK", "[{\n" + "            \"falsch]");
        mOverviewViewModel.handleWebsiteResponse(r);
        Mockito.verify(errorObserver).onChanged("Unterminated string at 23 [character 20 line 2]");
    }

    @Test
    public void testNullReservations() {
        Response r = new Response(200, "OK", null);
        mOverviewViewModel.handleWebsiteResponse(r);
        Mockito.verify(errorObserver).onChanged("ErrorRespBodyIsNull");
    }

    @Test
    public void testValidJsonResponse() {
        Response r = new Response(200, "OK", "{  \n" + "   \"reservations\":[  \n" + "      {  \n" + "         \"id\":39,\n" + "         \"token\":\"e39b96b7be3d5c89fc9011432e85cc49\",\n" + "         \"start\":\"2018-04-02T08:00:00+02:00\",\n" + "         \"end\":\"2018-04-06T18:00:00+02:00\",\n" + "         \"parkinglot_id\":1,\n" + "         \"users_id\":57,\n" + "         \"visitedPersonId\":57,\n" + "         \"licencePlate\":\"TE-ST 7\",\n" + "         \"reservationState\":\"RESERVED\",\n" + "         \"messageInfo\":\"GENERIC_MESSAGE\",\n" + "         \"notifiedOnDevice\":false,\n" + "         \"isRecurring\":null,\n" + "         \"recurringID\":null,\n" + "         \"created\":\"2018-02-22T16:41:31+01:00\",\n" + "         \"parkinglot\":{  \n" + "            \"id\":1,\n" + "            \"pid\":13,\n" + "            \"description\":\"S3|19 P7\",\n" + "            \"mqttchannel\":\"parking\\/1\\/13\",\n" + "            \"gateway_id\":1,\n" + "            \"status\":\"blocked\",\n" + "            \"ip\":\"192.168.10.51\",\n" + "            \"hwid\":\"00-aa-bb-cc-de-02\",\n" + "            \"lastStateChange\":\"2018-03-04T16:45:42+01:00\",\n" + "            \"type\":\"\",\n" + "            \"locationId\":1,\n" + "            \"sites\":[  \n" + "               {  \n" + "                  \"id\":5,\n" + "                  \"name\":\"KOM_Smart_App1\",\n" + "                  \"description\":null,\n" + "                  \"prefix\":null,\n" + "                  \"locationID\":1,\n" + "                  \"isPublic\":\"NO\",\n" + "                  \"publicStart\":null,\n" + "                  \"publicEnd\":null,\n" + "                  \"isPublicRecurring\":null,\n" + "                  \"recurringId\":null,\n" + "                  \"_joinData\":{  \n" + "                     \"id\":1,\n" + "                     \"sitesId\":5,\n" + "                     \"parkingLotsId\":1\n" + "                  }\n" + "               }\n" + "            ],\n" + "            \"location\":{  \n" + "               \"id\":1,\n" + "               \"description\":\"parking\\/13\",\n" + "               \"type\":\"SQARE\",\n" + "               \"upperLeftLongitude\":49.874404,\n" + "               \"upperLeftLatitude\":8.66052,\n" + "               \"upperRightLongitude\":49.874395,\n" + "               \"upperRightLatitude\":8.66046,\n" + "               \"lowerLeftLongitude\":49.874376,\n" + "               \"lowerLeftLatitude\":8.660467,\n" + "               \"lowerRightLongitude\":49.874386,\n" + "               \"lowerRightLatitude\":8.660526,\n" + "               \"width\":null,\n" + "               \"length\":null,\n" + "               \"imagePath\":\"NULL\"\n" + "            }\n" + "         },\n" + "         \"user\":{  \n" + "            \"id\":57,\n" + "            \"username\":\"app\",\n" + "            \"created\":\"2018-02-22T16:09:30+01:00\",\n" + "            \"modified\":\"2018-02-22T16:09:30+01:00\",\n" + "            \"email\":\"app@app.de\",\n" + "            \"mobileNumber\":\"\",\n" + "            \"landlineNumber\":\"\",\n" + "            \"firstname\":\"Android\",\n" + "            \"lastname\":\"App\",\n" + "            \"title\":\"\",\n" + "            \"gender\":\"other\",\n" + "            \"userState\":\"ACTIVE\",\n" + "            \"company\":\"\",\n" + "            \"department\":\"\",\n" + "            \"street\":\"\",\n" + "            \"ZIP\":\"\",\n" + "            \"zones\":[  \n" + "               {  \n" + "                  \"id\":1,\n" + "                  \"name\":\"kom\",\n" + "                  \"description\":\"KOM Parking Zone P5-P7\",\n" + "                  \"_joinData\":{  \n" + "                     \"id\":9,\n" + "                     \"user_id\":57,\n" + "                     \"zone_id\":1,\n" + "                     \"keyName\":null,\n" + "                     \"start\":null,\n" + "                     \"end\":null\n" + "                  }\n" + "               }\n" + "            ]\n" + "         }\n" + "      }\n" + "   ]\n" + "}");
        mOverviewViewModel.handleWebsiteResponse(r);
        //TODO add an assertion
        /*SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.GERMAN);
        Reservation reservation = new Reservation(format.parse("2018-04-01T12:45:00+02:00"), format.parse("2018-04-01T17:15:00+02:00"), new ParkingLot("S3|19 P7",
                "KOM \\/ Dez. V", new LatLng(49.874404,
                8.66052)), false, "3b7624d2416973ae9ccb70db97f4f3cd");
        List<Reservation> list = new LinkedList<Reservation>();
        list.add(reservation);
        Mockito.verify(dataObserver).onChanged(list);*/
    }

}