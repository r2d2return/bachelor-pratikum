<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Site'), ['action' => 'edit', $site->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Site'), ['action' => 'delete', $site->id], ['confirm' => __('Are you sure you want to delete # {0}?', $site->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sites'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Site'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="sites view large-10 medium-9 columns">
    <h2><?= h($site->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($site->name) ?></p>
            <h6 class="subheader"><?= __('Imagepath') ?></h6>
            <p><?= h($site->imagepath) ?></p>
            <h6 class="subheader"><?= __('Coordinates') ?></h6>
            <p><?= h($site->coordinates) ?></p>
            <h6 class="subheader"><?= __('Prefix') ?></h6>
            <p><?= h($site->prefix) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($site->id) ?></p>
        </div>
    </div>
</div>
<?php $this->Html->addCrumb('Sites', '/sites'); ?>
<?php $this->Html->addCrumb('Details', ['controller' => 'Sites', 'action' => 'view']); ?>