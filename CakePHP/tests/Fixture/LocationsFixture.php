<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LocationsFixture
 *
 */
class LocationsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    /*
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'description' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'type' => ['type' => 'string', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'upperLeftLongitude' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'upperLeftLatitude' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'upperRightLongitude' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'upperRightLatitude' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'lowerLeftLongitude' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'lowerLeftLatitude' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'lowerRightLongitude' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'lowerRightLatitude' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'width' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'length' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'imagePath' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];*/
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'description' => 'Lorem ipsum dolor sit amet',
            'type' => 'CIRCLE',
            'upperLeftLongitude' => 1,
            'upperLeftLatitude' => 1,
            'upperRightLongitude' => 1,
            'upperRightLatitude' => 1,
            'lowerLeftLongitude' => 1,
            'lowerLeftLatitude' => 1,
            'lowerRightLongitude' => 1,
            'lowerRightLatitude' => 1,
            'width' => 1,
            'length' => 1,
            'imagePath' => 'Lorem ipsum dolor sit amet'
        ],

        [
            'id' => 2,
            'description' => 'Lorem ipsum dolor sit amet',
            'type' => 'CIRCLE',
            'upperLeftLongitude' => 1,
            'upperLeftLatitude' => 1,
            'upperRightLongitude' => 1,
            'upperRightLatitude' => 1,
            'lowerLeftLongitude' => 1,
            'lowerLeftLatitude' => 1,
            'lowerRightLongitude' => 1,
            'lowerRightLatitude' => 1,
            'width' => 1,
            'length' => 1,
            'imagePath' => 'Lorem ipsum dolor sit amet'
        ],

        [
            'id' => 3,
            'description' => 'Lorem ipsum dolor sit amet',
            'type' => 'CIRCLE',
            'upperLeftLongitude' => 1,
            'upperLeftLatitude' => 1,
            'upperRightLongitude' => 1,
            'upperRightLatitude' => 1,
            'lowerLeftLongitude' => 1,
            'lowerLeftLatitude' => 1,
            'lowerRightLongitude' => 1,
            'lowerRightLatitude' => 1,
            'width' => 1,
            'length' => 1,
            'imagePath' => 'Lorem ipsum dolor sit amet'
        ],


        [
            'id' => 4,
            'description' => 'Lorem ipsum dolor sit amet',
            'type' => 'CIRCLE',
            'upperLeftLongitude' => 1,
            'upperLeftLatitude' => 1,
            'upperRightLongitude' => 1,
            'upperRightLatitude' => 1,
            'lowerLeftLongitude' => 1,
            'lowerLeftLatitude' => 1,
            'lowerRightLongitude' => 1,
            'lowerRightLatitude' => 1,
            'width' => 1,
            'length' => 1,
            'imagePath' => 'Lorem ipsum dolor sit amet'
        ],

        [
            'id' => 5,
            'description' => 'Lorem ipsum dolor sit amet',
            'type' => 'CIRCLE',
            'upperLeftLongitude' => 1,
            'upperLeftLatitude' => 1,
            'upperRightLongitude' => 1,
            'upperRightLatitude' => 1,
            'lowerLeftLongitude' => 1,
            'lowerLeftLatitude' => 1,
            'lowerRightLongitude' => 1,
            'lowerRightLatitude' => 1,
            'width' => 1,
            'length' => 1,
            'imagePath' => 'Lorem ipsum dolor sit amet'
        ]
    ];
}
