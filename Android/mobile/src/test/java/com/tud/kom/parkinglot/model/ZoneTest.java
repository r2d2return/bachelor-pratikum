package com.tud.kom.parkinglot.model;

import org.junit.Assert;
import org.junit.Test;

public class ZoneTest {
    @Test
    public void testDefaultIsNull() {
        Zone s = new Zone();
        Assert.assertEquals(0, s.getId());
        Assert.assertNull(s.getName());
    }

    @Test
    public void testToString() {
        Assert.assertNotNull(new Zone().toString());
    }
}
