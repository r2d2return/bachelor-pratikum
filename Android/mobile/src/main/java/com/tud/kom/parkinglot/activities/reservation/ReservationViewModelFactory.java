package com.tud.kom.parkinglot.activities.reservation;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.Factory;
import android.content.Context;
import android.support.annotation.NonNull;

import com.tud.kom.parkinglot.SavedPreferences;

class ReservationViewModelFactory implements Factory {

    private final String password;

    private final String username;

    public ReservationViewModelFactory(Context c) {
        this.username = SavedPreferences.getPreference(c, SavedPreferences.Filename.USERNAME);
        this.password = SavedPreferences.getPreference(c, SavedPreferences.Filename.PASSWORD);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull final Class<T> modelClass) {
        return (T) new ReservationViewModel(username, password);
    }
}
