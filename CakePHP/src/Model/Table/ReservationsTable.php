<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Reservations Model
 *
 * @property \App\Model\Table\ParkinglotsTable|\Cake\ORM\Association\BelongsTo $Parkinglots
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Reservation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Reservation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Reservation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Reservation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Reservation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Reservation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Reservation findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReservationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('reservations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Parkinglots', [
            'foreignKey' => 'parkinglot_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'users_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('token', 'create')
            ->notEmpty('token');

        $validator
            ->dateTime('start')
            ->requirePresence('start', 'create')
            ->notEmpty('start');

        $validator
            ->dateTime('end')
            ->requirePresence('end', 'create')
            ->notEmpty('end');

        $validator
            ->integer('visitedPersonId')
            ->requirePresence('visitedPersonId', 'create')
            ->notEmpty('visitedPersonId');

        $validator
            ->requirePresence('licencePlate', 'create')
            ->notEmpty('licencePlate');

        $validator
            ->allowEmpty('reservationState');

        $validator
            ->allowEmpty('messageInfo');

        $validator
            ->allowEmpty('isRecurring');

        $validator
            ->integer('recurringID')
            ->allowEmpty('recurringID');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parkinglot_id'], 'Parkinglots'));
        $rules->add($rules->existsIn(['users_id'], 'Users'));

        return $rules;
    }
    public function ReservationParkinglotZone($userId) {
        $zone_id = $this->Users->zoneId($userId);
        $parkinglotZone = $this->Parkinglots->find('list', [
                    'limit' => 200,
                    'keyField' => 'id',
                    'valueField' => 'description'
                    ])->matching('Sites.Zones', function ($q) use ($zone_id) {
                            return $q->where(['Zones.id' => $zone_id]);
                            });
        return $parkinglotZone;
    }
    public function AdminParkinglotZone($userId) {
        $parkinglotZone = $this->Parkinglots->find('list', [
                    'limit' => 200,
                    'keyField' => 'id',
                    'valueField' => 'description'
                    ]);
        return $parkinglotZone;
    }
}
