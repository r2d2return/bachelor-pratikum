package com.tud.kom.parkinglot.activities.overview;

import com.tud.kom.parkinglot.base.BaseDiffUtilsCallback;
import com.tud.kom.parkinglot.model.Reservation;

public class ReservationDiffUtilsCallback extends BaseDiffUtilsCallback<Reservation> {
    @Override
    public boolean areContentsTheSame(final int oldItemPosition, final int newItemPosition) {
        return mListOld.get(oldItemPosition).equals(mListNew.get(newItemPosition));
    }

    @Override
    public boolean areItemsTheSame(final int oldItemPosition, final int newItemPosition) {
        return mListOld.get(oldItemPosition).getId() == mListNew.get(newItemPosition).getId();
    }
}