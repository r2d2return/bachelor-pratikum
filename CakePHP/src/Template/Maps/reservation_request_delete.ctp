<div class="row">
    <div class="col-lg-12">
        <div id="md-sub-menu">
            <ul>
                <li><a class="modal-status" data-reservation-parking-id="<?php echo $parkingId; ?>">Status</a></li>
            <li><a class="modal-reservation" data-reservation-parking-id="<?php echo $parkingId; ?>">Reservation</a></li>
        <li><a class="modal-reservation-request active" data-reservation-parking-id="<?php echo $parkingId; ?>">Reservation Request</a></li>
</ul>
        </div>
<div class="panel panel-default">
    <div class="panel-body">
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr>
                    <th>From</th>
                    <th>Requested By</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php $counter = 1; ?>
                <?php foreach($reservationRequests as $reservationRequest):
                            //    print_r($reservationRequest);exit;?>
                <tr role="row" class="gradeA <?php echo ($counter%2 == 0 ?  'odd': 'even'); ?>">
                <td class="sorting_1"><?php echo $reservationRequest['start']; ?></td>
                <td><?php echo $reservationRequest['firstname']." ".$reservationRequest['lastname']; ?></td>
                <td class="actions center">
                    <?= $this->Html->image('check.png', [ 'alt' => __('Accept'), 'data-reservation-request-id' => $reservationRequest['id'], 'data-parking-id' => $parkingId, 'class' => 'reservation-request-add-reservation']) ?>
                    <?= $this->Html->image('delete.png', [ 'alt' => __('Delete'), 'data-reservation-request-id' => $reservationRequest['id'], 'data-parking-id' => $parkingId, 'class' => 'delete-reservation-request' ]) ?>
                </td>
            </tr>
            <?php $counter++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div>
        <a class="reservation-request-add-modal btn btn-default">Reservation Request</a>
    </div>
</div>