package com.tud.kom.parkinglot.notification.location;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.model.Reservation;
import com.tud.kom.parkinglot.model.parser.ReservationParser;
import com.tud.kom.parkinglot.network.NetworkCallback;
import com.tud.kom.parkinglot.network.RequestAbstraction;
import com.tud.kom.parkinglot.network.Response;
import com.tud.kom.parkinglot.notification.LocationServiceHelper;
import com.tud.kom.parkinglot.notification.ParkingLotNotification;

import org.json.JSONException;

import java.text.ParseException;
import java.util.Date;

public class LocationJobService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, NetworkCallback {
    private static final String TAG = LocationJobService.class.getSimpleName();
    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();
    @VisibleForTesting
    public FusedLocationProviderApi mFusedLocationApi = LocationServices.FusedLocationApi;
    @VisibleForTesting
    public GoogleApiClient mGoogleApiClient;
    private Intent intent;
    private LocationRequest mLocationRequest;
    private Reservation reservation;
    private PowerManager.WakeLock wakeLock;

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(11833, buildForegroundNotification());

        Log.d(TAG, "job started");

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        wakeLock.acquire();

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "job aborted for no permission in onStartJob");
            stopSelf();
            return;
        }
        String username = SavedPreferences.getPreference(this, SavedPreferences.Filename.USERNAME);
        String password = SavedPreferences.getPreference(this, SavedPreferences.Filename.PASSWORD);
        RequestAbstraction.getReservations(this, username, password);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "job stopped");
        mFusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, this);
        String username = SavedPreferences.getPreference(this, SavedPreferences.Filename.USERNAME);
        String password = SavedPreferences.getPreference(this, SavedPreferences.Filename.PASSWORD);
        RequestAbstraction.markAsNotified(response -> markAsNotifiedCallback(username, password), reservation, username, password);
    }

    private void markAsNotifiedCallback(String username, String password) {
        LocationServiceHelper.scheduleNextAlarm(this, response2 -> {
            stopForeground(true);
            wakeLock.release();
        }, username, password);

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "new location: (lon " + location.getLatitude() + ", lat " + location.getLongitude() + ")");
        boolean arrived = arrived(location);
        if (endOfReservationTime() || arrived) {
            mFusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, this);
            if (arrived)
                ParkingLotNotification.sendNotification(this, reservation);
            stopSelf();
        }
    }

    public boolean arrived(Location currentLocation) {
        float[] distance = new float[1];
        Location.distanceBetween(currentLocation.getLatitude(), currentLocation.getLongitude(), reservation.getParkingLot().getLatLng().latitude, reservation.getParkingLot().getLatLng().longitude, distance);
        Log.d(TAG, "distance is: " + distance[0]);
        return distance[0] < 500.0f;
    }

    public boolean endOfReservationTime() {
        return new Date(System.currentTimeMillis()).after(reservation.getEndTime());
    }

    @Override
    public void handleWebsiteResponse(Response response) {
        if (response.getRespCode() == 200) {
            try {
                // parse reservation and parkinglot
                reservation = LocationServiceHelper.nextReservationForAlarm(ReservationParser.parseReservationsArray(response.getResp()));
                if (reservation == null || reservation.getStartTime().after(new Date(System.currentTimeMillis()))) {
                    Log.w(TAG, "not time to start a location service now -> stop service");
                    stopSelf();
                    return;
                }

                // prepare request
                // TODO uncomment smallestdisplacement. set priority to balanced
                mLocationRequest = LocationRequest.create().setInterval(15000).setFastestInterval(1000)/*.setSmallestDisplacement(100)*/.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                if (mGoogleApiClient == null) {
                    mGoogleApiClient = new GoogleApiClient.Builder(this).addOnConnectionFailedListener(this).addConnectionCallbacks(this).addApi(LocationServices.API).build();
                }
                this.mGoogleApiClient.connect();

            } catch (JSONException | ParseException e) {
                e.printStackTrace();
                Log.e(TAG, "json could not be parsed -> stop service");
                stopSelf();
            }
        }
        else {
            Log.w(TAG, "website response is not 200 (" + response.getRespCode() + ") -> stop service");
            stopSelf();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "ON BIND");
        this.intent = intent;
        return mBinder;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "google connected");
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "job aborted for no permission in onConnected");
            stopSelf();
            return;
        }
        mFusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "google connection failed");
        stopSelf();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private Notification buildForegroundNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("backgroundtask", "backgroundtask", NotificationManager.IMPORTANCE_MIN);
            notificationChannel.setDescription("backgroundtask");
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder b = new NotificationCompat.Builder(this, "backgroundtask").setOngoing(true).setContentTitle(getApplicationContext().getString(R.string.app_name)).setContentText(getApplicationContext().getString(R.string.foreground_service_notification_text)).setSmallIcon(R.drawable.ic_launcher);
        return b.build();
    }

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC. Needed for testing.
     */
    class LocalBinder extends Binder {
        LocationJobService getService() {
            return LocationJobService.this;
        }
    }
}
