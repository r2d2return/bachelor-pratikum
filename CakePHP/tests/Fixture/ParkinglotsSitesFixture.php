<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ParkinglotsSitesFixture
 *
 */
class ParkinglotsSitesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    /*
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'sitesId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'parkingLotsId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_sitesId' => ['type' => 'index', 'columns' => ['sitesId'], 'length' => []],
            'fk_parkingLotId' => ['type' => 'index', 'columns' => ['parkingLotsId'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_parkingLotId' => ['type' => 'foreign', 'columns' => ['parkingLotsId'], 'references' => ['parkinglots', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_sitesId' => ['type' => 'foreign', 'columns' => ['sitesId'], 'references' => ['sites', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];*/
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'sitesId' => 1,
            'parkingLotsId' => 1
        ],

        [
            'id' => 2,
            'sitesId' => 2,
            'parkingLotsId' => 2
        ],

        [
            'id' => 3,
            'sitesId' => 3,
            'parkingLotsId' => 3
        ],

        [
            'id' => 4,
            'sitesId' => 4,
            'parkingLotsId' => 4
        ],

        [
            'id' => 5,
            'sitesId' => 5,
            'parkingLotsId' => 5
        ]
    ];
}
