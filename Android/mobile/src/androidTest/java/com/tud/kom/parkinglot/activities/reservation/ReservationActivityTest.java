package com.tud.kom.parkinglot.activities.reservation;

import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.PickerActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.text.format.DateFormat;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.SavedPreferences.Filename;
import com.tud.kom.parkinglot.network.NetworkTask;
import com.tud.kom.parkinglot.utils.JsonUtils;

import org.bouncycastle.util.encoders.Base64;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static android.support.test.InstrumentationRegistry.getContext;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasTextColor;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.tud.kom.parkinglot.activities.reservation.ReservationActivity.EXTRA_PARKINGLOT_ID;
import static com.tud.kom.parkinglot.activities.reservation.ReservationActivity.EXTRA_SITE_ID;
import static com.tud.kom.parkinglot.activities.reservation.ReservationActivity.EXTRA_ZONE_ID;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.containsString;

/**
 * TODO failing tests in jacoco report, but not in manual run
 */
public class ReservationActivityTest {

    @Rule
    public IntentsTestRule<ReservationActivity> mActivityTestRule = new IntentsTestRule<ReservationActivity>(ReservationActivity.class, true, false) {
        @Override
        protected void beforeActivityLaunched() {
            SavedPreferences.savePreference(InstrumentationRegistry.getTargetContext(), Filename.USERNAME, "smart");
            SavedPreferences.savePreference(InstrumentationRegistry.getTargetContext(), Filename.PASSWORD, "parking");
            super.beforeActivityLaunched();
        }
    };

    private MockWebServer mServer;

    @Before
    public void setUp() throws Exception {
        mServer = new MockWebServer();
        mServer.start();
        NetworkTask.baseUrl = mServer.url("/").toString();

        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonZones(getContext(), "Zone1", "Zone2", "Zone3", "Zone4")));
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonSites(getContext(), "Site1", "Site2", "Site3", "Site4")));
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonParkingLots(getContext(), "P1", "P2", "P3", "P4")));
    }

    @After
    public void tearDown() throws Exception {
        mServer.shutdown();
    }

    @Test
    public void testChangeParkingLot() throws Exception {
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonSites(getContext(), "Site11", "Site22", "Site33", "Site44")));
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonParkingLots(getContext(), "P11", "P22", "P33", "P44")));
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonParkingLots(getContext(), "P111", "P222", "P333", "P444")));

        mActivityTestRule.launchActivity(new Intent());

        mServer.takeRequest();
        mServer.takeRequest();
        mServer.takeRequest();

        onView(withId(R.id.spinner_zone)).perform(click());
        onData(anything()).atPosition(2).perform(click());
        onView(withId(R.id.spinner_zone)).check(matches(withSpinnerText(containsString("Zone3"))));
        onView(withId(R.id.spinner_site)).check(matches(withSpinnerText(containsString("Site11"))));
        onView(withId(R.id.spinner_parking_lot)).check(matches(withSpinnerText(containsString("P11"))));

        onView(withId(R.id.spinner_site)).perform(click());
        onData(anything()).atPosition(1).perform(click());
        onView(withId(R.id.spinner_site)).check(matches(withSpinnerText(containsString("Site22"))));
        onView(withId(R.id.spinner_parking_lot)).check(matches(withSpinnerText(containsString("P111"))));

        onView(withId(R.id.spinner_parking_lot)).perform(click());
        onData(anything()).atPosition(3).perform(click());
        onView(withId(R.id.spinner_parking_lot)).check(matches(withSpinnerText(containsString("P444"))));
    }

    @Test
    public void testEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, 2);
        calendar.add(Calendar.MINUTE, 12);

        mActivityTestRule.launchActivity(new Intent());

        setDate(R.id.text_view_end_date, calendar);

        onView(withId(R.id.text_view_end_date)).check(matches(withText(DateFormat.getDateFormat(mActivityTestRule.getActivity().getApplicationContext()).format(calendar.getTime()))));
    }

    @Test
    public void testEndTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, 1);

        mActivityTestRule.launchActivity(new Intent());

        setTime(R.id.text_view_end_time, calendar);

        onView(withId(R.id.text_view_end_time)).check(matches(withText(DateFormat.getTimeFormat(mActivityTestRule.getActivity().getApplicationContext()).format(calendar.getTime()))));
    }

    @Test
    public void testEndTimeInvalid() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, -1);

        mActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.text_view_end_time)).check(matches(hasTextColor(R.color.reservation_time_valid)));

        setDate(R.id.text_view_end_date, calendar);
        setTime(R.id.text_view_end_time, calendar);

        onView(withId(R.id.text_view_end_time)).check(matches(withText(DateFormat.getTimeFormat(mActivityTestRule.getActivity().getApplicationContext()).format(calendar.getTime()))));
        onView(withId(R.id.text_view_end_time)).check(matches(hasTextColor(R.color.reservation_time_invalid)));
    }

    @Test
    public void testEndTimeInvalidBeforeStartTime() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, 2);

        mActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.text_view_end_time)).check(matches(hasTextColor(R.color.reservation_time_valid)));

        setDate(R.id.text_view_start_date, calendar);
        setTime(R.id.text_view_start_time, calendar);
        calendar.add(Calendar.HOUR_OF_DAY, -1);
        setDate(R.id.text_view_end_date, calendar);
        setTime(R.id.text_view_end_time, calendar);

        Thread.sleep(100);
        onView(withId(R.id.text_view_end_time)).check(matches(hasTextColor(R.color.reservation_time_invalid)));
    }

    @Test
    public void testEndTimeMovesWithStartTime() {
        mActivityTestRule.launchActivity(new Intent());

        Calendar calendar = Calendar.getInstance();
        setDate(R.id.text_view_start_date, calendar);
        setTime(R.id.text_view_start_time, calendar);
        setDate(R.id.text_view_end_date, calendar);
        setTime(R.id.text_view_end_time, calendar);

        calendar.add(Calendar.HOUR_OF_DAY, 3);

        setDate(R.id.text_view_end_date, calendar);
        setTime(R.id.text_view_end_time, calendar);

        calendar.add(Calendar.HOUR_OF_DAY, 5);
        setDate(R.id.text_view_start_date, calendar);
        setTime(R.id.text_view_start_time, calendar);

        calendar.add(Calendar.HOUR_OF_DAY, 3);
        onView(withId(R.id.text_view_end_date)).check(matches(withText(DateFormat.getDateFormat(mActivityTestRule.getActivity().getApplicationContext()).format(calendar.getTime()))));
        onView(withId(R.id.text_view_end_time)).check(matches(withText(DateFormat.getTimeFormat(mActivityTestRule.getActivity().getApplicationContext()).format(calendar.getTime()))));
    }

    @Test
    public void testOnCreate() throws Exception {
        mActivityTestRule.launchActivity(new Intent());

        RecordedRequest request = mServer.takeRequest();
        assertEquals("/zones.json", request.getPath());
        assertEquals("Basic " + Base64.toBase64String(("smart:parking").getBytes()), request.getHeader("Authorization"));

        RecordedRequest request2 = mServer.takeRequest();
        assertEquals("/sites/getSitesForZone.json?zone_id=0", request2.getPath());
        assertEquals("Basic " + Base64.toBase64String(("smart:parking").getBytes()), request2.getHeader("Authorization"));

        RecordedRequest request3 = mServer.takeRequest();
        assertEquals("/parkinglots/getParkinglotsForSite.json?site_id=0", request3.getPath());
        assertEquals("Basic " + Base64.toBase64String(("smart:parking").getBytes()), request3.getHeader("Authorization"));
    }

    @Test
    public void testPresetParkingLot() {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_ZONE_ID, 0);
        intent.putExtra(EXTRA_SITE_ID, 1);
        intent.putExtra(EXTRA_PARKINGLOT_ID, 2);

        mActivityTestRule.launchActivity(intent);

        onView(withId(R.id.spinner_zone)).check(matches(withSpinnerText(containsString("Zone1"))));
        onView(withId(R.id.spinner_site)).check(matches(withSpinnerText(containsString("Site2"))));
        onView(withId(R.id.spinner_parking_lot)).check(matches(withSpinnerText(containsString("P3"))));
    }

    @Test
    public void testReserveButton_success() throws Exception {
        mServer.enqueue(new MockResponse().setResponseCode(200));//reservation

        mActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.button_reserve)).perform(click());

        mServer.takeRequest();
        mServer.takeRequest();
        mServer.takeRequest();
        RecordedRequest request = mServer.takeRequest();
        assertEquals("/reservations/add.json", request.getPath());
        assertEquals("Basic " + Base64.toBase64String(("smart:parking").getBytes()), request.getHeader("Authorization"));
        assertTrue(request.getBody().readUtf8().endsWith("&parkinglot_id=0&reservationState=RESERVED&licencePlate=TE-ST-18&messageInfo="));

        assertTrue(mActivityTestRule.getActivity().isFinishing());
    }

    @Test
    public void testStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 2);

        mActivityTestRule.launchActivity(new Intent());

        setDate(R.id.text_view_start_date, calendar);

        onView(withId(R.id.text_view_start_date)).check(matches(withText(DateFormat.getDateFormat(mActivityTestRule.getActivity().getApplicationContext()).format(calendar.getTime()))));
    }

    @Test
    public void testStartTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, 2);
        calendar.add(Calendar.MINUTE, 12);

        mActivityTestRule.launchActivity(new Intent());

        setTime(R.id.text_view_start_time, calendar);

        onView(withId(R.id.text_view_start_time)).check(matches(withText(DateFormat.getTimeFormat(mActivityTestRule.getActivity().getApplicationContext()).format(calendar.getTime()))));
    }

    @Test
    public void testStartTimeAfterEndTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, 2);

        mActivityTestRule.launchActivity(new Intent());

        setDate(R.id.text_view_start_date, calendar);
        setTime(R.id.text_view_start_time, calendar);
        onView(withId(R.id.text_view_start_time)).check(matches(withText(DateFormat.getTimeFormat(mActivityTestRule.getActivity().getApplicationContext()).format(calendar.getTime()))));

        calendar.add(Calendar.HOUR_OF_DAY, 1);

        onView(withId(R.id.text_view_end_date)).check(matches(withText(DateFormat.getDateFormat(mActivityTestRule.getActivity().getApplicationContext()).format(calendar.getTime()))));
        onView(withId(R.id.text_view_end_time)).check(matches(withText(DateFormat.getTimeFormat(mActivityTestRule.getActivity().getApplicationContext()).format(calendar.getTime()))));
    }

    @Test
    public void testStartTimeInvalid() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, -1);

        mActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.text_view_start_time)).check(matches(hasTextColor(R.color.reservation_time_valid)));

        setDate(R.id.text_view_start_date, calendar);
        setTime(R.id.text_view_start_time, calendar);

        onView(withId(R.id.text_view_start_time)).check(matches(withText(DateFormat.getTimeFormat(mActivityTestRule.getActivity().getApplicationContext()).format(calendar.getTime()))));
        onView(withId(R.id.text_view_start_time)).check(matches(hasTextColor(R.color.reservation_time_invalid)));
    }

    private void setDate(@IdRes int id, final Calendar calendar) {
        final int year = calendar.get(Calendar.YEAR);
        final int monthOfYear = calendar.get(Calendar.MONTH) + 1;
        final int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        onView(withId(id)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(year, monthOfYear, dayOfMonth));
        onView(withId(android.R.id.button1)).perform(click());
    }

    private void setTime(@IdRes int id, final Calendar calendar) {
        final int hours = calendar.get(Calendar.HOUR_OF_DAY);
        final int minutes = calendar.get(Calendar.MINUTE);

        onView(withId(id)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(hours, minutes));
        onView(withId(android.R.id.button1)).perform(click());
    }
}