<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ReservationsController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;
/**
 * App\Controller\ReservationsController Test Case
 */
class ReservationsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.reservations',
        'app.parkinglots',
        'app.gateways',
        'app.sites',
        'app.parkinglots_sites',
        'app.zones',
        'app.users',
        'app.database_logs',
        'app.tenants',
        'app.tenants_users',
        'app.roles',
        'app.auth_acl',
        'app.users_roles',
        'app.users_zones',
        'app.zones_sites',
        'app.locations',
        'app.firebase',
        'app.failed_login_attempts',
        //'app.remote_keys',
        'app.sensorlogs',
    ];

    /** 
        CONFIGURATION
        ===============================================================================
    */
        //For these tests to work, debug HAS to be set to false in /config/app.php. If you need the debugging, comment out 'end' in the $json_return_keys variable
        //Define which keys should be checked to be included/excluded from a response -> See "Schnittstellendefinition"
        public $json_return_keys = [

            //Check if no top-level-fields according to the "Schnittstellendefinition" are retrieved
            'id',
            'start',
            'token',
            //'end',
            'parkinglot_id',
            'users_id',
            'visitedPersonId',
            'licencePlate',
            'reservationState',
            'messageInfo',
            'isRecurring',
            'recurringID',
            'created',
            
            'parkinglot',
                'id',
                'pid',
                'description',
                'mqttchannel',
                'gateway_id',
                'status',
                'ip',
                'hwid',
                'lastStateChange',
                'type',
                'locationId',
                
                'sites',
                    'id',
                    'name',
                    'description',
                    'prefix',
                    'locationID',
                    'isPublic',
                    'publicStart',
                    'publicEnd',
                    'isPublicRecurring',
                    'recurringID',

                    '_joinData',
                        'id',
                        'sitesId',
                        'parkingLotsId',

                    //'location', Conflict with return-Message "You are not authorized to access that location"
                        'id',
                        'description',
                        'type',
                        'upperLeftLongitude',
                        'upperLeftLatitude',
                        'upperRightLongitude',
                        'upperRightLatitude',
                        'lowerLeftLongitude',
                        'lowerLeftLatitude',
                        'lowerRightLongitude',
                        'lowerRightLatitude',
                        'width',
                        'length',
                        'imagePath',
            
                'user',
                    'id',
                    'username',
                    'created',
                    'modified',
                    'email',
                    'mobileNumber',
                    'landlineNumber',
                    'firstname',
                    'lastname',
                    'title',
                    'gender',
                    'userState',
                    'company',
                    'department',
                    'street',
                    'ZIP',

                    'zones',
                        'id',
                        'name',
                        'description',

                        '_joinData',
                            'id',
                            'user_id',
                            'zone_id',
                            'keyName',
                            'start',
                            //'end',

                '_matchingData'
        ];

        public $json_return_keys_admin = [

            //Check if no top-level-fields according to the "Schnittstellendefinition" are retrieved
            'id',
            'start',
            //'end',
            'parkinglot_id',
            'users_id',
            'visitedPersonId',
            'licencePlate',
            'reservationState',
            'messageInfo',
            'isRecurring',
            'recurringID',
            'created',
            
            'parkinglot',
                'id',
                'pid',
                'description',
                'mqttchannel',
                'gateway_id',
                'status',
                'ip',
                'hwid',
                'lastStateChange',
                'type',
                'locationId',
            
            'user',
                'id',
                'username',
                'created',
                'modified',
                'email',
                'mobileNumber',
                'landlineNumber',
                'firstname',
                'lastname',
                'title',
                'gender',
                'userState',
                'company',
                'department',
                'street',
                'ZIP'
        ];

        //Define which ROLES are allowed and not allowed to access the location /reservations.json
         // NOT IN USE public $authorized_roles = ['Admin'] ;//, 'Admin', 'Reservation Manager'];
         public $auth_roles_to_id = array(
            "Admin"=> 1,
            "Guest"=> 2,
            "Visitor"=>3,
            "Reservation Manager"=>4,
            "Facility Manager"=>5
         );                 
         public $unauthorized_roles = [ 'Guest',  'Facility Manager'];                   

         /*
        ======================================================================================
        */

        // HELPERS
        function doesContain($array){
            foreach ($array as $key) {
                $this->assertResponseContains($key);
            }
        }

        function doesNOTContain($array){
            foreach ($array as $key) {
                $this->assertResponseNOTContains($key);
            }
        }

    

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndexUnauthenticated()
    {

        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Not A User',
                'PHP_AUTH_PW' => 'Correct',
            ]
        ]);

        $this->get('/reservations.json');
        //debug($this->_response->body());
        $this->assertResponseError();
        $this->assertResponseCode(401);
        $this->assertResponseNotEmpty();
        $this->assertResponseContains('Unauthorized');

        //Check if no top-level-fields according to the "Schnittstellendefinition" are retrieved
         $this->doesNOTContain($this->json_return_keys);
            
        //------------------//

        $this->assertContentType('application/json');
    }

    public function testIndexUnAuthorized()
    {
        //Fixtures are chosen so that username == password == role
        //Should contain all roles that are NOT allowed to access the requested function. Thus are authorized.
        $users = $this->unauthorized_roles;

        //Send a request for every role that is allowed to make the request
        foreach ($users as $user)  {
            //debug($user);
            $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => $user,
                    'PHP_AUTH_PW' => $user,
                ]
            ]);

            $this->get('/reservations.json');
            //debug($this->_response->body());

            $this->assertResponseError();
            $this->assertResponseCode(403);
            $this->assertResponseNotEmpty();
            $this->assertResponseContains('You are not authorized to access that location.');

            //Check if no top-level-fields according to the "Schnittstellendefinition" are retrieved
            $this->doesNOTContain($this->json_return_keys);      
            //------------------//
                    
           
            
            $this->assertContentType('application/json');
        }

        if(empty($users)){
            echo("\nWARNING: No meaningfull assertions were made in ReservationsController.testGetUserUnAuthorized()! \nIf every role is authorized to access /reservations.json, ignore this message");
            $this->assertEquals(true, true);
            
        }
    }

    /**
     * Test getUser method
     * Key Assertion: Requester is given exactly his and only his information about his corresponding User
     *For further details check the documentation "Controller-Tests for Smartparking" 
     * @return void
     */
    public function testIndexAuthorizedAsAdmin()
    {
        //Fixtures are chosen so that username == password == role
        //Should contain all roles that are allowed to access the requested function. Thus are authorized.
        $users = ['Admin'];

        //Send a request for every role that is allowed to make the request
        foreach ($users as $user)  {
            //debug($user);
            $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => $user,
                    'PHP_AUTH_PW' => $user,
                ]
            ]);
            $request_time = time();
            $this->get('/reservations.json');
            //debug($this->_response->body());

            $response_body = (array) $this->_response->body();
            $result = $response_body[0];
            $result = (array) json_decode($result);

            //debug(gettype($result));
            $reservations = $result['reservations'];

            //Asserts that per User (5) every valid reservation (2 per user) is retrieved thus 10
            $this->assertEquals(10, count($reservations));

            $this->assertResponseOk();
            $this->assertResponseNotEmpty();
            $this->assertResponseCode(200);
            
            if(!empty($reservations)){

                //Check if all fields according to the "Schnittstellendefinition" are retrieved     
                $this->doesContain($this->json_return_keys_admin);
                //------------------//

                //debug(sizeof($reservations));
                //Check if all retrieved reservations are in ascending order
                $prev_time = 0;
                //debug(count($reservations));
                foreach ($reservations as $reservation) {
                    $tmp = (array) $reservation;
                    $curr_time=  strtotime($tmp["start"]);
                    $this->assertLessThanOrEqual($curr_time, $prev_time, 'The start-dates of the reservations are not sorted in ascending order');
                    $prev_time = $curr_time;
                }
                /////////////////////////////////////////////////

                //Check if all endtimes are greater or equal to the time of the request
                    
                foreach ($reservations as $reservation) {
                    $tmp = (array) $reservation;
                    $curr_time=  strtotime($tmp["end"]);
                    $this->assertGreaterThanOrEqual($request_time, $curr_time, 'At least one reservation was retrieved whos endtime was already exeeded by the time of request');
                    
                }
            }else{
                echo("\n\n\033[01;31m WARNING: \033[0m No meaningfull assertions were made in ReservationsController.testGetUserAuthorizedAsAdmin(), because no reservations are retrieved! \nPlease review the fixtures to insert a valid reservation\n\n");
            }
            
           
            
            $this->assertContentType('application/json');
        }
        
        
        if(empty($users)){
            echo("\n\n\033[01;31m WARNING: \033[0m No meaningfull assertions were made in ReservationsController.testGetUserAuthorizedAs Admin()! \nIf no role is authorized to access /reservations.json, ignore this message\n\n");
            $this->assertEquals(true, true);
            
        }
        
    }

    /**
     * Test getUser method
     * Key Assertion: Requester is given exactly his and only his information about his corresponding User
     *For further details check the documentation "Controller-Tests for Smartparking" 
     * @return void
     */
    public function testIndexAuthorizedAsNonAdmin()
    {
        //Fixtures are chosen so that username == password == role
        //Should contain all roles that are allowed to access the requested function. Thus are authorized.
        $users = ['Visitor', 'Reservation Manager']; 

        //Send a request for every role that is allowed to make the request
        foreach ($users as $user)  {
            //debug($user);
            $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => $user,
                    'PHP_AUTH_PW' => $user,
                ]
            ]);

            $request_time = time();
            $this->get('/reservations.json');
            //debug($this->_response->body());    

            $this->assertResponseOk();
            $this->assertResponseNotEmpty();
            $this->assertResponseCode(200);
            
            $response_body = (array) $this->_response->body();
            //debug("\n\n". $this->_response->body() . "\n\n");
            $result = $response_body[0];
            $result = (array) json_decode($result);

            //debug(gettype($result));
            $reservations = $result['reservations'];

            $this->assertEquals(2, count($reservations));

            if(!empty($reservations)){
                //Check if all fields according to the "Schnittstellendefinition" are retrieved     
                $this->doesContain($this->json_return_keys);

                

                //debug($reservations);
                //Check if all retrieved reservations are in ascending order
                    $prev_time = 0;
                    //debug(count($reservations));
                    foreach ($reservations as $reservation) {
                        $tmp = (array) $reservation;
                        $curr_time=  strtotime($tmp["start"]);
                        $this->assertLessThanOrEqual($curr_time, $prev_time, 'The start-dates of the reservations are not sorted in ascending order');
                        $prev_time = $curr_time;
                    }
                /////////////////////////////////////////////////

                //Check if all endtimes are greater or equal to the time of the request
                    foreach ($reservations as $reservation) {
                        $tmp = (array) $reservation;
                        $curr_time=  strtotime($tmp["end"]);
                        $this->assertGreaterThanOrEqual($request_time, $curr_time, 'At least one reservation was retrieved whos endtime was already exeeded by the time of request');
                        
                    }
                //////////////////////////////////////////////////
                
                //Check if only reservations with my user-id are retrieved
                    foreach ($reservations as $reservation) {
                        $tmp = (array) $reservation;
                        $curr_id=  $tmp['users_id'];
                        //debug($curr_id);
                        //debug($this->auth_roles_to_id["Admin"]);
                        $this->assertEquals($this->auth_roles_to_id[$user], $curr_id);                        
                    }
                //////////////////////////////////////

            }else{
                echo("\n\n\033[01;31m WARNING: \033[0m No meaningfull assertions were made in ReservationsController.testGetUserAuthorizedAsNonAdmin(), because no reservations are retrieved! \nPlease review the fixtures to insert a valid reservation\n\n");
            }
            
           
            
            $this->assertContentType('application/json');
        }
        
        
        if(empty($users)){
            echo("No meaningfull assertions were made in ReservationsController.testGetUserAuthorizedAsNonAdmin()! \nIf no role is authorized to access /reservations.json, ignore this message");
            $this->assertEquals(true, true);
            
        }
        
    }

    /**
     * Test add method with Authorization and no time clash
     *
     * @return void
     */
    public function testAddAuthorizedNoTimeClash()
    {
        //Fixtures are chosen so that username == password == role
        //Should contain all roles that are allowed to access the requested function. Thus are authorized.
        $users = ['Admin', 'Visitor', 'Reservation Manager']; 
        $reservations_added = 0;
        //Send a request for every role that is allowed to make the request
        foreach ($users as $user)  {

            //To guarantee different creation times 
            sleep(1);
            ///

            $requester_id = $this->auth_roles_to_id[$user];
            
            $this->configRequest([
                    'environment' => [
                        'PHP_AUTH_USER' => $user,
                        'PHP_AUTH_PW' => $user
                    ]
                ]);
                $post_data = [
                    //required
                    'start' => '29.01.2038+15:09',
                    'end' => '29.01.2048+15:09',
                    'parkinglot_id' => $requester_id,
                    'reservationState' => 'RESERVED',
                    'licencePlate' => 'TE-ST-2018',
                    

                    //optional
                    'messageInfo' => 'Der Test hat geklappt'
                ];
                $this->post('/reservations/add_api.json',$post_data);
                $reservations_added++;
                //debug($this->_response->body());    

                $reservations = TableRegistry::get("Reservations");
                $this->assertEquals(15+$reservations_added, count($reservations->find()->all()));

                //debug($reservations->find()->all());
                $newest_reservation = $reservations
                                        ->find()
                                        ->order(["created" => 'DESC'])
                                        ->first();

                $newest_message_info = $newest_reservation['messageInfo'];
                //debug($newest_reservation);
                $users_id = $newest_reservation['users_id'];
                $visitedPersonId = $newest_reservation['visitedPersonId'];
                //debug($newest_message_info);
                $this->assertEquals('Der Test hat geklappt', $newest_message_info);
                $this->assertResponseCode(200);
                $this->assertResponseOk();
                $this->assertEquals($users_id, $requester_id);
                $this->assertEquals($visitedPersonId, $requester_id);
                $this->assertContentType('application/json');

        }

        if(empty($users)){
            echo("No meaningfull assertions were made in ReservationsControllerTest::testAddAuthorizedNoConflict()! \nIf no role is authorized to access, ignore this message");
            $this->assertEquals(true, true);
            
        }
            
    }

    
    /**
     * Test add method with Authorization and no time clash
     *
     * @return void
     */
    public function testAddAuthorizedTimeClash()
    {
        //Fixtures are chosen so that username == password == role
        //Should contain all roles that are allowed to access the requested function. Thus are authorized.
        $users = ['Admin', 'Reservation Manager']; 
        
        //Send a request for every role that is allowed to make the request
        foreach ($users as $user)  {

            $requester_id = $this->auth_roles_to_id[$user];
            
            $this->configRequest([
                    'environment' => [
                        'PHP_AUTH_USER' => $user,
                        'PHP_AUTH_PW' => $user
                    ]
                ]);
                $post_data = [
                    //required
                    'start' => '29.01.2019+15:09', // <- these times are chosen to clash with one of the reservations in
                    'end' => '29.01.2048+15:09', // ReservationsFixture.php
                    'parkinglot_id' => 1, 
                    'reservationState' => 'RESERVED',
                    'licencePlate' => 'TE-ST-2018',
                    

                    //optional
                    'messageInfo' => 'Der Test hat geklappt'
                ];
                $this->post('/reservations/add_api.json',$post_data);
                //debug($this->_response->body());    

                $reservations = TableRegistry::get("Reservations");
                $this->assertEquals(15, count($reservations->find()->all()));

                $this->assertResponseCode(400);
                $this->assertResponseError();
                $this->assertResponseContains('"Error":"Reservierungszeiten ueberschneiden sich!"');
                $this->assertContentType('application/json');
            

        }

        if(empty($users)){
            echo("No meaningfull assertions were made in ReservationsControllerTest::testAddAuthorizedNoConflict()! \nIf no role is authorized to access, ignore this message");
            $this->assertEquals(true, true);
            
        }
            
    }

    /**
     * Test add method with Authorization but request lacks one of the required fields
     *
     * @return void
     */
    public function testAddAuthorizedMalformedRequest()
    {
        //Fixtures are chosen so that username == password == role
        //Should contain all roles that are allowed to access the requested function. Thus are authorized.
        $users = ['Admin', 'Visitor', 'Reservation Manager']; 
        
        //Send a request for every role that is allowed to make the request
        foreach ($users as $user)  {

            for($code=1; $code<6; $code++){
                $requester_id = $this->auth_roles_to_id[$user];
                
                $this->configRequest([
                        'environment' => [
                            'PHP_AUTH_USER' => $user,
                            'PHP_AUTH_PW' => $user
                        ]
                    ]);

                    if($code!=1){
                        $post_data = [
                        //required
                        
                        //'start' => '29.01.2018+15:09', 
                        'end' => '29.01.2048+15:09', 
                        'parkinglot_id' => 1, 
                        'reservationState' => 'RESERVED',
                        'licencePlate' => 'TE-ST-2018',

                        ];

                    }else if($code !=2){
                        $post_data = [
                        //required
                        
                        'start' => '29.01.2018+15:09', 
                        //'end' => '29.01.2048+15:09', 
                        'parkinglot_id' => 1, 
                        'reservationState' => 'RESERVED',
                        'licencePlate' => 'TE-ST-2018',

                        ];

                    }else if($code !=3){
                        $post_data = [
                        //required
                        
                        'start' => '29.01.2018+15:09', 
                        'end' => '29.01.2048+15:09', 
                        //'parkinglot_id' => 1, 
                        'reservationState' => 'RESERVED',
                        'licencePlate' => 'TE-ST-2018',

                        ];
                        
                    }else if($code !=4){
                        $post_data = [
                        //required
                        
                        'start' => '29.01.2018+15:09', 
                        'end' => '29.01.2048+15:09', 
                        'parkinglot_id' => 1, 
                        //'reservationState' => 'RESERVED',
                        'licencePlate' => 'TE-ST-2018',

                        ];
                        
                    }else if($code !=5){
                        $post_data = [
                        //required
                        
                        'start' => '29.01.2018+15:09', 
                        'end' => '29.01.2048+15:09', 
                        'parkinglot_id' => 1, 
                        'reservationState' => 'RESERVED',
                        //'licencePlate' => 'TE-ST-2018',
 
                        ];
                        
                    }
                    
                    $this->post('/reservations/add_api.json',$post_data);
                    //debug($this->_response->body());    

                    $reservations = TableRegistry::get("Reservations");
                    $this->assertEquals(15, count($reservations->find()->all()));

                    $this->assertResponseCode(400);
                    $this->assertResponseError();
                    $this->assertResponseContains('"Error":"Die Reservierung konnte nicht gespeichert werden, weil entscheidende Felder nicht gesetzt wurden. Bitte versuchen Sie es erneut."');
                    $this->assertContentType('application/json');

            }
            

        }

        if(empty($users)){
            echo("No meaningfull assertions were made in ReservationsControllerTest::testAddAuthorizedNoConflict()! \nIf no role is authorized to access, ignore this message");
            $this->assertEquals(true, true);
            
        }
            
    }
    /**
     * Test add method with Authorization and no time clash
     *
     * @return void
     */
    public function testAddUnAuthorized()
    {
        //Fixtures are chosen so that username == password == role
        //Should contain all roles that are allowed to access the requested function. Thus are authorized.
        $users = ['Guest', 'Facility Manager']; 
        
        //Send a request for every role that is allowed to make the request
        foreach ($users as $user)  {

            $requester_id = $this->auth_roles_to_id[$user];
            
            $this->configRequest([
                    'environment' => [
                        'PHP_AUTH_USER' => $user,
                        'PHP_AUTH_PW' => $user
                    ]
                ]);
                $post_data = [
                    //required
                    'start' => '29.01.2018+15:09', // <- these times are chosen to clash with one of the reservations in
                    'end' => '29.01.2048+15:09', // ReservationsFixture.php
                    'parkinglot_id' => 1, 
                    'reservationState' => 'RESERVED',
                    'licencePlate' => 'TE-ST-2018',
                    

                    //optional
                    'messageInfo' => 'Der Test hat geklappt'
                ];
                $this->post('/reservations/add_api.json',$post_data);
                //debug($this->_response->body());    

                $reservations = TableRegistry::get("Reservations");
                $this->assertEquals(15, count($reservations->find()->all()));

                $this->assertResponseCode(403);
                $this->assertResponseError();
                $this->assertResponseContains('You are not authorized to access that location."');
                $this->assertContentType('application/json');
            

        }

        if(empty($users)){
            echo("No meaningfull assertions were made in ReservationsControllerTest::testAddAuthorizedNoConflict()! \nIf no role is authorized to access, ignore this message");
            $this->assertEquals(true, true);
            
        }
            
    }

    /**
    * Test add with end before starttime should return an error
    *
    */
    public function testAddEndBeforeStart(){
        $this->configRequest([
                    'environment' => [
                        'PHP_AUTH_USER' => 'Visitor',
                        'PHP_AUTH_PW' => 'Visitor'
                    ]
                ]);
        $post_data = [
            //required
            'start' => '29.01.2048+15:09', 
            'end' => '29.01.2018+15:09', 
            'parkinglot_id' => 1, 
            'reservationState' => 'RESERVED',
            'licencePlate' => 'TE-ST-2018',
            

            //optional
            'messageInfo' => 'Der Test hat geklappt'
        ];
        $this->post('/reservations/add_api.json',$post_data);
        //debug($this->_response->body());    

        $reservations = TableRegistry::get("Reservations");
        $this->assertEquals(15, count($reservations->find()->all()));

        $this->assertResponseCode(400);
        $this->assertResponseError();
        $this->assertResponseContains('"Die Reservierung kann nicht gespeichert werden, weil die Startzeit nach der Endzeit liegt"');
        $this->assertContentType('application/json');
    }

    /**
    * Test add with end before starttime
    *
    */
    public function testAddReservationInThePast(){
        $this->configRequest([
                    'environment' => [
                        'PHP_AUTH_USER' => 'Visitor',
                        'PHP_AUTH_PW' => 'Visitor'
                    ]
                ]);
        $post_data = [
            //required
            'start' => '29.01.1997+15:09', 
            'end' => '30.01.1997+15:09', 
            'parkinglot_id' => 1, 
            'reservationState' => 'RESERVED',
            'licencePlate' => 'TE-ST-2018',
            

            //optional
            'messageInfo' => 'Der Test hat geklappt'
        ];
        $this->post('/reservations/add_api.json',$post_data);
        //debug($this->_response->body());    

        $reservations = TableRegistry::get("Reservations");
        $this->assertEquals(15, count($reservations->find()->all()));

        $this->assertResponseCode(400);
        $this->assertResponseError();
        $this->assertResponseContains('"Die Reservierung kann nicht gespeichert werden, weil die Endzeit der Reservierung in der Vergangenheit liegt"');
        $this->assertContentType('application/json');
    }

    /**
    * Test history method
    *
    */
    public function testHistoryAuthorizedAdmin(){
        $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => 'Admin',
                    'PHP_AUTH_PW' => 'Admin',
                ]
            ]);

        $this->get('/reservations/history.json');
        //debug($this->_response->body());
        $return_array = (array)json_decode($this->_response->body());
        $this->assertEquals(5, count($return_array['reservations']));

        $this->assertResponseOk();
        $this->assertResponseCode(200);
        $this->assertResponseNotEmpty();
        //$this->assertResponseContains('You are not authorized to access that location.');

       
        
        $this->assertContentType('application/json');


    }

    /**
    * Test history method
    *
    */
    public function testHistoryAuthorizedNonAdmin(){
        $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => 'Visitor',
                    'PHP_AUTH_PW' => 'Visitor',
                ]
            ]);

        $this->get('/reservations/history.json');
        debug($this->_response->body());
        $return_array = (array)json_decode($this->_response->body());
        $this->assertEquals(1, count($return_array['reservations']));

        $this->assertResponseOk();
        $this->assertResponseCode(200);
        $this->assertResponseNotEmpty();
        //$this->assertResponseContains('You are not authorized to access that location.');

       
        
        $this->assertContentType('application/json');


    }


    /**
    * Test Firebase Message
    *
    */
    public function testFirebaseMessage(){
        $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => 'Visitor',
                    'PHP_AUTH_PW' => 'Visitor',
                ]
            ]);

        $this->get('/reservations/sendFirebaseMessage.json');
        debug($this->_response->body());
 


        //$this->assertResponseContains('You are not authorized to access that location.');

       
        
        $this->assertContentType('application/json');
    }

    /**
    * Test Brute Force Protection 
    *
    */
    public function testBruteForceProtectionSuccessfullLoginAfterNMinus1Attempts(){
        
        if(!Configure::read('Security.BFProtectionUnit.activated')){
            $this->markTestIncomplete('To test the Brute Force Protection Unit it has to be activated in the config file');
        }

        $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => 'Visitor',
                    'PHP_AUTH_PW' => 'WRONG', // <- Wrong password on purpose
                ]
            ]);

        // Make N-1 Failed Attempts
        $allowed_attempts = Configure::read('Security.BFProtectionUnit.allowed_attempts');
        //debug('Allowed attempts: ' .$allowed_attempts);
        for($i=0; $i<$allowed_attempts-1; $i++){
            $this->get('/reservations.json');
            $this->assertResponseContains('Unauthorized');
            $this->assertResponseCode(401);
        }
        
        $this->assertResponseContains('Unauthorized');
        $this->assertResponseCode(401);

        $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => 'Visitor',
                    'PHP_AUTH_PW' => 'Visitor', // <- Correct password
                ]
            ]);

        //debug($this->_response->body());
        

        $this->get('/reservations.json');

        $this->assertResponseNotContains('"Error":"Your account is temporarily blocked due to too many login attempts. Please try again in about 5 minutes"');

        //debug($this->_response->body());
        $this->assertResponseCode(200);
        


        //$this->assertResponseContains('You are not authorized to access that location.');

       
        
        $this->assertContentType('application/json');
    }

    /**
    * Test Brute Force Protection 
    *
    */
    public function testBruteForceProtectionUnSuccessfullLoginAfterNAttempts(){
        if(!Configure::read('Security.BFProtectionUnit.activated')){
            $this->markTestSkipped('To test the Brute Force Protection Unit it has to be activated in the config file');
        }

        $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => 'Visitor',
                    'PHP_AUTH_PW' => 'WRONG', // <- Wrong password on purpose
                ]
            ]);

        // Make N Failed Attempts
        $allowed_attempts = Configure::read('Security.BFProtectionUnit.allowed_attempts');
        debug('Allowed attempts: ' .$allowed_attempts);
        for($i=0; $i<$allowed_attempts; $i++){
            $this->get('/reservations.json');
            $this->assertResponseContains('Unauthorized');
            $this->assertResponseCode(401);
        }
        
        $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => 'Visitor',
                    'PHP_AUTH_PW' => 'Visitor', // <- Correct password
                ]
            ]);


        $this->get('/reservations.json');
        
        $this->assertResponseCode(401);

        $this->assertResponseContains('"Error":"Your account is temporarily blocked due to too many login attempts. Please try again in about 5 minutes"');
        
        $this->assertContentType('application/json');
    }

    /**
    * Test Brute Force Protection 
    *
    */
    public function testBruteForceProtectionUserIsAllowedToLoginAgainAfterXMinutes(){
        if(!Configure::read('Security.BFProtectionUnit.activated')){
            $this->markTestSkipped('To test the Brute Force Protection Unit it has to be activated in the config file');
        }

        $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => 'Visitor',
                    'PHP_AUTH_PW' => 'WRONG', // <- Wrong password on purpose
                ]
            ]);

        // Make N Failed Attempts
        $allowed_attempts = Configure::read('Security.BFProtectionUnit.allowed_attempts');
        $mins_to_block = Configure::read('Security.BFProtectionUnit.mins_to_block');
        
        print("\n\n");
        $line = readline("WARNING: This test tests if the Brute-Force-Protection-Unit allows a login after the specified minutes in the configuration. On the current time is set to '. $mins_to_block .' Minutes so the test will run at least that long. Do you want to continue or skip this test?\n\n Continue? (y/n): ");

        print("\n\n");
 
        
        if(! ($line=='y')){
            $this->markTestSkipped();
            return;
        }


        debug('Allowed attempts: ' .$allowed_attempts);
        for($i=0; $i<$allowed_attempts; $i++){
            $this->get('/reservations.json');
            $this->assertResponseContains('Unauthorized');
            $this->assertResponseCode(401);
        }
  
        //Wait for x Minutes
        sleep(60*$mins_to_block);
        
        $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => 'Visitor',
                    'PHP_AUTH_PW' => 'Visitor', // <- Correct password
                ]
            ]);


        $this->get('/reservations.json');
        
        $this->assertResponseCode(200);

        $this->assertResponseNotContains('"Error":"Your account is temporarily blocked due to too many login attempts. Please try again in about 5 minutes"');
        
        $this->assertContentType('application/json');
    }

    /**
     * Test view method
     *
     * @return void
     
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    

    /**
     * Test edit method
     *
     * @return void
     
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
    */
}
