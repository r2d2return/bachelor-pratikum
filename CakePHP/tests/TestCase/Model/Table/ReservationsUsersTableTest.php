<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReservationsUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReservationsUsersTable Test Case
 */
class ReservationsUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReservationsUsersTable
     */
    public $ReservationsUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.reservations_users',
        'app.reservations',
        'app.users',
        'app.logging',
        'app.tenants',
        'app.tenants_users',
        'app.roles',
        'app.auth_acl',
        'app.users_roles',
        'app.zones',
        'app.users_zones',
        'app.sites',
        'app.gateways',
        'app.issued_comands',
        'app.parkinglots',
        'app.sensorlogs',
        'app.parkinglots_sites',
        'app.zones_sites'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReservationsUsers') ? [] : ['className' => ReservationsUsersTable::class];
        $this->ReservationsUsers = TableRegistry::get('ReservationsUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReservationsUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
