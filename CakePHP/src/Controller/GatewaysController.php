<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Gateways Controller
 *
 * @property \App\Model\Table\GatewaysTable $Gateways
 */
class GatewaysController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('gateways', $this->paginate($this->Gateways));
        $this->set('_serialize', ['gateways']);
    }

    /**
     * View method
     *
     * @param string|null $id Gateway id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gateway = $this->Gateways->get($id, [
            'contain' => []
        ]);
        $this->set('gateway', $gateway);
        $this->set('_serialize', ['gateway']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gateway = $this->Gateways->newEntity();
        if ($this->request->is('post')) {
            $gateway = $this->Gateways->patchEntity($gateway, $this->request->data);
            if ($this->Gateways->save($gateway)) {
                $this->Flash->success(__('The gateway has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The gateway could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('gateway'));
        $this->set('_serialize', ['gateway']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Gateway id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gateway = $this->Gateways->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gateway = $this->Gateways->patchEntity($gateway, $this->request->data);
            if ($this->Gateways->save($gateway)) {
                $this->Flash->success(__('The gateway has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The gateway could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('gateway'));
        $this->set('_serialize', ['gateway']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Gateway id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gateway = $this->Gateways->get($id);
        if ($this->Gateways->delete($gateway)) {
            $this->Flash->success(__('The gateway has been deleted.'));
        } else {
            $this->Flash->error(__('The gateway could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
