package com.tud.kom.parkinglot;

import com.tud.kom.parkinglot.network.Request;
import com.tud.kom.parkinglot.network.Response;

import junit.framework.Assert;

import org.junit.Test;

public class NetworkTest {
    @Test
    public void testResponse() {
        int code = 200;
        String mess = "hi", text = "ho";
        Response resp = new Response(code, mess, text);
        Assert.assertEquals(resp.getRespCode(), code);
        Assert.assertEquals(resp.getRespMess(), mess);
        Assert.assertEquals(resp.getResp(), text);
    }

    @Test
    public void testRequest() {
        Request resp = new Request("ext", Request.RequestType.GET);
        Assert.assertEquals(resp.getUrlBaseExtension(), "ext");
        Assert.assertEquals(resp.getRequestType(), Request.RequestType.GET);
        resp.addBodyParam("la", "le");
        Assert.assertEquals(resp.getBodyParams().size(), 0);
        resp.addHeaderParam("li", "lo").addHeaderParam("lu", "lx").addUrlParam("ko", "ki").addUrlParam("ki", "ku");
        Assert.assertEquals(resp.formattedBodyParams(), null);
        Assert.assertEquals(resp.formattedUrlExtWithParams(), "ext?ko=ki&ki=ku");

        resp = new Request("ext", Request.RequestType.POST);
        try {
            resp.addBodyParam("la", "le").addBodyParam("lu", "li");
        } catch (Exception e) {
            org.junit.Assert.fail();
        }
        Assert.assertEquals(resp.formattedBodyParams(), "la=le&lu=li");
    }
}
