<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
            <li class="btn btn-default"><?= $this->Html->link(__('List RemoteKeys'), ['action' => 'index']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Parking Lots'), ['controller' => 'Parkinglots', 'action' => 'index']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('New Parking Lot'), ['controller' => 'Parkinglots', 'action' => 'add']) ?></li>

        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Reservation
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <?= $this->Form->create($remoteKey) ?>
                        <fieldset>
                            <legend><?= __('Add RemoteKey') ?></legend>
                            <?php
                            $this->Form->templates([
                                'dateWidget' => '{{day}}{{month}}{{year}}<br />{{hour}}{{minute}}{{second}}{{meridian}}'
                            ]);
                                echo "<div class='form-group'>".$this->Form->input('username', ['label' => 'Keyowner Name:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('mail', ['label' => 'Mail:', 'class' => 'form-control'])."</div>";
                                echo $this->Form->input('token', ['type' => 'hidden', 'readonly' => 'readonly', 'value' => $token]);
                                echo "<div class='form-group'>".$this->Form->text('valid_from', ['label' => 'Key valid from:', 'class' => 'dtpicker form-control', 'required' => true, 'default' => date('d.m.Y H:i')])."</div>";
                                echo "<div class='form-group'>".$this->Form->text('valid_until', ['label' => 'Key valid until:', 'class' => 'dtpicker form-control', 'required' => true, 'default' => date('d.m.Y H:i', strtotime('+7 day'))])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('parkinglot_id', ['options' => $parkinglots, 'class' => 'form-control'])."</div>";
                            ?>
                        </fieldset>
                        <?= $this->Form->button('Submit',  array('class' => 'btn btn-default')); ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php $this->Html->addCrumb('Remote Keys', '/remoteKeys'); ?>
<?php $this->Html->addCrumb('Add', ['controller' => 'RemoteKeys', 'action' => 'add']); ?>