<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Reservation
            </div>
            <div class="panel-body">
                <div class="row reservation-add-submit-form">
                    <?= $this->Form->create($reservation) ?>
                    <fieldset>
                        <div class="col-lg-6">
                            <?php   $this->Form->templates([
                                                    'dateWidget' => '{{day}}{{month}}{{year}}<br />{{hour}}{{minute}}{{second}}{{meridian}}'
                                            ]);
                                echo "<div class='form-group'>".$this->Form->input('visitor.gender', array('type' => 'select',
                                'options' => array(
                                "m" => "Male",
                                "f" => "Female"),'class' => 'form-control'
                                ))."</div>";
                            ?>
                            <div class='form-group'>
                                <?php echo $this->Form->input('visitor.name', ['label' => 'Visitor Name:', 'class' => 'form-control']); ?>
                            </div>
                            <div class='form-group'>
                                <?php echo $this->Form->input('visitor.mail', ['label' => 'Visitor Mail:', 'class' => 'form-control']); ?>
                            </div>
                            <div class='form-group'>
                                <?php echo $this->Form->input('visitor.mobile', ['label' => 'Visitor Mobile:', 'class' => 'form-control']); ?>
                            </div>
                            <div class='form-group'>
                                <?php echo  $this->Form->input('token', ['type' => 'hidden', 'readonly' => 'readonly', 'value' => $token]); ?>
                            </div>
                            <?= $this->Form->button(__('Submit'),  array('class' => 'btn btn-default reservation-edit-submit')) ?>
                        </div>
                        <div class="col-lg-6">
                            <div class='form-group'>
                                <label>Start date and time:</label>
                                <?= $this->Form->text('start', ['class' => 'dtpicker form-control', 'required' => true, 'default' => date('d.m.Y H:i')]); ?>
                            </div>

                            <div class='form-group'>
                                <label>End date and time:</label>
                                <?= $this->Form->text('end', ['class' => 'dtpicker form-control', 'required' => true, 'default' => date('d.m.Y H:i', strtotime('+3 hour'))]) ;?>
                            </div>
                            <?php
                                echo "<div class='form-group'>".$this->Form->input('visitedPersonName', array('class' => 'form-control'))."</div>";
                                echo "<div class='form-group'>".$this->Form->input('parkinglot_id', array(['options' => $parkinglots], 'class' => 'form-control'))."</div>";
                            ?>
                        </div>

                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
                <?php if($this->Flash->render('success')):?>
                <div class="alert alert-success">
                    The reservation has been saved and the visitor was notified of any changes to his booking.<br>
                    <a class="modal-reservation" data-reservation-parking-id="<?php echo $reservation['parkinglot_id']; ?>">Back to reservation list</a>
                </div>
                <?php endif; ?>
                <?php if($this->Flash->render('error')):?>
                <div class="alert alert-danger">
                    The reservation could not be saved. Please, try again.<br>
                    <a class="modal-reservation" data-reservation-parking-id="<?php echo $reservation['parkinglot_id']; ?>">Back to reservation list</a>
                </div>
                <?php endif; ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
        <!-- /.row -->
