package com.tud.kom.parkinglot.activities.login;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.activities.navigation.NavigationActivity;
import com.tud.kom.parkinglot.network.NetworkTask;

import org.bouncycastle.util.encoders.Base64;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.tud.kom.parkinglot.utils.PermissionUtils.allowPermissionsIfNeeded;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {

    private static final String validPassword = "aaaaaaaa", rightName = "smart", rightPassword = "parking", wrongPassword = "test";

    @Rule
    public IntentsTestRule<LoginActivity> mActivityRule = new IntentsTestRule<LoginActivity>(LoginActivity.class, true, false) {
        @Override
        protected void beforeActivityLaunched() {
            SavedPreferences.deleteAllPreferences(InstrumentationRegistry.getTargetContext());
            super.beforeActivityLaunched();
        }
    };

    private MockWebServer mServer;

    @Before
    public void setUp() throws Exception {
        mServer = new MockWebServer();
        mServer.start();
        NetworkTask.baseUrl = mServer.url("/").toString();
    }

    @After
    public void tearDown() throws Exception {
        mServer.shutdown();
    }

    @Test
    public void testLogin_failure_rightEmail_rightPassword() {
        mServer.enqueue(new MockResponse().setResponseCode(401));
        startActivity();

        onView(withId(R.id.username)).perform(typeText(rightName));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.password)).perform(typeText(wrongPassword));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.sign_in_button)).perform(click());
    }

    @Test
    public void testLogin_failure_rightEmail_wrongPassword() {
        startActivity();
        onView(withId(R.id.username)).perform(typeText(rightName));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.sign_in_button)).perform(click());
        onView(withId(R.id.password)).check(matches(hasErrorText(mActivityRule.getActivity().getString(R.string.error_incorrect_password))));
    }

    @Test
    public void testLogin_failure_wrongEmail_rightPassword() {
        startActivity();
        onView(withId(R.id.password)).perform(typeText(validPassword));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.sign_in_button)).perform(click());
        onView(withId(R.id.username)).check(matches(hasErrorText(mActivityRule.getActivity().getString(R.string.error_incorrect_uname))));
    }

    @Test
    public void testLogin_success() {
        mServer.enqueue(new MockResponse().setResponseCode(200));
        startActivity();
        final int LAUNCH_TIMEOUT = 5000;
        final String BASIC_SAMPLE_PACKAGE = "com.tud.kom.parkinglot";
        // Initialize UiDevice instance
        UiDevice mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        mDevice.wait(Until.hasObject(By.pkg(BASIC_SAMPLE_PACKAGE).depth(0)), LAUNCH_TIMEOUT);
        mDevice.waitForIdle();

        UiObject email = mDevice.findObject(new UiSelector().resourceId("com.tud.kom.parkinglot:id/username").className("android.widget.EditText"));
        UiObject pwd = mDevice.findObject(new UiSelector().resourceId("com.tud.kom.parkinglot:id/password").className("android.widget.EditText"));
        Espresso.closeSoftKeyboard();
        UiObject signInButton = mDevice.findObject(new UiSelector().resourceId("com.tud.kom.parkinglot:id/sign_in_button").className("android.widget.Button"));
        try {
            assertTrue("singin-button doesnt exist or isnt enabled", signInButton.exists() && signInButton.isEnabled());
            assertTrue("username-edittext doesnt exist or isnt enabled", email.exists() && email.isEnabled());
            assertTrue("password-edittext doesnt exist or isnt enabled", pwd.exists() && pwd.isEnabled());
            email.setText(rightName);
            pwd.setText(rightPassword);
            mDevice.waitForIdle();
            signInButton.click();
            mDevice.wait(Until.hasObject(By.pkg(BASIC_SAMPLE_PACKAGE).depth(0)), LAUNCH_TIMEOUT);
            mDevice.waitForIdle();
            //TODO check if mainactivity is called
        } catch (UiObjectNotFoundException e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testStartOverviewActivity() throws Exception {
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody("{\"id\":123}"));//login
        mServer.enqueue(new MockResponse().setResponseCode(200));//firebase

        startActivity();

        onView(withId(R.id.username)).perform(typeText("smart"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.password)).perform(typeText("parking"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.sign_in_button)).perform(click());

        intended(hasComponent(NavigationActivity.class.getName()));
        RecordedRequest request = mServer.takeRequest();
        assertEquals("/users/getUser.json", request.getPath());
        assertEquals("Basic " + Base64.toBase64String(("smart:parking").getBytes()), request.getHeader("Authorization"));
    }

    private void startActivity() {
        mActivityRule.launchActivity(new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), LoginActivity.class));
        allowPermissionsIfNeeded(ACCESS_FINE_LOCATION);
    }
}
