<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
			
						        <li class="btn btn-default"><?= $this->Form->postLink(
		                __('Delete'),
		                ['action' => 'delete', $databaseLog->id],
		                ['confirm' => __('Are you sure you want to delete # {0}?', $databaseLog->id)]
		            )
		        ?>
		    	</li>
							<li class="btn btn-default"><?= $this->Html->link(__('List Database Logs'), ['action' => 'index']) ?></li>
						
	        <li class="btn btn-default"><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
	        <li class="btn btn-default"><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
			    	</ul>
	</div>
</div>



<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add/Edit
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
    					<?= $this->Form->create($databaseLog) ?>
    					<fieldset>
        					<legend><?= __('Edit Database Log') ?></legend>
        						<?php
echo "<div class='form-group'>".$this->Form->input('type', ['label' => 'Type:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('message', ['label' => 'Message:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('context', ['label' => 'Context:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('ip', ['label' => 'Ip:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('hostname', ['label' => 'Hostname:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('uri', ['label' => 'Uri:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('refer', ['label' => 'Refer:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('user_agent', ['label' => 'User_agent:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('count', ['label' => 'Count:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('tenant', ['label' => 'Tenant:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('action', ['label' => 'Action:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('userName', ['label' => 'UserName:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('userRole', ['label' => 'UserRole:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('oldValue', ['label' => 'OldValue:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('newValue', ['label' => 'NewValue:', 'class' => 'form-control'])."</div>";
echo "<div class='form-group'>".$this->Form->input('user_id', ['label' => 'User_id:', 'class' => 'form-control'])."</div>";
        ?>
    					</fieldset>
				    <?= $this->Form->button(__('Submit')) ?>
				    <?= $this->Form->end() ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
