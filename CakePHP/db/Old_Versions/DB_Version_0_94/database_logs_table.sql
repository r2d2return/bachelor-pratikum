-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 30, 2017 at 07:02 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartparking`
--

-- --------------------------------------------------------

--
-- Table structure for table `database_logs`
--

CREATE TABLE `database_logs` (
  `id` int(11) NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `context` text COLLATE utf8_unicode_ci,
  `created` timestamp NULL DEFAULT NULL,
  `ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hostname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` int(10) NOT NULL DEFAULT '0',
  `tenant` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` enum('CREATE','UPDATE','DELETE','OTHER') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'OTHER',
  `userName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userRole` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldValue` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `newValue` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `database_logs`
--

INSERT INTO `database_logs` (`id`, `type`, `message`, `context`, `created`, `ip`, `hostname`, `uri`, `refer`, `user_agent`, `count`, `tenant`, `action`, `userName`, `userRole`, `oldValue`, `newValue`, `user_id`) VALUES
(23, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => NewCustomer , Reservation Manager\n    [newValue] => BrandNewCustomer , Guest\n)', '2017-08-26 20:14:27', '::1', 'localhost', '/users/edit/5', 'https://localhost/users/edit/5', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'NewCustomer , Reservation Manager', 'BrandNewCustomer , Guest', 3),
(24, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => nhkdi , Visitor\n)', '2017-08-26 20:15:50', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'nhkdi , Visitor', 3),
(25, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nhkdi , Visitor\n    [newValue] => NewFacility , Facility Manager\n)', '2017-08-26 20:16:49', '::1', 'localhost', '/users/edit/33', 'https://localhost/users/edit/33', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'nhkdi , Visitor', 'NewFacility , Facility Manager', 3),
(26, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => NewFacility , Facility Manager\n    [newValue] => ndhaknd , Reservation Manager\n)', '2017-08-26 20:18:39', '::1', 'localhost', '/users/edit/33', 'https://localhost/users/edit/33', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'NewFacility , Facility Manager', 'ndhaknd , Reservation Manager', 3),
(27, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => ksmjs , Visitor\n)', '2017-08-26 20:54:59', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'ksmjs , Visitor', 3),
(28, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => dacaa , Reservation Manager\n)', '2017-08-26 20:56:13', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'dacaa , Reservation Manager', 3),
(29, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => mah , \n)', '2017-08-26 21:03:05', '::1', 'localhost', '/users/delete/6', 'https://localhost/users', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'mah , ', NULL, 3),
(30, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Fac , Facility Manager\n)', '2017-08-26 21:04:05', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'Fac , Facility Manager', 3),
(31, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => Fac , Facility Manager\n)', '2017-08-26 21:04:11', '::1', 'localhost', '/users/delete/36', 'https://localhost/users', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'Fac , Facility Manager', NULL, 3),
(32, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Reservation , Reservation Manager\n)', '2017-08-28 12:14:01', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, NULL, 'OTHER', NULL, NULL, NULL, NULL, NULL),
(33, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => guest , Admin\n)', '2017-08-28 12:15:22', '::1', 'localhost', '/users/delete/38', 'https://localhost/users', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'guest , Admin', NULL, 3),
(34, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Reservation , Reservation Manager\n    [newValue] => Visitor , Visitor\n)', '2017-08-28 12:16:26', '::1', 'localhost', '/users/edit/39', 'https://localhost/users/edit/39', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'Reservation , Reservation Manager', 'Visitor , Visitor', 3),
(35, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Facility , Facility Manager\n)', '2017-08-28 12:17:31', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'Facility , Facility Manager', 3),
(36, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => vis , Visitor\n)', '2017-08-28 15:55:50', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'vis , Visitor', 3),
(37, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => vis , Visitor\n    [newValue] => facilit , Facility Manager\n)', '2017-08-28 15:56:32', '::1', 'localhost', '/users/edit/41', 'https://localhost/users/edit/41', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'vis , Visitor', 'facilit , Facility Manager', 3),
(38, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => facilit , Facility Manager\n)', '2017-08-28 15:57:03', '::1', 'localhost', '/users/delete/41', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'facilit , Facility Manager', NULL, 3),
(39, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Facility , Facility Manager\n    [newValue] => FacilityAdmin , Admin\n)', '2017-08-28 22:42:52', '::1', 'localhost', '/users/edit/40', 'https://localhost/users/edit/40', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, NULL, 'OTHER', NULL, NULL, NULL, NULL, NULL),
(40, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => FacilityAdmin , Admin\n    [newValue] => Facility , Facility Manager\n)', '2017-08-28 22:43:59', '::1', 'localhost', '/users/edit/40', 'https://localhost/users/edit/40', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'FacilityAdmin , Admin', 'Facility , Facility Manager', 3),
(41, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => res , Reservation Manager\n)', '2017-08-29 12:35:27', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'res , Reservation Manager', 3),
(42, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => visme , Visitor\n)', '2017-08-29 12:41:43', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'res', 'Reservation Manager', NULL, 'visme , Visitor', 41),
(43, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => newVisi , Visitor\n)', '2017-08-30 18:00:42', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, NULL, 'OTHER', NULL, NULL, NULL, NULL, NULL),
(44, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => newVisi , Visitor\n)', '2017-08-30 18:02:08', '::1', 'localhost', '/users/delete/43', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'newVisi , Visitor', NULL, 3),
(45, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => visme , Visitor\n    [newValue] => vismejdhd , Reservation Manager\n)', '2017-08-30 18:02:36', '::1', 'localhost', '/users/edit/42', 'https://localhost/users/edit/42', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'visme , Visitor', 'vismejdhd , Reservation Manager', 3),
(46, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => gu , Guest\n)', '2017-08-30 18:03:04', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'gu , Guest', 3),
(47, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => gu , Guest\n)', '2017-08-30 18:31:30', '::1', 'localhost', '/users/delete/44', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'gu , Guest', NULL, 3),
(48, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => vismejdhd , Reservation Manager\n)', '2017-08-30 18:32:18', '::1', 'localhost', '/users/delete/42', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'vismejdhd , Reservation Manager', NULL, 3),
(49, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => res , Reservation Manager\n    [newValue] => resd , Guest\n)', '2017-08-30 18:55:05', '::1', 'localhost', '/users/edit/41', 'https://localhost/users/edit/41', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'res , Reservation Manager', 'resd , Guest', 3),
(50, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => resd , Guest\n)', '2017-08-30 19:01:21', '::1', 'localhost', '/users/delete/41', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'resd , Guest', NULL, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `database_logs`
--
ALTER TABLE `database_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_fk` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `database_logs`
--
ALTER TABLE `database_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
