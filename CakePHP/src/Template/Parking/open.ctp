<div style="font-weight:bold;">
	<div style="text-align:center;margin-top:10px;padding-top:10px;">
		<?= $this->Html->image('barrier_open.png', [ 'alt' => __('Barrier open')]); ?>
		<div style="text-align:center;">
			Please stand by while parking lock is opening and pull in.<br />
			This parking lot is reserved for you until <?= h($reservation->end) ?>
			<br />
			Please enjoy your stay!
		</div>
	</div>
</div>
<?php $this->Html->addCrumb('Parking', '/parking'); ?>
<?php $this->Html->addCrumb('Open', ['controller' => 'Parking', 'action' => 'open']); ?>