<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ParkinglotsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ParkinglotsTable Test Case
 */
class ParkinglotsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ParkinglotsTable
     */
    public $Parkinglots;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.parkinglots',
        'app.gateways',
        'app.remote_keys',
        'app.reservations',
        'app.users',
        'app.database_logs',
        'app.tenants',
        'app.tenants_users',
        'app.roles',
        'app.auth_acl',
        'app.users_roles',
        'app.zones',
        'app.parkinglots_zones',
        'app.users_zones',
        'app.sites',
        'app.parkinglots_sites',
        'app.zones_sites',
        'app.sensorlogs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Parkinglots') ? [] : ['className' => ParkinglotsTable::class];
        $this->Parkinglots = TableRegistry::get('Parkinglots', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Parkinglots);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
