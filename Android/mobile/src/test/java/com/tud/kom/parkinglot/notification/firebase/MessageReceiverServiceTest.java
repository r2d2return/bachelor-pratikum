package com.tud.kom.parkinglot.notification.firebase;

import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.SavedPreferences.Filename;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowService;

import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
public class MessageReceiverServiceTest {
    private MessageReceiverService mService;
    private ShadowService mShadowService;

    @Before
    public void setUp() {
        mService = Robolectric.setupService(MessageReceiverService.class);
        mShadowService = shadowOf(mService);
    }

    @Test
    public void testOnMessageReceived_remoteMessageNull() {
        mService.onMessageReceived(null);
    }

    @Test
    public void testOnMessageReceived_missingOrWrongCredentials() {
        Assert.assertEquals(1, mService.prepareScheduleAlarm("42", true));
        SavedPreferences.savePreference(RuntimeEnvironment.application.getApplicationContext(), Filename.USERNAME, "smart");
        Assert.assertEquals(1, mService.prepareScheduleAlarm("42", true));
        SavedPreferences.savePreference(RuntimeEnvironment.application.getApplicationContext(), Filename.PASSWORD, "parking");
        Assert.assertEquals(1, mService.prepareScheduleAlarm("42", true));
        SavedPreferences.savePreference(RuntimeEnvironment.application.getApplicationContext(), Filename.UID, "43");
        Assert.assertEquals(1, mService.prepareScheduleAlarm("42", true));
    }

    @Test
    public void testOnMessageReceived_noPermissions() {
        SavedPreferences.savePreference(RuntimeEnvironment.application.getApplicationContext(), Filename.USERNAME, "smart");
        SavedPreferences.savePreference(RuntimeEnvironment.application.getApplicationContext(), Filename.PASSWORD, "parking");
        SavedPreferences.savePreference(RuntimeEnvironment.application.getApplicationContext(), Filename.UID, "42");
        Assert.assertEquals(2, mService.prepareScheduleAlarm("42", true));
    }
}