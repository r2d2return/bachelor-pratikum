<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersZonesFixture
 *
 */
class UsersZonesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    /*
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'zone_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'keyName' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'start' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'end' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];*/
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'user_id' => 1,
            'zone_id' => 1,
            'keyName' => 'Lorem ipsum dolor sit amet',
            'start' => '2017-08-23 11:07:11',
            'end' => '2018-08-23 11:07:11'
        ],

        [
            'id' => 2,
            'user_id' => 2,
            'zone_id' => 2,
            'keyName' => 'Lorem ipsum dolor sit amet',
            'start' => '2017-08-23 11:07:11',
            'end' => '2018-08-23 11:07:11'
        ],

        [
            'id' => 3,
            'user_id' => 3,
            'zone_id' => 3,
            'keyName' => 'Lorem ipsum dolor sit amet',
            'start' => '2017-08-23 11:07:11',
            'end' => '2018-08-23 11:07:11'
        ],

        [
            'id' => 4,
            'user_id' => 4,
            'zone_id' => 4,
            'keyName' => 'Lorem ipsum dolor sit amet',
            'start' => '2017-08-23 11:07:11',
            'end' => '2018-08-23 11:07:11'
        ],

        [
            'id' => 5,
            'user_id' => 5,
            'zone_id' => 5,
            'keyName' => 'Lorem ipsum dolor sit amet',
            'start' => '2017-08-23 11:07:11',
            'end' => '2018-08-23 11:07:11'
        ]

        
    ];
}
