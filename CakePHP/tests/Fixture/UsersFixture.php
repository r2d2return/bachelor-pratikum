<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;
use Cake\Auth\DefaultPasswordHasher;

/**
 * UsersFixture
 *
 */
class UsersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    /*
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'username' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'password' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'email' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'mobileNumber' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'landlineNumber' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'firstname' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'lastname' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'title' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'gender' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'userState' => ['type' => 'string', 'length' => null, 'null' => false, 'default' => 'ACTIVE', 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'company' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'department' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'street' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'ZIP' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];*/
    // @codingStandardsIgnoreEnd

   public function init(){
        
        $this->records = [
        [
            'id' => 1,
            'username' => 'Admin',
            'password' => (new DefaultPasswordHasher)->hash('Admin'),
            'created' => '2017-08-23 11:06:46',
            'modified' => '2017-08-23 11:06:46',
            'email' => 'Lorem ipsum dolor sit amet',
            'mobileNumber' => 'Lorem ipsum dolor sit amet',
            'landlineNumber' => 'Lorem ipsum dolor sit amet',
            'firstname' => 'Lorem ipsum dolor sit amet',
            'lastname' => 'Lorem ipsum dolor sit amet',
            'title' => 'Lorem ipsum dolor sit amet',
            'gender' => 'Lorem ipsum dolor sit amet',
            'userState' => 'ACTIVE',
            'company' => 'Lorem ipsum dolor sit amet',
            'department' => 'Lorem ipsum dolor sit amet',
            'street' => 'Lorem ipsum dolor sit amet',
            'ZIP' => 'Lorem ipsum dolor sit amet'
        ],

        [
            'id' => 2,
            'username' => 'Guest',
            'password' => (new DefaultPasswordHasher)->hash('Guest'),
            'created' => '2017-08-23 11:06:46',
            'modified' => '2017-08-23 11:06:46',
            'email' => 'Lorem ipsum dolor sit amet',
            'mobileNumber' => 'Lorem ipsum dolor sit amet',
            'landlineNumber' => 'Lorem ipsum dolor sit amet',
            'firstname' => 'Lorem ipsum dolor sit amet',
            'lastname' => 'Lorem ipsum dolor sit amet',
            'title' => 'Lorem ipsum dolor sit amet',
            'gender' => 'Lorem ipsum dolor sit amet',
            'userState' => 'ACTIVE',
            'company' => 'Lorem ipsum dolor sit amet',
            'department' => 'Lorem ipsum dolor sit amet',
            'street' => 'Lorem ipsum dolor sit amet',
            'ZIP' => 'Lorem ipsum dolor sit amet'
        ],

        [
            'id' => 3,
            'username' => 'Visitor',
            'password' => (new DefaultPasswordHasher)->hash('Visitor'),
            'created' => '2017-08-23 11:06:46',
            'modified' => '2017-08-23 11:06:46',
            'email' => 'Lorem ipsum dolor sit amet',
            'mobileNumber' => 'Lorem ipsum dolor sit amet',
            'landlineNumber' => 'Lorem ipsum dolor sit amet',
            'firstname' => 'Lorem ipsum dolor sit amet',
            'lastname' => 'Lorem ipsum dolor sit amet',
            'title' => 'Lorem ipsum dolor sit amet',
            'gender' => 'Lorem ipsum dolor sit amet',
            'userState' => 'ACTIVE',
            'company' => 'Lorem ipsum dolor sit amet',
            'department' => 'Lorem ipsum dolor sit amet',
            'street' => 'Lorem ipsum dolor sit amet',
            'ZIP' => 'Lorem ipsum dolor sit amet'
        ],

        [
            'id' => 4,
            'username' => 'Reservation Manager',
            'password' => (new DefaultPasswordHasher)->hash('Reservation Manager'),
            'created' => '2017-08-23 11:06:46',
            'modified' => '2017-08-23 11:06:46',
            'email' => 'Lorem ipsum dolor sit amet',
            'mobileNumber' => 'Lorem ipsum dolor sit amet',
            'landlineNumber' => 'Lorem ipsum dolor sit amet',
            'firstname' => 'Lorem ipsum dolor sit amet',
            'lastname' => 'Lorem ipsum dolor sit amet',
            'title' => 'Lorem ipsum dolor sit amet',
            'gender' => 'Lorem ipsum dolor sit amet',
            'userState' => 'ACTIVE',
            'company' => 'Lorem ipsum dolor sit amet',
            'department' => 'Lorem ipsum dolor sit amet',
            'street' => 'Lorem ipsum dolor sit amet',
            'ZIP' => 'Lorem ipsum dolor sit amet'
        ],

        [
            'id' => 5,
            'username' => 'Facility Manager',
            'password' => (new DefaultPasswordHasher)->hash('Facility Manager'),
            'created' => '2017-08-23 11:06:46',
            'modified' => '2017-08-23 11:06:46',
            'email' => 'Lorem ipsum dolor sit amet',
            'mobileNumber' => 'Lorem ipsum dolor sit amet',
            'landlineNumber' => 'Lorem ipsum dolor sit amet',
            'firstname' => 'Lorem ipsum dolor sit amet',
            'lastname' => 'Lorem ipsum dolor sit amet',
            'title' => 'Lorem ipsum dolor sit amet',
            'gender' => 'Lorem ipsum dolor sit amet',
            'userState' => 'ACTIVE',
            'company' => 'Lorem ipsum dolor sit amet',
            'department' => 'Lorem ipsum dolor sit amet',
            'street' => 'Lorem ipsum dolor sit amet',
            'ZIP' => 'Lorem ipsum dolor sit amet'
        ]

    ];


    
    
    parent::init();
    }
    
}
