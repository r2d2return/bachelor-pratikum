<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Parkinglots Model
 *
 * @property \App\Model\Table\LocationsTable|\Cake\ORM\Association\HasOne $Locations
 * @property \App\Model\Table\GatewaysTable|\Cake\ORM\Association\BelongsTo $Gateways
 * @property \App\Model\Table\RemoteKeysTable|\Cake\ORM\Association\HasMany $RemoteKeys
 * @property \App\Model\Table\ReservationsTable|\Cake\ORM\Association\HasMany $Reservations
 * @property \App\Model\Table\SensorlogsTable|\Cake\ORM\Association\HasMany $Sensorlogs
 * @property \App\Model\Table\SitesTable|\Cake\ORM\Association\BelongsToMany $Sites
 * @property \App\Model\Table\ZonesTable|\Cake\ORM\Association\BelongsToMany $Zones
 *
 * @method \App\Model\Entity\Parkinglot get($primaryKey, $options = [])
 * @method \App\Model\Entity\Parkinglot newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Parkinglot[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Parkinglot|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Parkinglot patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Parkinglot[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Parkinglot findOrCreate($search, callable $callback = null, $options = [])
 */
class ParkinglotsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('parkinglots');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Gateways', [
            'foreignKey' => 'gateway_id'
        ]);
        $this->hasMany('RemoteKeys', [
            'foreignKey' => 'parkinglot_id'
        ]);
        $this->hasMany('Reservations', [
            'foreignKey' => 'parkinglot_id'
        ]);
        $this->hasMany('Sensorlogs', [
            'foreignKey' => 'parkinglot_id'
        ]);
        $this->belongsToMany('Sites', [
            'foreignKey' => 'parkingLotsId',
            'targetForeignKey' => 'sitesId',
            'joinTable' => 'parkinglots_sites'
        ]);
        $this->hasOne('Locations')
            ->setForeignKey('id');
        /**
        $this->belongsToMany('Zones', [
            'foreignKey' => 'parkinglot_id',
            'targetForeignKey' => 'zone_id',
            'joinTable' => 'parkinglots_zones'
        ]);
         * /
         * **/
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('pid')
            ->requirePresence('pid', 'create')
            ->notEmpty('pid')
            ->add('pid', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->scalar('mqttchannel')
            ->allowEmpty('mqttchannel');

        $validator
            ->scalar('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->scalar('ip')
            ->allowEmpty('ip');

        $validator
            ->scalar('hwid')
            ->allowEmpty('hwid');

        $validator
            ->dateTime('lastStateChange')
            ->requirePresence('lastStateChange', 'create')
            ->notEmpty('lastStateChange');

        $validator
            ->scalar('type')
            ->allowEmpty('type');

        $validator
            ->integer('locationId')
            ->requirePresence('locationId', 'create')
            ->notEmpty('locationId');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['pid']));
        $rules->add($rules->existsIn(['gateway_id'], 'Gateways'));

        return $rules;
    }
}
