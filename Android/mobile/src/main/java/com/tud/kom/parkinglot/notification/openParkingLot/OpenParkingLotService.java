package com.tud.kom.parkinglot.notification.openParkingLot;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.tud.kom.parkinglot.model.Reservation;
import com.tud.kom.parkinglot.network.NetworkCallback;
import com.tud.kom.parkinglot.network.RequestAbstraction;
import com.tud.kom.parkinglot.network.Response;

public class OpenParkingLotService extends IntentService implements NetworkCallback {
    public static final String RESERVATION_EXTRA = "reservation";
    private static final String TAG = OpenParkingLotService.class.getSimpleName();
    private Intent intent;

    public OpenParkingLotService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        this.intent = intent;
        Log.i(TAG, "open parking lot: " + intent.getSerializableExtra(RESERVATION_EXTRA));
        RequestAbstraction.openParkingLot((Reservation) intent.getSerializableExtra(RESERVATION_EXTRA), this);
    }

    @Override
    public void handleWebsiteResponse(Response response) {
        OpenParkingLotBroadcast.completeWakefulIntent(intent);
    }
}
