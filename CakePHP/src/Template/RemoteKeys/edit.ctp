<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $remoteKey->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $remoteKey->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List RemoteKeys'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Parking Lots'), ['controller' => 'Parkinglots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Parking Lot'), ['controller' => 'Parkinglots', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="remoteKeys form large-10 medium-9 columns">
    <?= $this->Form->create($remoteKey) ?>
    <fieldset>
        <legend><?= __('Edit RemoteKey') ?></legend>
        <?php
        	$this->Form->templates([
    			'dateWidget' => '{{day}}{{month}}{{year}}<br />{{hour}}{{minute}}{{second}}{{meridian}}'
			]);
            echo $this->Form->input('username', ['label' => 'Keyowner-Name:']);
            echo $this->Form->input('mail', ['label' => 'mail']);
            echo $this->Form->input('token', ['readonly' => 'readonly']);
            echo $this->Form->input('valid_from', ['monthNames' => false]);
            echo $this->Form->input('valid_until', ['monthNames' => false]);
            echo $this->Form->input('parkinglot_description', ['options' => $parkinglots]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<?php $this->Html->addCrumb('Remote Keys', '/remoteKeys'); ?>
<?php $this->Html->addCrumb('Edit', ['controller' => 'RemoteKeys', 'action' => 'edit']); ?>