<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Reservationrequest Entity
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $gender
 * @property string $phone
 * @property \Cake\I18n\Time $start
 * @property \Cake\I18n\Time $end
 * @property string $status
 */
class Reservationrequest extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
